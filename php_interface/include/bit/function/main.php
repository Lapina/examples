<?php
/**
 * Author : Lapina Anastasia
 * Created: 12.08.14 10:24
*/

/**
 * Получить название месяца по порядковому номеру
 *
 * @param $month
 * @param string $type = ru|en|ru_rod
 * @return mixed
 */
function getMonthName($month, $type = 'ru')
{
    $data_month['ru'] = array(
        '01' => 'январь',	'02' => 'февраль',	'03' => 'март',
        '04' => 'апрель',	'05' => 'май',		'06' => 'июнь',
        '07' => 'июль',		'08' => 'август',	'09' => 'сентябрь',
        '10' => 'октябрь',	'11' => 'ноябрь',	'12' => 'декабрь'
    );
    $data_month['en'] = array(
        '01' => 'january',	'02' => 'february',	'03' => 'march',
        '04' => 'april',	'05' => 'may',		'06' => 'june',
        '07' => 'july',		'08' => 'august',	'09' => 'september',
        '10' => 'october',	'11' => 'november',	'12' => 'december'
    );
    $data_month['ru_rod'] = array(
        '01' => 'января',	'02' => 'февраля',	'03' => 'марта',
        '04' => 'апреля',	'05' => 'мая',		'06' => 'июня',
        '07' => 'июля',		'08' => 'августа',	'09' => 'сентября',
        '10' => 'октября',	'11' => 'ноября',	'12' => 'декабря'
    );

    return $data_month[$type][$month];
}

/**
 * Функция транслитезируем слово из языка @param string $lang в английский
 *
 * @param $name
 * @param string $lang
 * @return string
 */
function bxTranslate($name, $lang = 'ru')
{
    $params = Array(
        "max_len"          => "100", # обрезает символьный код до 100 символов
        "change_case"      => "L",   # буквы преобразуются к нижнему регистру
        "replace_space"    => "_",   # меняем пробелы на нижнее подчеркивание
        "replace_other"    => "_",   # меняем левые символы на нижнее подчеркивание
        "delete_repeat_replace" => "true", # удаляем повторяющиеся нижние подчеркивания
    );

    return CUtil::translit($name, $lang, $params);
}

/**
 * Вывод массива с помощью <PRE></PRE>
 *
 * @param $data
 * @param bool $debug
 */
function pre($data, $debug = false)
{
    echo "<div style='clear:both'></div>";
    echo "<pre style='min-width:300px; padding: 10px; background: #fff; color: #008000; font-size: 13px; border: 1px solid #ddd; margin: 10px;'>";
    if($debug === true) print_r(debug_backtrace());
    print_r($data);
    echo "</pre>";
    echo "<div style='clear:both'></div>";
}
