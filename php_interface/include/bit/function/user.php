<?php
/**
 * Author : Lapina Anastasia
 * Created: 04.02.15 13:04
*/

/**
 * Функция проверяет финансовый пароль
 *
 * @param $userID
 * @param $pass
 * @return bool
 */
function checkFinPass($userID,$pass)
{
    $rsUsers = \Bitrix\Main\UserTable::getList(
        array(
            'select' => array('ID','UF_PASS_FIN'),
            'filter' => array('ID' => $userID)
        )
    );
    if ($user = $rsUsers->fetch())
    {
        if($user['UF_PASS_FIN'] == $pass && $userID == $user['ID'])
        {
            return true;
        }
    }

    return false;
}

/**
 * Функция находит пользователя по номеру контракта
 *
 * @param $contract
 * @return bool
 */
function findUserByContract($contract)
{
    $rsUsers = \Bitrix\Main\UserTable::getList(
        array(
            'select' => array('ID','UF_NUMBER_CONTRACT'),
            'filter' => array('UF_NUMBER_CONTRACT' => $contract)
        )
    );
    if ($user = $rsUsers->fetch())
    {
        if($user['UF_NUMBER_CONTRACT'] == $contract)
        {
            return  $user['ID'];
        }
    }

    return false;
}
/**
 * Функция находит пользователя по номеру контракта
 *
 * @param $fio
 * @return bool
 */
function findUserByFio($fio)
{
    $arUser = array();
    $rsUsers = \Bitrix\Main\UserTable::getList(
        array(
            'select' => array('ID','UF_NAME_FULL'),
            'filter' => array('UF_NAME_FULL' => '%'.$fio.'%')
        )
    );
    while ($user = $rsUsers->fetch())
    {
        if(stristr($user['UF_NAME_FULL'],$fio))
            $arUser[$user['ID']] = $user['ID'];
    }
    $rsUsers = \Bitrix\Main\UserTable::getList(
        array(
            'select' => array('ID','UF_DESCRIPTION'),
            'filter' => array('UF_DESCRIPTION' => '%'.$fio.'%')
        )
    );
    while ($user = $rsUsers->fetch())
    {
        if(stristr($user['UF_DESCRIPTION'],$fio))
            $arUser[$user['ID']] = $user['ID'];
    }

    return (count($arUser)>0)?$arUser:false;
}