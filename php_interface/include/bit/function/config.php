<?php
/**
 * Author : Lapina Anastasia
 * Created: 15.08.14 10:22
*/

/**
 * Функция возвращает раздел из инфоблока для определенного языка
 * в случае, если есть свойство с указанием ID языка из HL
 *
 * @param $iblockID - ID инфоблока
 * @param $propertyCode - код свойства с  указанием ID языка из HL
 * @return bool|mixed|null
 */
function getSectionByLanguageID($iblockID,$propertyCode)
{
    if(!$iblockID || !$propertyCode || !BIT_LANGUAGE_HL) return false;

    $cacheId = 'news_'.BIT_LANGUAGE_ID.$iblockID;

    if($section = BITCacheAPC::GetInstance()->getData($cacheId))
    {
        return $section;
    }
    else
    {
        CModule::IncludeModule('iblock');
        $result = CIBlockSection::getList(array(),array('IBLOCK_ID'=>$iblockID,$propertyCode=>BIT_LANGUAGE_HL),false,array('ID'))->Fetch();

        if($result['ID'])
        {
            BITCacheAPC::GetInstance()->setData($cacheId,$result['ID']);
            $section = $result['ID'];
        }
    }
    return $section;
}
