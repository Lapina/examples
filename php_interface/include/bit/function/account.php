<?php
/**
 * Author : Lapina Anastasia
 * Created: 23.01.15 16:12
*/

function BITPayUserAccountDeliveryOrderCallback($productID, $userID, $bPaid, $orderID, $quantity = 1)
{
    global $DB;

    $account = IntVal($productID);
    $userID  = IntVal($userID);
    $bPaid   = ($bPaid ? True : False);
    $orderID = IntVal($orderID);

    if ($userID <= 0)
        return False;

    if ($orderID <= 0)
        return False;

    if (!($arOrder = CSaleOrder::GetByID($orderID)))
        return False;

    $baseLangCurrency = CSaleLang::GetLangCurrency($arOrder["LID"]);
    $arAmount = unserialize(COption::GetOptionString("sale", "pay_amount", 'a:4:{i:1;a:2:{s:6:"AMOUNT";s:2:"10";s:8:"CURRENCY";s:3:"EUR";}i:2;a:2:{s:6:"AMOUNT";s:2:"20";s:8:"CURRENCY";s:3:"EUR";}i:3;a:2:{s:6:"AMOUNT";s:2:"30";s:8:"CURRENCY";s:3:"EUR";}i:4;a:2:{s:6:"AMOUNT";s:2:"40";s:8:"CURRENCY";s:3:"EUR";}}'));
    if (!array_key_exists($productID, $arAmount))
        return False;

    $currentPrice = $arAmount[$productID]["AMOUNT"] * $quantity;
    $currentCurrency = $arAmount[$productID]["CURRENCY"];
    if ($arAmount[$productID]["CURRENCY"] != $baseLangCurrency)
    {
        $currentPrice = CCurrencyRates::ConvertCurrency($arAmount[$productID]["AMOUNT"], $arAmount[$productID]["CURRENCY"], $baseLangCurrency) * $quantity;
        $currentCurrency = $baseLangCurrency;
    }

    if (!CSaleUserAccount::UpdateAccount($userID, ($bPaid ? $currentPrice : -$currentPrice), $currentCurrency, "MANUAL", $orderID, "Payment to user account"))
        return False;

    return True;
}

/**
 * Функция оплаты со счета
 * 
 * @param $userId
 * @param $currency
 * @param $orderId
 * @param $paySum
 * @return bool
 */
function BITDoPayOrderFromAccount($userId, $currency, $orderId, $paySum)
{
    CModule::IncludeModule('sale');
    CModule::IncludeModule('bit_hl');
    CModule::IncludeModule('currenct');

    $dbUserAccount = CSaleUserAccount::GetList(
        array(),
        array(
            "USER_ID" => $userId,
            "CURRENCY" => $currency,
        )
    );

    $arUserAccount = $dbUserAccount->Fetch();

    if (!$arUserAccount) return false;
    if ($arUserAccount["CURRENT_BUDGET"] <= 0) return false;

    // если бонусный счет

    if ($currency == 'BON')
    {
        $payBonus = \BIT\ORM\BITCurrencyConvert::ConvertToBall($paySum);
        if($payBonus>0 && $arUserAccount["CURRENT_BUDGET"] >= $payBonus)
        {
            if($acc = CSaleUserAccount::UpdateAccount($userId, "-".$payBonus, $currency, "PAY_ORDER", $orderId, 'PAY_ORDER')) { }
            else { return false;}
        }

    }

    // если обычный

    else
    {
        if ($arUserAccount["CURRENT_BUDGET"] < $paySum)
            return false;

        if($acc = CSaleUserAccount::UpdateAccount($userId, "-".$paySum, $currency, "PAY_ORDER", $orderId, 'PAY_ORDER')) { }
        else { return false;}
    }

    // оплата заказа

    if($order = CSaleOrder::GetByID($orderId))
    {
        if($order['CURRENCY'] != BIT_CURRENCY_ID)
        {
            $paySum = CCurrencyRates::ConvertCurrency($paySum, BIT_CURRENCY_ID, $order['CURRENCY']);
        }

        if(intval($order['SUM_PAID'])>0)
        {
            $paySum += $order['SUM_PAID'];
        }

        $arFields = array(
            "SUM_PAID" => $paySum,
            "USER_ID"  => $userId
        );

        CSaleOrder::Update($orderId, $arFields);
        if ($order['PRICE'] == $paySum)
            CSaleOrder::PayOrder($orderId, "Y", False, False);

        return true;
    }

    return false;
}
