<?php
/**
 * Author : Lapina Anastasia
 * Created: 26.11.14 19:10
*/

/**
 * Конвертация валюты в текущую
 *
 * @param $price
 * @param $currency
 * @return bool|float
 */
function convertCurrentCurrency($price,$currency)
{
    if(!CModule::IncludeModule('currency')) return;

    if(strlen($price)>0 && BIT_CURRENCY_ID)
    {
        $val = CCurrencyRates::ConvertCurrency($price, $currency, BIT_CURRENCY_ID);
        return $val;
    }
    return false;
}

/**
 * Конвертация корзины в текущую валюту
 *
 * @param $currency
 */
function convertBasket($currency = BIT_CURRENCY_ID)
{
    CModule::IncludeModule('sale');
    CModule::IncludeModule('currency');

    global $DB;


    $res = $DB->Query('SELECT * from b_sale_basket where ORDER_ID IS NULL AND FUSER_ID = '.CSaleBasket::GetBasketUserID());
    while($r = $res->Fetch())
    {
        if($r['CURRENCY'] != $currency)
        {
            $price = CCurrencyRates::ConvertCurrency($r['PRICE'],$r['CURRENCY'],$currency);
            $price = round($price);

            $arUpdate = array(
                'PRICE'    => $price,
                'CURRENCY' => "'".$currency."'",
            );
            //echo '<pre>'.print_r($DB->Query('select * from b_sale_basket WHERE ID = '.$r['ID'])->Fetch(),true).'</pre>';
            $DB->Update("b_sale_basket", $arUpdate, "WHERE ID=".$r['ID'], $err_mess.__LINE__);
            //echo '<pre>'.print_r($DB->Query('select * from b_sale_basket WHERE ID = '.$r['ID'])->Fetch(),true).'</pre>';
        }
    }

}