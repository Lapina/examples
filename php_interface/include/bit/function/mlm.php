<?php
/**
 * Author : Lapina Anastasia
 * Created: 01.12.14 15:18
*/

/**
 * Подсчет бонусов
 *
 * @param null $ID
 * @return int
 */
function countBonus($ID = NULL)
{
    CModule::IncludeModule('sale');
    CModule::IncludeModule('iblock');

    $bonus = 0;

    if($arProduct = BITEventOrder::getProductOrder($ID))
    {
        $arID = array_keys($arProduct);
        $res  = CIBlockElement::GetList(array(),array('ID'=>$arID),false,false,array('ID','PROPERTY_I_BONUS_PAY'));
        while($r = $res->Fetch())
        {
            if($arProduct[$r['ID']])
            {
                $bonus += intval($r['PROPERTY_I_BONUS_PAY_VALUE'])*$arProduct[$r['ID']]['QUANTITY'];
            }
        }
    }

    return intval($bonus);

}
/**
 * Получение баллов до последующего повышения статуса
 *
 * @param $userID
 * @return bool
 */
function getBallNextMLM($userID)
{
    CModule::IncludeModule('hl');
    $status = array();
    $result  = false;
    $PRICE  = false;
    // найдем статусы и суммы
    $res = \BIT\ORM\BITStatus::getList();
    while($r = $res->fetch())
    {
        $status[$r['UF_STRING']]['SUM']  = $r['UF_SUMM'];
        $status[$r['UF_STRING']]['STR']  = $r['UF_STRING'];
        $status[$r['UF_STRING']]['NAME'] = (BIT_LANGUAGE_ID=='ru'?$r['UF_NAME']:$r['UF_NAME_EN']);
    }
    if(count($status)>0)
    {
        if(intval($userID)>0)
        {
            // найдем сумму для вступленияю юзера в группы МЛМ
            if($user = \BIT\ORM\BITMlmAll::getList(array('filter'=>array('UF_USER'=>$userID),'select'=>array('UF_STATE')))->fetch())
            {
                if($user['UF_STATE']=='U')
                {
                    if($status['B'])
                    {
                        $result = $status['B'];
                    }
                }
                elseif($user['UF_STATE']=='B')
                {
                    if($status['S'])
                    {
                        $result = $status['S'];
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if($status['B'])
                {
                    $result = $status['B'];
                }
            }
        }
        else
        {
            if($status['B'])
            {
                $result = $status['B'];
            }
        }

    }
    else
    {
        return false;
    }

    return $result;
}