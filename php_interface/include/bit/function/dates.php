<?php
/**
 * Author : Lapina Anastasia
 * Created: 31.10.14 17:30
*/


function returnDateFilter()
{
    if($_REQUEST['date_start'] || $_REQUEST['date_finish'] || $_REQUEST['date_select'])
    {
        $today      = strtotime(date("d.m.Y"));
        $dateStart  = false;
        $dateFinish = false;

        # фильтр даты
        if($_REQUEST['date_start'] xor $_REQUEST['date_finish'])
        {
            if($_REQUEST['date_start'] && strlen($_REQUEST['date_start'])>0)
            {
                $date = strtotime($_REQUEST['date_start']);
                $date = date("d.m.Y",($date));
                $dateStart = strtotime($date);
            }
            if($_REQUEST['date_finish'] && strlen($_REQUEST['date_finish'])>0)
            {
                $date = strtotime($_REQUEST['date_finish']);
                $date = date("d.m.Y",($date+86400));
                $dateFinish = strtotime($date);
            }
        }
        elseif(strlen($_REQUEST['date_finish'])>0 && strlen($_REQUEST['date_start'])>0)
        {
            $_REQUEST['date_finish'] = (strtotime($_REQUEST['date_start']) > strtotime($_REQUEST['date_finish']))?$_REQUEST['date_start']:$_REQUEST['date_finish'];

            $date = strtotime($_REQUEST['date_start']);
            $date = date("d.m.Y",($date));
            $dateStart = strtotime($date);

            $plus = ($_REQUEST['date_start'] == $_REQUEST['date_finish'])?86399:0; $date = strtotime($_REQUEST['date_finish']);
            $date = date("d.m.Y",($date+$plus));
            $dateFinish = strtotime($date);

        }
        if($_REQUEST['date_select'])
        {
            $tmpDate = false;
            switch($_REQUEST['date_select'])
            {
                case 'T':
                    $tmpDate = $today;
                    break;
                case 'W':
                    $tmpDate = strtotime('-1 week');
                    break;
                case 'M':
                    $tmpDate = strtotime('-1 month');
                    break;
                case 'K':
                    $tmpDate = strtotime('-3 month');
                    break;
                case 'P':
                    $tmpDate = strtotime('-6 month');
                    break;
                case 'Y':
                    $tmpDate = strtotime('-1 year');
                    break;
            }
            $dateStart  = $tmpDate;
            $tmpDate    = date("d.m.Y",(time()+86400));
            $dateFinish = strtotime($tmpDate);
        }
        if($dateStart!==false && $dateFinish!==false)
        {
            return array('start'=>$dateStart,'finish'=>$dateFinish);
        }
    }
    return false;
}