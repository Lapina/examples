<?php
/**
 * Author : Lapina Anastasia
 * Created: 12.08.14 10:55
*/

class BITGeo {

    protected static $instance = NULL;    # экземпляр класса

    var $use_cache=true;

    var $CURRENT_CITY;
    var $CURRENT_COUNTRY;
    var $CURRENT_REGION;

    var $USER_CURRENT_CITY;
    var $USER_CURRENT_COUNTRY;
    var $USER_CURRENT_REGION;

    var $QT         = 0;
    var $USER_ID    = '-1';
    var $GEO        = false;
    var $cache_salt ='quatr';
    var $cache      = false;
    var $inited     = false;
    var $cache_ttl  = 36000;
    var $cache_path = false;

    var $DBH;

    function BITGeo($USER_CHOICE=false)
    {
        if(!defined('BIT_LANGUAGE_ID')) define('BIT_LANGUAGE_ID','ru');

        $this->DBH = new PDO('sqlite:'.__DIR__.'/geo.db');
        $this->DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if(class_exists('CPHPCache') and $this->use_cache)
        {
            $this->cache      = new CPHPCache();
            $this->cache_path = '/is_geo_cache/';
        }

        $this->FindUserInfo($USER_CHOICE);

        switch(BIT_LANGUAGE_ID)
        {
            case "ru":
                define('G_LANG','RUS');
                break;
            default:
                define('G_LANG','ENG');
        }
    }


    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL)
        {
            self::$instance = new BITGeo();
        }
        # возвращаем объект
        return self::$instance;
    }



    private function FindUserInfo($USER_CHOICE=false)
    {

        if(!isset($_SESSION["USER_CHOICE"])) $_SESSION["USER_CHOICE"]=false;
        if(function_exists('geoip_record_by_name')) $this->GEO = geoip_record_by_name($_SERVER['GEOIP_ADDR']);

        global $USER;
        if($USER->IsAuthorized())
        {
            if($U=$USER->GetList($by='id',$order='asc',array("ID"=>$USER->GetID()),array("SELECT"=>array("UF_GEO_ID")))->GetNext())
            {
                $this->USER_ID=$USER->GetID();

                if($U["UF_GEO_ID"]!=$this->GEO["city"])
                {
                    define('GEO_CHANGED',true);
                }
            }
        }
        else
        {
            $this->USER_ID = rand(111111,999999);
        }

        if($this->GEO["country_code"]) $this->CURRENT_COUNTRY=$this->GetCountryByCode($this->GEO["country_code"]);
        if($this->CURRENT_COUNTRY['ID'] and $this->GEO["city"]) $this->CURRENT_CITY=$this->GetCityByCountyAndName($this->CURRENT_COUNTRY['ID'], $this->GEO["city"]);
        if(is_array($this->CURRENT_CITY) and $this->CURRENT_CITY['REGION']) $this->CURRENT_REGION=$this->GetRegionByID($this->CURRENT_CITY['REGION']);

        if(
            !($USER_CHOICE or$_SESSION["USER_CHOICE"]) and
            !(is_array($this->CURRENT_CITY) or is_array($this->CURRENT_REGION) or is_array($this->CURRENT_COUNTRY)))
        {
            $USER_CHOICE = 619;
        }

        if($USER_CHOICE or $_SESSION["USER_CHOICE"])
        {
            if(intval($USER_CHOICE)) $_SESSION["USER_CHOICE"]=intval($USER_CHOICE);

            $USER_CHOICE = $_SESSION["USER_CHOICE"];
            $this->USER_CURRENT_CITY = $this->GetCityByID($USER_CHOICE);

            if($this->USER_CURRENT_CITY["REGION"])	$this->USER_CURRENT_REGION=$this->GetRegionByID($this->USER_CURRENT_CITY["REGION"]);
            if($this->USER_CURRENT_CITY["COUNTRY"]) $this->USER_CURRENT_COUNTRY=$this->GetCountryByID($this->USER_CURRENT_CITY["COUNTRY"]);
        }
        else
        {
            $this->USER_CURRENT_CITY    = $this->CURRENT_CITY;
            $this->USER_CURRENT_REGION  = $this->CURRENT_REGION;
            $this->USER_CURRENT_COUNTRY = $this->CURRENT_COUNTRY;
        }
    }

    public function GetUserInfo()
    {
        if(function_exists('geoip_record_by_name'))
        {
            $GEO = geoip_record_by_name($this->GetClientIP());
        }

        global $USER;
        $arResult = array();

        if($USER->IsAuthorized())
        {
            if($U=$USER->GetList($by='id',$order='asc',array("ID"=>$USER->GetID()),array("SELECT"=>array()))->GetNext())
            {
                $arResult["USER_ID"]=$USER->GetID();
            }
        }
        else
        {
            $arResult["USER_ID"]= '';
        }

        if($GEO["country_code"]) $arResult["COUNTRY"] = $this->GetCountryByCode($GEO["country_code"]);

        if($arResult["COUNTRY"]['ID'] and $GEO["city"]) $arResult["CITY"] = $this->GetCityByCountyAndName($arResult["COUNTRY"]['ID'], $GEO["city"]);
        if(is_array($arResult["CITY"]) and $arResult["CITY"]['REGION']) $arResult["REGION"] = $this->GetRegionByID($arResult["CITY"]['REGION']);

        return $arResult;
    }

    public function GetClientIP()
    {
        if( $_SERVER['HTTP_X_FORWARDED_FOR'] != '' )
        {
            $IP = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : ((!empty($_ENV['REMOTE_ADDR'])) ? $_ENV['REMOTE_ADDR'] : "" );
            $entries = preg_split('[, ]', $_SERVER['HTTP_X_FORWARDED_FOR']);
            reset($entries);

            while (list(, $entry) = each($entries))
            {
                $entry = trim($entry);
                if ( preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", $entry, $ip_list) )
                {
                    $private_ip = array(
                        '/^0\./',
                        '/^127\.0\.0\.1/',
                        '/^192\.168\..*/',
                        '/^172\.((1[6-9])|(2[0-9])|(3[0-1]))\..*/',
                        '/^10\..*/'
                    );

                    $found_ip = preg_replace($private_ip, $IP, $ip_list[1]);

                    if ($IP != $found_ip)
                    {
                        $IP = $found_ip;
                        break;
                    }
                }
            }
        }
        else
        {
            $IP = ( !empty($_SERVER['REMOTE_ADDR']) ) ? $_SERVER['REMOTE_ADDR'] : ( ( !empty($_ENV['REMOTE_ADDR']) ) ? $_ENV['REMOTE_ADDR'] : "" );
        }

        return $IP;
    }

    private function CacheStore($key,$data)
    {
        if($data===false) return false;

        if(is_object($this->cache))
        {
            if ($this->cache_ttl > 0)
            {
                if($this->cache->InitCache($this->cache_ttl, $key, $this->cache_path))
                {
                    $res = $this->cache->GetVars();
                    if (isset($res["data"])) return $res["data"];
                }

                $this->cache->StartDataCache($this->cache_ttl, $key, $this->cache_path);
                $this->cache->EndDataCache(array("data"=>$data));
            }
        }
        return $data;
    }

    private function CacheGet($key)
    {
        if(is_object($this->cache))
        {
            if ($this->cache_ttl > 0)
            {
                if($this->cache->InitCache($this->cache_ttl, $key, $this->cache_path))
                {
                    $res = $this->cache->GetVars();
                    if (isset($res["data"]))  return $res["data"]; else return false;
                }
            }
        }
        return false;
    }

    public function q($q,$anyway_array=false)
    {
        $r     = false;
        $start = microtime(1);
        $re    = $this->DBH->query($q);

        $this->QT+=microtime(1)-$start;
        $re->setFetchMode(PDO::FETCH_ASSOC);

        while ($row=$re->fetch())
        {
            if(isset($row["FLAG"]) and strlen($row["FLAG"])>3) $row["FLAG"]='<img class="c_flag" src="/upload/flags/'.$row["FLAG"].'"/>';
            if(!isset($row["RUS"]) or empty($row["RUS"])) if(isset($row["ENG"])) $row["RUS"]=$row["ENG"];
            $r[]=$row;
        }

        if(is_array($r)) return (count($r)>1 or $anyway_array)?$r:$r[0];
    }

    public function GetCountryList($with_FLAG=false)
    {
        if($FromCache = $this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        return $this->CacheStore($key,$this->q('select ID, RUS, ENG'.(($with_FLAG)?", FLAG":"").' from strana order by RUS ASC, ENG ASC'));
    }
    public function GetCountryAllList($with_FLAG=false)
    {
        if($FromCache = $this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        return $this->CacheStore($key,$this->q('select * '.(($with_FLAG)?", FLAG":"").' from strana order by RUS ASC, ENG ASC'));
    }

    public function GetCountryByID($ID)
    {
        $ID = intval($ID);
        if($FromCache = $this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        return $this->CacheStore($key,$this->q('select * from strana where ID='.$ID));
    }
    public function GetCountryArrId($arrId)
    {
        if(!is_array($arrId) || count($arrId)<1) return;

        $select      = "";
        $countDouble = 0;

        foreach($arrId as $id)
        {
            if($countDouble != 0) $select .= ' OR ';
            $select .= trim('ID')."='".$id."' ";
            $countDouble++;
        }
        if($FromCache = $this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        return $this->CacheStore($key,$this->q('select * from strana where '.$select));
    }
    public function GetRegionArrId($arrId) {
        if(!is_array($arrId) || count($arrId)<1) return;

        $select      = "";
        $countDouble = 0;
        foreach($arrId as $id)
        {
            if($countDouble!=0)
                $select .= ' OR ';
            $select.= trim('ID')."='".$id."' ";
            $countDouble++;
        }
        if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        return $this->CacheStore($key,$this->q('select * from region where '.$select));
    }
    public function GetCityArrId($arrId) {
        if(!is_array($arrId) || count($arrId)<1) return;

        $select      = "";
        $countDouble = 0;
        foreach($arrId as $id)
        {
            if($countDouble!=0)
                $select .= ' OR ';
            $select.= trim('ID')."='".$id."' ";
            $countDouble++;
        }
        if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        return $this->CacheStore($key,$this->q('select * from gorod where '.$select));
    }
    public function GetCountryByCode($CODE) { $CODE=trim($CODE);
        if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        return $this->CacheStore($key,$this->q('select * from strana where CODE=\''.$CODE.'\''));
    }
    public function GetRegionRegion($REGION=0) { $REGION=intval($REGION); if($REGION==0) return false;
        if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        return $this->CacheStore($key,$this->q('select ID, RUS, ENG, COUNTRY from region where REGION='.$REGION.' order by SORT DESC, ID asc',true));
    }
    public function GetCountryRegion($COUNTRY,$REGION=0) { $COUNTRY=intval($COUNTRY); $REGION=intval($REGION);
        if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        return $this->CacheStore($key,$this->q('select ID, RUS, ENG, COUNTRY from region where COUNTRY='.$COUNTRY.' '.(($REGION>0)?('and REGION='.$REGION):'').' order by RUS ASC, ENG ASC',true));
    }
    public function GetCountryRegionFLevel($COUNTRY) { $COUNTRY=intval($COUNTRY); $REGION=intval($REGION);
        if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        return $this->CacheStore($key,$this->q('select ID, RUS, ENG, COUNTRY from region where COUNTRY='.$COUNTRY.' AND REGION=0 order by SORT DESC, ID asc',true));
    }

    public function GetRegionByID($REGION) { $REGION=intval($REGION);
        if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        if($REGION>0) {
            return $this->CacheStore($key,$this->q('select * from REGION where ID='.$REGION));
        } else return false;
    }

    public function GetCountryCity($COUNTRY,$PATTERN=false,$strong=false) { $COUNTRY=intval($COUNTRY);
        if($PATTERN) $PATTERN=strtolower($PATTERN);
        if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        if($COUNTRY>0) {
            return $this->CacheStore($key,$this->q('select * from gorod where (CUST = 0 OR CUST='.$this->USER_ID.') AND COUNTRY='.$COUNTRY.(($PATTERN)?" AND (ENG LIKE '".$PATTERN.(($strong)?"":"%")."' OR SEARCH_RUS LIKE '".$PATTERN.(($strong)?"":"%")."')":""),true));
        } else return false;
    }

    public function GetRegionCity($REGION) { $REGION=intval($REGION);
        if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        if($REGION>0) {
            return $this->CacheStore($key,$this->q('select * from gorod where REGION='.$REGION.' AND (CUST = 0 OR CUST='.$this->USER_ID.') ORDER BY RUS ASC, ENG ASC',true));
        } else return false;
    }
    public function GetRegionCityNameSort($REGION) { $REGION=intval($REGION);
        if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        if($REGION>0) {
            return $this->CacheStore($key,$this->q('select * from gorod where REGION='.$REGION." AND (CUST = 0 OR CUST=".$this->USER_ID.") ORDER BY SORT DESC, ".G_LANG,true));
        } else return false;
    }
    public function GetCityByID($city) { $city=intval($city);
        if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        if($city>0) {
            return $this->CacheStore($key,$this->q('select * from gorod where ID='.$city));
        } else return false;
    }

    public function CheckCity($COUNTRY_ID,$CITY_ID) { $COUNTRY_ID=intval($COUNTRY_ID); $CITY_ID=intval($CITY_ID);
        if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        if($CITY_ID==0 or $COUNTRY_ID==0) return false;

        return $this->CacheStore($key,$this->q('select * from gorod where COUNTRY='.$COUNTRY_ID.' and ID = '.$CITY_ID.' limit 1'));
    }

    public function AddCity($COUNTRY_ID,$CITY_NAME){
        $CITY_NAME=trim(strip_tags($CITY_NAME));
        if($CITY_NAME=='' || $COUNTRY_ID==0) return false;
        $CITY_NAME=mysql_escape_string($CITY_NAME);
        $this->q($Q="INSERT INTO gorod ('COUNTRY','RUS','ENG','SEARCH_RUS','CUST') VALUES('".$COUNTRY_ID."','".$CITY_NAME."','".$CITY_NAME."','".strtolower($CITY_NAME)."','".$this->USER_ID."')");
        $this->cache->CleanDir($this->cache_path);
    }

    public function GetCustomCity(){
        $Cust=$this->q('SELECT gorod.* , strana.RUS CountryName FROM gorod, strana where gorod.CUST != 0 AND strana.ID=gorod.COUNTRY',true);
        return $Cust;
    }

    public function GetCityByCountyAndName($COUNTRY_ID,$city_name) { $COUNTRY_ID=intval($COUNTRY_ID); $city_name=trim($city_name);
        if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        if($city_name=='' or $COUNTRY_ID==0) return false;

        return $this->CacheStore($key,$this->q('select * from gorod where (CUST=0 OR CUST='.$this->USER_ID.') AND COUNTRY='.$COUNTRY_ID.' and ENG = \''.$city_name.'\' limit 1'));
    }

    public function GetRegionRegionThree($REGION) { $REGION=intval($REGION); if($REGION==0) return false;
        if($this->use_cache) if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        if(!$this->q('select REGION from region where REGION='.$REGION.' limit 1')) return false;
        $FIRST_LEVEL= $this->q('select * from region where REGION='.$REGION.' order by SORT DESC',true);
        if($FIRST_LEVEL) foreach($FIRST_LEVEL as &$F_REG) if($REG_SUB=$this->GetRegionRegionThree($F_REG["ID"])) $F_REG["SUB"]=$REG_SUB;
        if($this->use_cache) 	return $this->CacheStore($key,$FIRST_LEVEL);
        else 					return $FIRST_LEVEL;
    }

    public function GetCountryRegionThree($COUNTRY) { $COUNTRY=intval($COUNTRY);
        if($this->use_cache) if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        $FIRST_LEVEL= $this->q('select * from region where COUNTRY='.$COUNTRY.' and REGION=0 order by SORT DESC',true);
        if($FIRST_LEVEL) foreach($FIRST_LEVEL as &$F_REG) if($REG_SUB=$this->GetRegionRegionThree($F_REG["ID"])) { $F_REG["SUB"]=$REG_SUB; }
        if($this->use_cache) 	return $this->CacheStore($key,$FIRST_LEVEL);
        else 					return $FIRST_LEVEL;
    }

    public function GetFlag($COUNTRY) { $COUNTRY=intval($COUNTRY);
        if($FromCache=$this->CacheGet($key=md5($this->cache_salt.__FUNCTION__.implode(func_get_args())))) return $FromCache;
        if($COUNTRY>0) {
            $FLAG=$this->q('select ID,FLAG from strana where ID='.$COUNTRY);
            if($FLAG['FLAG']) return $this->CacheStore($key,$FLAG['FLAG']);
        } else return false;
    }
}