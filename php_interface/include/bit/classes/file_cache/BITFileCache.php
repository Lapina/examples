<?php
/**
 * Author : Lapina Anastasia
 * Created: 12.08.14 10:53
 */
class BITFileCache
{
    private static $dirCache = '/tmp/file_cache/';

    /**
     * Получаем данные с диска
     *
     * @param $path
     * @param $sKey
     * @return mixed|null
     */
    function getData($path,$sKey)
    {
        $vData = file_get_contents(self::$dirCache.$path.'/'.$sKey);
        $vData = unserialize($vData);

        return ($vData) ? $vData :null;
    }

    /**
     *
     * Сохраняем данные на диск
     *
     * @param $path
     * @param $sKey
     * @param $vData
     * @return bool|int
     */
    public static function setData($path,$sKey, $vData)
    {
        $pathDir = self::$dirCache;
        if(is_array($path))
        {
            foreach($path as $p)
            {
                $pathDir .= '/'.$p;
                if(!is_dir($pathDir))
                {
                    mkdir($pathDir);
                }
            }
        }
        else
        {
            return false;
        }
        return file_put_contents($pathDir.'/'.$sKey, serialize($vData));
    }
}
