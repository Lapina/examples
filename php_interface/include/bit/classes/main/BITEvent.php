<?php
# загрузка страницы
AddEventHandler("main", "OnBeforeProlog",  Array("BITLoad", "config"));
#--------- Элементы ---------#
# добавление элементов
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("BITEventElement", "checkModerate"));
# обновление элементов
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("BITEventElement", "checkModerate"));

#--------- ******* ---------#
#--------- Разделы ---------#


#--------- ******* ---------#
#--------- Стоимости ---------#


#--------- ******* ---------#
#--------- Пользователи ---------#

# перед авторизацией
AddEventHandler("main", "OnBeforeUserLogin", Array("BITEventUser", "checkContract"));

# перед добавлением

# перед регистрацией
//AddEventHandler("main", "OnBeforeUserAdd", Array("BITEventUser", "checkExist"));
//AddEventHandler("main", "OnBeforeUserAdd", Array("BITEventUser", "setParams"));

# после регистрации
AddEventHandler("main", "OnAfterUserAdd", Array("BITEventUser", "add2MLM"));
AddEventHandler("main", "OnAfterUserAdd", Array("BITEventUser", "addAffiliateHref"));
AddEventHandler("main", "OnAfterUserAdd", Array("BITEventUser", "add2AffiliateStatistic"));
AddEventHandler("main", "OnAfterUserAdd", Array("BITEventUser", "createAccount"));
AddEventHandler("main", "OnAfterUserRegister", Array("BITEventSms", "sendRegister"));

# обновление
AddEventHandler("main", "OnAfterUserUpdate", Array("BITEventUser", "updatePassFin"));
AddEventHandler("main", "OnAfterUserUpdate", Array("BITEventUser", "updateSponsor"));
AddEventHandler("main", "OnAfterUserUpdate", Array("BITEventUser", "updateStatus"));
AddEventHandler("main", "OnAfterUserUpdate", Array("BITEventUser", "updateAccount"));

#--------- ******* ---------#
#--------- Корзина ---------#

# обновление корзины
AddEventHandler("sale", "OnBeforeBasketUpdateAfterCheck", Array("BITEventBasket", "reCurrency"));

#--------- ******* ---------#
#--------- Заказы ---------#

# добавление заказов
AddEventHandler("sale", "OnOrderAdd", Array("BITEventSms", "sendOrderCreate"));
AddEventHandler("sale", "OnOrderAdd", Array("BITEventOrder", "saveBonus"));

# изменение статусов заказов
AddEventHandler("sale", "OnSaleStatusOrder", Array("BITEventSms", "sendOrderStatusChange"));

# оплата заказа
AddEventHandler("sale", "OnSalePayOrder", Array("BITEventOrder", "payUpdateUserGroup"));
AddEventHandler("sale", "OnSalePayOrder", Array("BITEventOrder", "UpdateLoSo"));

# отмена заказа
AddEventHandler("sale", "OnSaleCancelOrder", Array("BITEventOrder", "cancelUpdateUserGroup"));

#--------- ******* ---------#
#--------- Отправка сообщения в почтовую форму ---------#

#--------- ******* ---------#
#--------- Счета ---------#
# После добавление счета
AddEventHandler("sale", "OnAfterUserAccountAdd", Array("BITEventAccount", "writeToMlm"));
AddEventHandler("sale", "OnAfterUserAccountUpdate", Array("BITEventAccount", "writeToMlm"));
AddEventHandler("sale", "OnAfterUserAccountDelete", Array("BITEventAccount", "deleteFromMlm"));

#--------- ******* ---------#
#--------- Отправка сообщения в почтовую форму ---------#
AddEventHandler("main", "OnBeforeUserSendPassword", Array("BITEventCEvent", "forgotPass"));
AddEventHandler("main", "OnBeforeEventSend", Array("BITEventCEvent", "checkSend"));
AddEventHandler("main", "OnBeforeEventSend", Array("BITEventCEvent", "sendSms"));


class BITEvent
{
    protected static $instance = NULL; # экземпляр класса

    public function __construct()
    {

    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL) {
            self::$instance = new BITEvent();
        }
        # возвращаем объект
        return self::$instance;
    }
}

# Класс для обработки отправки СМС
class BITEventSms extends BITEvent
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL) {
            self::$instance = new BITEventSms();
        }
        # возвращаем объект
        return self::$instance;
    }
    /**
     *
    Восстановление доступа к аккаунту ?????
    Подтверждение смены пароля - только по почте
     *
     *
    Окончание срока действия заказа
    Об аннулировании заказа
    Об успешном выполнении заказа
    Рассылки по товарным предложениям
    Новостные рассылки

     */


    /**
     * Регистрация
     *
     * @param $arFields
     */
    public static function sendRegister(&$arFields)
    {
        if($arFields["USER_ID"])
        {
            BITConfigLang::includeLangFile();

            if(self::checkSend($arFields["USER_ID"],'NEW_USER'))
            {
                $mess = GetMessage('BIT_SMS_MESS_REGISTER');
                # массив для смс покупателю
                $arSms = array(
                    "TO_USER"   => $arFields["USER_ID"],
                    "MESS"      => $mess,
                );
                CEvent::Send("SMS", "s1", $arSms);
            }
        }
    }

    /**
     * Создание заказа
     *
     * @param $ID
     * @param $arFields
     */
    public static function sendOrderCreate($ID,$arFields)
    {
        if($arFields["USER_ID"])
        {
            if(self::checkSend($arFields["USER_ID"],'SALE_NEW_ORDER'))
            {
                BITConfigLang::includeLangFile();

                $mess      = GetMessage('BIT_SMS_MESS_NEW_ORDER');
                $arSearch  = array();
                $arValue   = array();
                $arReplace = array(
                    'ORDER_ID' => $ID,
                    'PRICE'    => $arFields['PRICE'],
                    'CURRENCY' => $arFields['CURRENCY'],
                );
                foreach ($arReplace as $search => $replace)
                {
                    $arSearch[] = "#".$search."#";
                    $arValue[]  = $replace;
                }

                $mess = str_replace($arSearch, $arValue, $mess);

                # массив для смс покупателю
                $arSms = array(
                    "TO_USER"   => $arFields["USER_ID"],
                    "MESS"      => $mess,
                );
                CEvent::Send("SMS", "s1", $arSms);
            }
        }
    }

    /**
     * Изменение статуса заказа
     *
     * @param $ID
     * @param $val
     */
    public static function sendOrderStatusChange($ID,$val)
    {
        if($val == 'F') return;

        CModule::IncludeModule('catalog');
        CModule::IncludeModule('sale');

        $order = \Bitrix\Sale\OrderTable::getById($ID)->fetch();
        if($order["USER_ID"])
        {
            $status = "SALE_STATUS_CHANGED_".$val;
            if(self::checkSend($order["USER_ID"],$status))
            {
                BITConfigLang::includeLangFile($_SESSION['BIT_LANGUAGE_ID']);

                $mess      = GetMessage('BIT_SMS_MESS_NEW_STATUS');
                $arStatus  = CSaleStatus::GetByID($val);
                $arSearch  = array();
                $arValue   = array();
                $arReplace = array(
                    'ORDER_ID' => $ID,
                    'STATUS'   => $arStatus['NAME'],
                );
                foreach ($arReplace as $search => $replace)
                {
                    $arSearch[] = "#".$search."#";
                    $arValue[]  = $replace;
                }

                $mess = str_replace($arSearch, $arValue, $mess);

                # массив для смс покупателю
                $arSms = array(
                    "TO_USER"   => $order["USER_ID"],
                    "MESS"      => $mess,
                );

                CEvent::Send("SMS", "s1", $arSms);
            }
        }
    }
    /**
     * Изменение статуса заказа
     *
     * @param $ID
     * @param $val
     */
    public static function sendOrderStatusReady($ID,$val)
    {
        if($val != 'F') return;

        CModule::IncludeModule('catalog');
        CModule::IncludeModule('sale');

        $order = \Bitrix\Sale\OrderTable::getById($ID)->fetch();
        if($order["USER_ID"])
        {
            if(self::checkSend($order["USER_ID"],"SALE_STATUS_CHANGED_F"))
            {
                BITConfigLang::includeLangFile($_SESSION['BIT_LANGUAGE_ID']);

                $mess      = GetMessage('BIT_SMS_MESS_NEW_READY');
                $arSearch  = array();
                $arValue   = array();
                $arReplace = array(
                    'ORDER_ID' => $ID
                );
                foreach ($arReplace as $search => $replace)
                {
                    $arSearch[] = "#".$search."#";
                    $arValue[]  = $replace;
                }

                $mess = str_replace($arSearch, $arValue, $mess);

                # массив для смс покупателю
                $arSms = array(
                    "TO_USER"   => $order["USER_ID"],
                    "MESS"      => $mess,
                );

                CEvent::Send("SMS", "s1", $arSms);
            }
        }
    }

    /**
     * Проверка возможности отправки смс
     *
     * @param $userID
     * @param $eventName
     * @return bool
     */
    public static function checkSend($userID,$eventName)
    {
        CModule::IncludeModule('bit_hl');

        $sms     = array();
        $arEvent = array();
        $return  = true;

        //получим список нотификаций:
        $res = \BIT\ORM\BITTypeNotice::getList();
        while($r = $res->fetch())
        {
            if($event = unserialize($r['UF_TYPE_EVENT']))
            {
                // созраним все эвенты
                foreach($event as $e)
                {
                    $arEvent[$e] = $r['ID'];
                }
                if(is_array($arEvent))
                {
                    // если нашли эвент
                    if($eventID = $arEvent[$eventName])
                    {
                        // если опознали, то найдем условия для него
                        if($userID !== false)
                        {
                            if($arUser = \BIT\ORM\BITUserNotification::getList(array('filter'=>array('UF_USER'=>$userID)))->fetch())
                            {
                                if(count($tmp = unserialize($arUser['UF_SMS']))>0)
                                {
                                    foreach($tmp as $id)
                                    {
                                        $sms[$id] = true;
                                    }
                                }
                                // если нет в массиве разрешенных - не отправляем
                                if(count($sms)>0)
                                {
                                    if(!isset($sms[$eventID]))
                                    {
                                        $return = false;
                                    }
                                }
                                else
                                {
                                    $return = false;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $return;
    }
}

# Класс для обработки запросов класса CEvent
class BITEventCEvent extends BITEvent
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL) {
            self::$instance = new BITEventCEvent();
        }
        # возвращаем объект
        return self::$instance;
    }

    /**
     * Отправка смс при срабатывание почтового события
     *
     * @param $arFields
     * @param $arTemplate
     * @return bool
     */
    public static function sendSms($arFields, $arTemplate)
    {
        # форма отправка смс
        if($arTemplate['EVENT_NAME']=='SMS' && strlen($arFields["MESS"])>2 )
        {
            if(strlen($arFields['TO_ADMIN'])>0)
                $userID = 1;
            else
                $userID = $arFields["TO_USER"];

            if($userID)
            {
                BITSms::GetInstance()->send($userID,$arFields["MESS"]);
            }
            return false;
        }
    }

    /**
     * Проверка возможности отправки сообщения (включена ли услуга у пользователя)
     * Эта инфа хранится в BITUserNotification
     *
     *
     * @param $arFields
     *        Обязательно должны присутствовать:
     *        ORDER_ID/EMAIL/
     * @param $arTemplate
     * @return bool
     */
    public static function checkSend($arFields, $arTemplate)
    {
        CModule::IncludeModule('bit_hl');
        CModule::IncludeModule('catalog');
        CModule::IncludeModule('sale');

        $return  = true;
        $arEvent = array();
        $userID  = false;

        //получим список нотификаций:
        $res = \BIT\ORM\BITTypeNotice::getList();
        while($r = $res->fetch())
        {
            if($event = unserialize($r['UF_TYPE_EVENT']))
            {
                // созраним все эвенты
                foreach($event as $e)
                {
                    $arEvent[$e] = $r['ID'];
                }
                if(is_array($arEvent))
                {
                    // если нашли эвент
                    if($eventID = $arEvent[$arTemplate['EVENT_NAME']])
                    {
                        // попробуем опознать пользователя:
                        if($arFields['ORDER_ID'])
                        {
                            if($arOrder = CSaleOrder::GetByID($arFields['ORDER_ID']))
                            {
                                if($arOrder['USER_ID'])
                                {
                                    $userID = $arOrder['USER_ID'];
                                }
                            }
                        }
                        // если опознали, то найдем условия для него
                        if($userID !== false)
                        {
                            if($arUser = \BIT\ORM\BITUserNotification::getList(array('filter'=>array('UF_USER'=>$userID)))->fetch())
                            {
                                if(count($tmp = unserialize($arUser['UF_EMAIL_SEND']))>0)
                                {
                                    foreach($tmp as $id)
                                    {
                                        $mail[$id] = true;
                                    }
                                }
                                // если нет в массиве разрешенных - не отправляем
                                if(count($mail)>0)
                                {
                                    if(!isset($mail[$eventID]))
                                    {
                                        $return = false;
                                    }
                                }
                                else
                                {
                                    $return = false;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $return;
    }

    /**
     * Отправка письма на восстановление пароля
     *
     * @param $arEvent
     * @param $arParams
     * @return bool
     */
    public function forgotPass($arEvent, $arParams)
    {
        if($_REQUEST['current_mail'] != $arEvent['EMAIL'])
        {
            $GLOBALS['APPLICATION']->ThrowException('Данная анкета зарегистрирована на другой E-Mail');
            return false;
        }
    }
}

# Класс хранит все операции с заказами
class BITEventOrder extends BITEvent
{
    protected $CSaleOrder;
    protected $CSaleOrderPropsValue;
    public function __construct()
    {
        parent::__construct();
        $this->basketUserID = CSaleBasket::GetBasketUserID();
        $this->CSaleOrder   = new CSaleOrder;
        $this->CSaleOrderPropsValue = new CSaleOrderPropsValue;
    }

    public static function GetInstance($a = false)
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL || $a === true ) {
            self::$instance = new BITEventOrder();
        }
        # возвращаем объект
        return self::$instance;
    }

    /**
     * Сохранение бонусов
     *
     * @param $ID
     * @param $arFields
     */
    public static function saveBonus($ID, $arFields)
    {

//! - реализовано в компоненте, но удалять нельзя на всякий случай
//        CModule::IncludeModule('bit_hl');
//
//        if($bonus = countBonus($ID))
//        {
//            // обновим в млм
//
//            \BIT\ORM\BITMlmOrder::add(array('UF_USER'=>$arFields['USER_ID'],'UF_DATE'=>time(),'UF_ORDER'=>$ID,'UF_BALL'=>$bonus));
//        }
    }

    public static function getProductOrder($ID = NULL)
    {
        $arReturn = array();
        CModule::IncludeModule('catalog');
        CModule::IncludeModule('sale');

        $dbBasketItems = CSaleBasket::GetList(
            array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
            array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "ORDER_ID" => $ID
            ),
            false,
            false,
            array("PRODUCT_ID","QUANTITY")
        );
        while ($arItems = $dbBasketItems->Fetch())
        {
            $arReturn[$arItems["PRODUCT_ID"]] = $arItems;
        }

        return (count($arReturn)>0)?$arReturn:false;
    }

    /**
     * Обновление LO и SO пользователя при оплате заказа
     *
     * @param $ID
     * @param $val
     * @return BITEventOrder|null
     */
    public static function UpdateLoSo($ID, $val)
    {
        CModule::IncludeModule('bit_hl');

        if($val == 'Y')
        {
            if($order = \BIT\ORM\BITMlmOrder::getList(array('filter'=>array('UF_ORDER'=>$ID)))->fetch())
            {
                if($user = \BIT\ORM\BITMlmAll::getList(array('filter'=>array('UF_USER'=>$order['UF_USER'])))->fetch())
                {
                    $ball = $order['UF_BALL'];
                    if($ball<1) return;
                    $lo   = $user['UF_LO'];
                    $so   = $user['UF_SO'];
                    $lo  += $ball;
                    $so  += $ball;

                    \BIT\ORM\BITMlmAll::update($user['ID'],array("UF_LO"=>$lo,"UF_SO"=>$so));
                    \BIT\ORM\BILogLo::add(array("UF_LO"=>$ball,"UF_SO"=>$ball,"UF_USER"=>$user['UF_USER'],"UF_DATE"=>time()));

                    $sponsor = $user['UF_SPONSOR'];

                    if($sponsor>0)
                    {
                        do
                        {
                            if($sp = \BIT\ORM\BITMlmAll::getList(array('filter'=>array('UF_USER'=>$sponsor)))->fetch())
                            {
                                $spSo = $sp['UF_SO'];
                                $spSo += $ball;
                                \BIT\ORM\BITMlmAll::update($sp['ID'],array("UF_SO"=>$spSo));
                                \BIT\ORM\BILogLo::add(array("UF_LO"=>$sp['UF_LO'],"UF_SO"=>$ball,"UF_USER"=>$sp['UF_USER'],"UF_DATE"=>time()));
                                $sponsor = $sp['UF_SPONSOR'];
                            }
                            else
                            {
                                $sponsor = false;
                            }
                        }
                        while($sponsor>0);
                    }
                }
            }
        }
    }

    /**
     * Обновление пользователя при оплате заказа
     *
     * @param $ID
     * @param $val
     * @return BITEventOrder|null
     */
    public static function payUpdateUserGroup($ID, $val)
    {
        CModule::IncludeModule('sale');
        CModule::IncludeModule('catalog');
        CModule::IncludeModule('bit_hl');

        $user  = new CUser();
        $group = array();

        if($order = \BIT\ORM\BITMlmOrder::getList(array('filter'=>array('UF_ORDER'=>$ID)))->fetch())
        {
            if($order['UF_BALL']<250) return;

            $status   = \BIT\ORM\BITLogStatus::getList(array('filter'=>array('order'=>array('UF_DATE' =>'DESC'),'filter' => array('UF_USER'=>$order['UF_USER'], 'UF_ORDER'=>$ID),'limit'=>1)))->fetch();
            $arGroups = $user->GetUserGroup($order['UF_USER']);

            // оплата совершена
            if($val == 'Y' && ($status['UF_STATUS'] == 'U' || !$status['UF_STATUS']))
            {
                // Бизнес партнер
                if($order['UF_BALL']>=250 && $order['UF_BALL']<500 && $order['UF_USER'])
                {
                    if(!array_search('14',$arGroups))
                    {
                        $group[] = 14;
                        $user->Update($order['UF_USER'],array('GROUP_ID'=>$group));
                        // занесем в лог
                        if($mlm = \BIT\ORM\BITMlmAll::getList(array('filter'=> array('UF_USER'=>$order['UF_USER'])))->fetch())
                        {
                            \BIT\ORM\BITMlmAll::update($mlm['ID'],array('UF_SATTE'=>'B'));
                            \BIT\ORM\BITLogStatus::add(array('UF_USER'=>$order['UF_USER'],'UF_STATUS'=>'B','UF_DATE'=>time(),'UF_ORDER'=>$ID));
                        }
                    }
                }

                // super Бизнес партнер
                elseif($order['UF_BALL']>=500 && $order['UF_USER'])
                {
                    if(!array_search('16',$arGroups))
                    {
                        if(!array_search('14',$group)) $group[] = 14;
                        $group[] = 16;
                        $user->Update($order['UF_USER'],array('GROUP_ID'=>$group));

                        // занесем в лог
                        if($mlm = \BIT\ORM\BITMlmAll::getList(array('filter'=> array('UF_USER'=>$order['UF_USER'])))->fetch())
                        {
                            \BIT\ORM\BITMlmAll::update($mlm['ID'],array('UF_SATTE'=>'S'));
                            \BIT\ORM\BITLogStatus::add(array('UF_USER'=>$order['UF_USER'],'UF_STATUS'=>'S','UF_DATE'=>time(),'UF_ORDER'=>$ID));
                        }
                    }
                }
            }

            // оплата отклонена
            elseif($val == 'N'  && ($status['UF_STATUS'] != 'U' || !$status['UF_STATUS']))
            {
                // Бизнес партнер
                $update = array();
                foreach($arGroups as $group)
                {
                    if($group == 14 || $group == 16) continue;
                    $update[] = $group;
                }
                $user->Update($order['UF_USER'],array('GROUP_ID'=>$update));
                // занесем в лог
                \BIT\ORM\BITLogStatus::add(array('UF_USER'=>$order['UF_USER'],'UF_STATUS'=>'U','UF_DATE'=>time(),'UF_ORDER'=>$ID));
            }
        }
    }

    /**
     * Обновление пользователя при оплате заказа
     *
     * @param $ID
     * @param $val
     * @return BITEventOrder|null
     */
    public static function cancelUpdateUserGroup($ID, $val)
    {
        CModule::IncludeModule('sale');
        CModule::IncludeModule('catalog');
        CModule::IncludeModule('bit_hl');

        $user  = new CUser();
        $group = array();

        if($order = \BIT\ORM\BITMlmOrder::getList(array('filter'=>array('UF_ORDER'=>$ID)))->fetch())
        {
            if($order['UF_BALL']<250) return;

            $status   = \BIT\ORM\BITLogStatus::getList(array('filter'=>array('order' => array('UF_DATE' =>'DESC'),'filter'=>array('UF_USER'=>$order['UF_USER'], 'UF_ORDER'=>$ID),'limit'=>1)))->fetch();
            $arGroups = $user->GetUserGroup($order['UF_USER']);

            // Заказ не отменен
            if($val == 'N' && ($status['UF_STATUS'] == 'U' || !$status['UF_STATUS']))
            {
                // Бизнес партнер
                if($order['UF_BALL']>=250 && $order['UF_BALL']<500 && $order['UF_USER'])
                {
                    if(!array_search('14',$arGroups))
                    {
                        $group[] = 14;
                        $user->Update($order['UF_USER'],array('GROUP_ID'=>$group));
                        // занесем в лог
                        \BIT\ORM\BITLogStatus::add(array('UF_USER'=>$order['UF_USER'],'UF_STATUS'=>'B','UF_DATE'=>time(),'UF_ORDER'=>$ID));
                    }
                }
                // super Бизнес партнер
                elseif($order['UF_BALL']>=500 && $order['UF_USER'])
                {
                    if(!array_search('16',$arGroups))
                    {
                        if(!array_search('14',$group))$group[] = 14;
                        $group[] = 16;
                        $user->Update($order['UF_USER'],array('GROUP_ID'=>$group));

                        // занесем в лог
                        \BIT\ORM\BITLogStatus::add(array('UF_USER'=>$order['UF_USER'],'UF_STATUS'=>'S','UF_DATE'=>time(),'UF_ORDER'=>$ID));
                    }
                }
            }
            // заказ отменен
            elseif($val == 'Н'  && ($status['UF_STATUS'] != 'U' || !$status['UF_STATUS']))
            {
                // Бизнес партнер
                $update = array();
                foreach($arGroups as $group)
                {
                    if($group == 14 || $group == 16) continue;
                    $update[] = $group;
                }
                $user->Update($order['UF_USER'],array('GROUP_ID'=>$update));
                // занесем в лог
                \BIT\ORM\BITLogStatus::add(array('UF_USER'=>$order['UF_USER'],'UF_STATUS'=>'U','UF_DATE'=>time(),'UF_ORDER'=>$ID));
            }
        }
    }
}

# Класс хранит все операции с корзиной
class BITEventBasket extends BITEvent
{
    public function __construct()
    {
        parent::__construct();
        $this->basketUserID = parent::GetInstance()->CSaleBasket->GetBasketUserID();
    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL) {
            self::$instance = new BITEventBasket();
        }
        # возвращаем объект
        return self::$instance;
    }

    /**
     * Пересчет корзны
     *
     * @param $ID
     * @param $arFields
     */
    public  function reCurrency($ID, &$arFields)
    {
        global $DB;

        CModule::IncludeModule('sale');
        CModule::IncludeModule('currency');

        if(!$arFields['PRICE'])
        {
            $res = $DB->Query('SELECT * from b_sale_basket where ORDER_ID IS NULL AND ID = '.$ID);
            while($r = $res->Fetch())
            {
                $arFields['PRICE'   ] = $r['PRICE'];
                $arFields['CURRENCY'] = $r['CURRENCY'];
            }
        }

        if($arFields['CURRENCY'] != BIT_CURRENCY_ID)
        {
            $price = CCurrencyRates::ConvertCurrency($arFields['PRICE'],$arFields['CURRENCY'],BIT_CURRENCY_ID);

            $arFields['PRICE']    = round($price);
            $arFields['CURRENCY'] = BIT_CURRENCY_ID;
        }
    }
}

# Класс хранит все операции с пользователями
class BITEventUser extends BITEvent
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL) {
            self::$instance = new BITEventUser();
        }
        # возвращаем объект
        return self::$instance;
    }

    /**
     * Проверка номера контракта
     *
     * @param $arFields
     * @return string
     */

    public static function checkContract(&$arFields)
    {

    }
    /**
     * Устанавливаем полное имя
     *
     * @param $arFields
     * @return string
     */

    public static function setFullName($arFields)
    {
        $fullName = "";
        if($arFields["NAME"])
        {
            $fullName .= $arFields['NAME'];
        }

        if($arFields["LAST_NAME"])
        {
            $fullName .= " ".$arFields['LAST_NAME'];
        }

        $fullName = trim($fullName);

        if($arFields["SECOND_NAME"])
        {
            $fullName .= " ".$arFields['SECOND_NAME'];
        }

        return $fullName;
    }

    /**
     * Проверяем наличие такого юзера
     *
     * @param $arFields
     * @return bool
     */
    public static function checkExist(&$arFields)
    {
        $_ar = CUser::GetList(
            ($by="id"), ($order="desc"),
            array(
                'EMAIL'             =>$arFields['EMAIL'],
                'NAME'              =>$arFields['NAME'],
                'LAST_NAME'         =>$arFields['LAST_NAME'],
                'SECOND_NAME'       =>$arFields['SECOND_NAME'],
                'PERSONAL_BIRTHDAY' =>$arFields['PERSONAL_BIRTHDAY'],
            )
        );
        if($_a = $_ar->Fetch())
        {
            $GLOBALS['APPLICATION']->ThrowException('Пользователь с данной комбинацией e-mail, ФИО и даты рождения уже существует.');
            return false;
        }
    }

    /**
     * Проверяем параметры
     *
     * @param $arFields
     * @return bool
     */
    public static function setParams(&$arFields)
    {
        # установим группу покупателя
        $arFields['GROUP_ID']     = self::setGroup($arFields);
        $arFields['UF_NAME_FULL'] = self::setFullName($arFields);
        # проверим спонсора
        if($sponsor = self::checkSponsor($arFields))
        {
            self::setSponsor($sponsor);
        }
        else
        {
            $GLOBALS['APPLICATION']->ThrowException('Данного спонсора не существует');
            return false;
        }
        # проверим UF_NUMBER_CONTRACT
        if($regId = self::setRegId($arFields))
        {
            $arFields['UF_NUMBER_CONTRACT'] = $regId;
        }
        else
        {
            $GLOBALS['APPLICATION']->ThrowException('Невозможно установить регистрационный номер, обратитесь к администратору сайта.');
            return false;
        }
    }

    /**
     * Добавление уникальной ссылки аффилиатов
     *
     * @param $arFields
     */
    public static function addAffiliateHref(&$arFields)
    {
        CModule::IncludeModule('bit_hl');
        \BIT\ORM\BITAffiliate::setUniqueHref($arFields['ID'],'','',true);
    }

    /**
     * Добавление в статистику аффилиатов
     *
     * @param $arFields
     */
    public static function add2AffiliateStatistic(&$arFields)
    {
        CModule::IncludeModule('bit_hl');
        \BIT\ORM\BITAffiliate::setDateRegister();
    }

    /**
     * Добавление связи с пользователем
     *
     * @param $arFields
     */
    public static function add2MLM(&$arFields)
    {
        if(intval($arFields['UF_SPONSOR'])>0 && intval($arFields['ID'])>0)
        {
            CModule::IncludeModule('bit_hl');
            if(!$arFields['UF_SPONSOR'])
            {
                $add = array(
                    'UF_USER'    =>$arFields['ID'],
                    'UF_SPONSOR' => "-1",
                    'UF_STATE'   =>'U',
                    'UF_LO'      => 0,
                    'UF_SO'      => 0,
                    'UF_MLM'     => 'N',
                    'UF_FOND'    => 'N',
                    'UF_LO_1'    => "-1",
                    'UF_LO_2'    => "-1",
                    'UF_LO_3'    => "-1",
                    "UF_LEVEL"   => 1
                );
                \BIT\ORM\BITMlmAll::add($add);
            }
            else
            {
                if($sponsor = \BIT\ORM\BITMlmAll::getList(array('filter'=>array('UF_USER'=>$arFields['UF_SPONSOR'])))->fetch())
                {
                    $lev = $sponsor['UF_LEVEL']+1;
                    $add = array(
                        'UF_USER'    => $arFields['ID'],
                        'UF_SPONSOR' => $arFields['UF_SPONSOR'],
                        'UF_STATE'   =>'U',
                        'UF_LO'      => 0,
                        'UF_SO'      => 0,
                        'UF_MLM'     => 'N',
                        'UF_FOND'    => 'N',
                        'UF_LO_1'    => $arFields['UF_SPONSOR'],
                        'UF_LO_2'    => $sponsor['UF_LO_1'],
                        'UF_LO_3'    => $sponsor['UF_LO_2'],
                        "UF_LEVEL"   => $lev
                    );
                    \BIT\ORM\BITMlmAll::add($add);
                }
            }
         }
    }

    /**
     * Добавление связи с пользователем
     *
     * @param $arFields
     */
    public static function createAccount(&$arFields)
    {

        CModule::IncludeModule('catalog');
        CModule::IncludeModule('sale');

        if(intval($arFields['ID'])>0)
        {
            if(BIT_CURRENCY_ID)
            {
                // Создание накопительного счета
                BITEventAccount::createCopyAccount($arFields['ID']);
                // Создаем персональный счет в текущей валюте
                BITEventAccount::createPersonalAccount($arFields['ID'],BIT_CURRENCY_ID);
                BITEventAccount::createBonusAccount($arFields['ID']);

                if(array_search('16',$arFields['GROUP_ID']))
                {
                    BITEventAccount::createFondAccount($arFields['ID']);
                }
            }
        }
    }

    /**
     * Устанавливаем группы юзера
     *
     * @param $arFields
     * @return bool
     */
    public static function setGroup($arFields)
    {
        # установим группу покупателя
        $group = $arFields['GROUP_ID'];
        $group = array_merge($group,array(13));  // УП

        if(strtolower($arFields['UF_SPOS_REG']) == 'bp')
        {
            $group = array_merge($group,array(14));
        }

        return $group;
    }

    /**
     * Проверяем наличие такого спонсора
     *
     * @param $arFields
     * @return bool
     */
    public static function checkSponsor($arFields)
    {
        # установим группу покупателя
        $sponsor = $arFields['UF_SPONSOR'];
        if($_s = BITUser::GetInstance()->getUserByID($sponsor))
        {
            return $sponsor;
        }
        return false;
    }

    /**
     * Устанавливаем связь спонсора
     *
     * @param $arFields
     * @return bool
     */
    public static function setSponsor($arFields)
    {
    }

    /**
     * Установка UF_NUMBER_CONTRACT
     *
     * @return bool
     */
    public static function setRegId()
    {
        global $DB;

        $regMax = 6999999;
        $regMin = 5000000;
        $regSet = false;

        $query  = 'SELECT ID from b_user ORDER BY ID DESC LIMIT 1';

        if($lastUser = $DB->Query($query, false, 'Error find last User ID on line '.__LINE__)->Fetch())
        {
            if($_lastUser = BITUser::GetInstance()->getUserByID($lastUser['ID']))
            {
                if($_lastUser['UF_NUMBER_CONTRACT'])
                {
                    $regSet = ++$_lastUser['UF_NUMBER_CONTRACT'];
                }
            }
        }
        if(intval($regSet)>0 && $regSet>$regMax)
        {
            $regSet = false;
        }
        if($regSet < $regMin)
        {
            $regSet = $regMin;
        }

        return $regSet;
    }

    /**
     * updateCache
     *
     * @param $arFields
     */

    public static function updateCache(&$arFields)
    {
        BITUser::clearCache($arFields['ID']);
    }

    /**
     * Обновление финансового пароль
     *
     * @param $arFields
     * @return bool
     */
    public function  updatePassFin(&$arFields)
    {
        if(isset($arFields['UF_PASS_FIN']) && strlen($arFields['UF_PASS_FIN'])>0)
        {
            $arr = array(
                'EMAIL'    => $arFields['EMAIL'],
                'PASS_FIN' => $arFields['UF_PASS_FIN']
            );
            CEvent::Send("UPDATE_PASS_FIN", "s1", $arr);
        }
    }

    /**
     * Обновление спонсора пользователя
     * Обновление пирамиды
     *
     * @param $arFields
     * @return bool
     */
    public function  updateSponsor(&$arFields)
    {
        if(!$arFields['UF_SPONSOR']) return;

        CModule::IncludeModule('bit_hl');
        if($user = \BIT\ORM\BITMlmAll::getList(array('filter'=>array('UF_USER'=>$arFields['ID'])))->fetch())
        {
            if($user['UF_SPONSOR'] != $arFields['UF_SPONSOR'])
            {
                \BIT\ORM\BITMlmAll::update($user['ID'],array("UF_SPONSOR"=>$arFields['UF_SPONSOR']));
                BITMlmUpdate::updatePyramid();
                \BIT\ORM\BITLogSponsor::add(array("UF_USER"=>$arFields["ID"],"UF_SPONSOR"=>$arFields['UF_SPONSOR'],"UF_DATE"=>time()));
            }
        }
        else
        {
            self::add2MLM($arFields);
        }
    }

    /**
     * Обновление статуса пользователя
     * УП БП суперБП
     *
     * @param $arFields
     * @return bool
     */
    public function  updateStatus(&$arFields)
    {
        if(!$arFields['UF_SPONSOR']) return;

        CModule::IncludeModule('bit_hl');

        $s = 'U';

        if(array_search('14',$arFields['GROUP_ID']))     $s = 'B';
        elseif(array_search('16',$arFields['GROUP_ID'])) $s = 'S';

        if(strlen($s)>0)
        {
            if($user = \BIT\ORM\BITMlmAll::getList(array('filter'=>array('UF_USER'=>$arFields['ID'])))->fetch())
            {
                if(!$user['UF_STATE'] || $user['UF_STATE'] != $s)
                {
                    \BIT\ORM\BITMlmAll::update($user['ID'],array('UF_STATE'=>$s));

                    if($user['UF_STATE'] == 'U')
                    {
                        BITEventAccount::deleteAccount($arFields['ID'],'F');
                    }
                    if($user['UF_STATE'] == 'B')
                    {
                        BITEventAccount::deleteAccount($arFields['ID'],'F');
                        BITEventFond::updateFondNewBp($arFields['UF_SPONSOR']);
                    }
                    if($user['UF_STATE'] == 'S')
                    {
                        BITEventAccount::createFondAccount($arFields['ID']);
                        BITEventFond::updateFondNewBp($arFields['UF_SPONSOR']);
                    }
                }
            }
        }
    }

    /**
     * Обновление счетов
     *
     * @param $arFields
     * @return bool
     */
    public function  updateAccount(&$arFields)
    {
        CModule::IncludeModule('bit_hl');
    }

    /**
     * Добавление пользователя
     *
     * @param $arFields
     * @return bool
     */
    public function UserAdd(&$arFields)
    {
        global $APPLICATION;
        $return = true;
        $except = array();

        if($_REQUEST['reg_rent']=='Y')
        {
            if(is_set($arFields, "PERSONAL_COUNTRY") && strlen($arFields["PERSONAL_COUNTRY"])<=0)
            {
                if(BIT_LANGUAGE_ID=='ru')
                    $except[] = "Не указана страна<br>";
                else
                    $except[] = "Field 'country' is required<br>";
                $return = false;
            }

            if(is_set($arFields, "PERSONAL_STATE") && strlen($arFields["PERSONAL_STATE"])<=0)
            {
                if(BIT_LANGUAGE_ID=='ru')
                    $except[] = "Не указан регион<br>";
                else
                    $except[] = "Field 'region' is required<br>";
                $return = false;
            }

            if(is_set($arFields, "PERSONAL_CITY") && strlen($arFields["PERSONAL_CITY"])<=0)
            {
                if(BIT_LANGUAGE_ID=='ru')
                    $except[] = "Не указан город<br>";
                else
                    $except[] = "Field 'city' is required<br>";
                $return = false;
            }

            if(is_set($arFields, "PERSONAL_STREET") && strlen($arFields["PERSONAL_STREET"])<=0)
            {
                if(BIT_LANGUAGE_ID=='ru')
                    $except[] = "Не указан адрес<br>";
                else
                    $except[] = "Field 'address' is required<br>";
                $return = false;
            }
        }

        if(is_set($arFields, "PERSONAL_PHONE") && strlen($arFields["PERSONAL_PHONE"])<=0)
        {
            if(BIT_LANGUAGE_ID=='ru')
                $except[] = "Не указан телефон";
            else
                $except[] = "Field 'phone number' is required";
            $return = false;
        }

        if(!$return)
        {
            $APPLICATION->throwException(implode(' ',$except));
        }
        return $return;
    }
}

# Класс хранит все операции с стоимостями
class BITEventPrice extends BITEvent
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL)
        {
            self::$instance = new BITEventPrice();
        }
        # возвращаем объект
        return self::$instance;
    }

}

# Класс хранит все операции с элементами инфоблока
class BITEventSection extends BITEvent
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL)
        {
            self::$instance = new BITEventElement();
        }
        # возвращаем объект
        return self::$instance;
    }
}

# Класс хранит все операции с элементами инфоблока
class BITEventElement extends BITEvent
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL)
        {
            self::$instance = new BITEventElement();
        }
        # возвращаем объект
        return self::$instance;
    }

    /**
     * Добавляет в календарь торговое предложение
     * в том случае, если у него есть заполненные даты
     *
     * addTpToCalendar
     *
     * @param $arFields
     * @return bool
     */
    public static function checkModerate(&$arFields)
    {
        if(
            $arFields['IBLOCK_ID'] == 22 ||
            $arFields['IBLOCK_ID'] == 23 ||
            $arFields['IBLOCK_ID'] == 26 ||
            $arFields['IBLOCK_ID'] == 25
        )
        {
            $result = CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID'], array("sort" => "asc"), Array("CODE"=>"MODERATION"));
            if($res = $result->Fetch())
            {
                $el = new CIBlockElement();

                if($res['VALUE_ENUM']=='Да' && $arFields['ACTIVE'] == "Y")
                {
                    self::sendModerate($arFields);
                    $el->Update($arFields['ID'],array('ACTIVE'=>'N'));
                }
                elseif($res['VALUE_ENUM']=='Да')
                {
                    self::sendModerate($arFields);
                }
            }
        }
    }

    public static function sendModerate($field)
    {
        $mail='';
        $arSend = array(
            'NAME' => $field['NAME'],
            'EDIT' => SITE_SERVER_NAME.'/bitrix/admin/iblock_element_edit.php?IBLOCK_ID='.$field['IBLOCK_ID'].'&type=content&ID='.$field['ID'].'&lang=ru&find_section_section=-1&WF=Y',
        );
        $filter = array("GROUPS_ID"=> Array(11));
        $rsUsers = CUser::GetList(($by="id"), ($order="asc"), $filter); // выбираем пользователей
        while($us = $rsUsers->Fetch())
        {
            if(strlen($mail)>0)
            {
                $mail .= ', '.$us['EMAIL'];
            }
            else
            {
                $mail = $us['EMAIL'];
            }
        }

        $arSend['MAIL'] = $mail;
        CEvent::Send("BIT_MODERATE", "s1", $arSend);
    }

}
# Класс хранит все операции с элементами инфоблока
class BITEventAccount extends BITEvent
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL) {
            self::$instance = new BITEventAccount();
        }
        # возвращаем объект
        return self::$instance;
    }

    /**
     * Обновление записи счетов при удалении
     *
     * @param $ID
     */
    public static function deleteFromMlm($ID)
    {
        CModule::IncludeModule('bit_hl');
        if($del = \BIT\ORM\BITMlmAccount::getList(array('filter'=>array('LOGIC' => array('OR' => array('UF_AC_COPY'=>$ID,'UF_AC_PERSONAL'=>$ID,'UF_AC_FOND'=>$ID,'UF_AC_BON'=>$ID)))))->fetch())
        {
            $arUpdate = array();
            if($del['UF_AC_COPY'] == $ID)         $arUpdate['UF_AC_COPY']     = NULL;
            elseif($del['UF_AC_PERSONAL'] == $ID) $arUpdate['UF_AC_PERSONAL'] = NULL;
            elseif($del['UF_AC_FOND'] == $ID)     $arUpdate['UF_AC_FOND']     = NULL;
            elseif($del['UF_AC_BON'] == $ID)     $arUpdate['UF_AC_BON']       = NULL;

            if(count($arUpdate)>0)
            {
                \BIT\ORM\BITMlmAccount::update($del['ID'],$arUpdate);
            }
        }
    }

    /**
     * Запись в счета при добавлении/обновлении
     *
     * @param $ID
     * @param $arFields
     */
    public static function writeToMlm($ID, $arFields)
    {
        if($arFields['CURRENCY'] == 'BAL')      $type = 'C';
        elseif($arFields['CURRENCY'] == 'FON' ) $type = 'F';
        elseif($arFields['CURRENCY'] == 'BON')  $type = 'B';
        else $type = 'P';

        self::setToMlm(array($arFields['USER_ID'],$ID,$type));
    }

    /**
     * Запишем в таблицу bit_mlm_account
     *
     * @param $add
     * UF_USER
     * ACCOUNT_ID
     * ACCOUNT_TYPE ('C|P|B|F')
     *
     * @return bool
     */
    public static function setToMlm($add = array('UF_USER'=>false,'ACCOUNT_ID'=>false,'ACCOUNT_TYPE'=>false))
    {
        CModule::IncludeModule('bit_hl');
        if(!$add['UF_USER'] || !$add['ACCOUNT_ID']|| !$add['ACCOUNT_TYPE']) return false;

        if($id = \BIT\ORM\BITMlmAccount::getList(array('filter'=>array('UF_USER'=>$add['UF_USER'])))->fetch())
        {
            // update
            $arUpdate = array();
            if($add['ACCOUNT_TYPE'] == 'C')       $arUpdate['UF_AC_COPY']     = $add['ACCOUNT_ID'];
            elseif($add['ACCOUNT_TYPE'] == 'P')   $arUpdate['UF_AC_PERSONAL'] = $add['ACCOUNT_ID'];
            elseif($add['ACCOUNT_TYPE'] == 'F' )  $arUpdate['UF_AC_FOND']     = $add['ACCOUNT_ID'];
            elseif($add['ACCOUNT_TYPE'] == 'B')   $arUpdate['UF_AC_BON']    = $add['ACCOUNT_ID'];

            AddMessage2Log($id);
            \BIT\ORM\BITMlmAccount::update($id['ID'],$arUpdate);
        }

        else
        {
            //add
            $arrAdd = array();
            $arrAdd['UF_USER'] = $add['UF_USER'];

            if($add['ACCOUNT_TYPE'] == 'C')     $arrAdd['UF_AC_COPY']     = $add['ACCOUNT_ID'];
            elseif($add['ACCOUNT_TYPE'] == 'P') $arrAdd['UF_AC_PERSONAL'] = $add['ACCOUNT_ID'];
            elseif($add['ACCOUNT_TYPE'] == 'F') $arrAdd['UF_AC_FOND']     = $add['ACCOUNT_ID'];
            elseif($add['ACCOUNT_TYPE'] == 'B') $arrAdd['UF_AC_BON']    = $add['ACCOUNT_ID'];

            \BIT\ORM\BITMlmAccount::add($arrAdd);
        }

    }

    /**
     * Создание персонального счета
     *
     * @param $userId
     * @param $currency
     */
    public static function createPersonalAccount($userId, $currency = BIT_CURRENCY_ID)
    {
        CModule::IncludeModule('catalog');
        CModule::IncludeModule('sale');
        CModule::IncludeModule('bit_hl');

        if($accounts  = \BIT\ORM\BITMlmAccount::getList(array('filter'=>array('UF_USER'=>$userId)))->fetch())
        {
            if(intval($accounts['UF_AC_PERSONAL'])>0) return;
        }
        // Создаем персональный счет в текущей валюте
        $Add = Array("USER_ID" => $userId, "CURRENCY" => $currency, "CURRENT_BUDGET" => 0);
        if($id = CSaleUserAccount::Add($Add))
        {
            self::setToMlm(array('UF_USER'=>$userId,'ACCOUNT_ID'=>$id,'ACCOUNT_TYPE'=>'P'));
        }
    }

    /**
     * Создание бонусного счета
     *
     * @param $userId
     */
    public static function createBonusAccount($userId)
    {
        CModule::IncludeModule('catalog');
        CModule::IncludeModule('sale');
        CModule::IncludeModule('bit_hl');

        $currency = 'BON';

        if($find = CSaleUserAccount::GetList(array(), array( "USER_ID"=>$userId,"CURRENCY" => $currency), false, false, array("ID", "USER_ID"))->Fetch())
        {
            return;
        }

        // Создаем бонусный счет в текущей валюте
        $Add = Array("USER_ID" => $userId, "CURRENCY" => $currency, "CURRENT_BUDGET" => 0);
        if($id = CSaleUserAccount::Add($Add))
        {
            self::setToMlm(array('UF_USER'=>$userId,'ACCOUNT_ID'=>$id,'ACCOUNT_TYPE'=>'B'));
        }
    }

    /**
     * Создание счета фонда
     *
     * @param $userId
     */
    public static function createFondAccount($userId)
    {
        CModule::IncludeModule('catalog');
        CModule::IncludeModule('sale');
        CModule::IncludeModule('bit_hl');

        if($accounts  = \BIT\ORM\BITMlmAccount::getList(array('filter'=>array('UF_USER'=>$userId)))->fetch())
        {
            if(intval($accounts['UF_AC_FOND'])>0) return;
        }

        // Создаем бонусный счет в текущей валюте
        $Add = Array("USER_ID" => $userId, "CURRENCY" => "FON", "CURRENT_BUDGET" => 0);
        if($id = CSaleUserAccount::Add($Add))
        {
            self::setToMlm(array('UF_USER'=>$userId,'ACCOUNT_ID'=>$id,'ACCOUNT_TYPE'=>'F'));
            \BIT\ORM\BITLogFond::add(array('UF_USER'=>$userId,'UF_ACCOUNT'=>$id,'UF_DATE'=>(time()+15552000)));
        }
    }


    /**
     * Создание накопительного счета
     *
     * @param $userId
     */
    public static function createCopyAccount($userId)
    {
        CModule::IncludeModule('catalog');
        CModule::IncludeModule('sale');
        CModule::IncludeModule('bit_hl');

        if($accounts  = \BIT\ORM\BITMlmAccount::getList(array('filter'=>array('UF_USER'=>$userId)))->fetch())
        {
            if(intval($accounts['UF_AC_COPY'])>0) return;
        }

        // Создаем бонусный счет в текущей валюте
        $Add = Array("USER_ID" => $userId, "CURRENCY" => "BAL", "CURRENT_BUDGET" => 0);
        if($id = CSaleUserAccount::Add($Add))
        {
            self::setToMlm(array('UF_USER'=>$userId,'ACCOUNT_ID'=>$id,'ACCOUNT_TYPE'=>'C'));
        }
    }

    /**
     * Удаление счета
     *
     * @param $userId
     * @param $type
     */
    public static function deleteAccount($userId,$type)
    {
        CModule::IncludeModule('catalog');
        CModule::IncludeModule('sale');
        CModule::IncludeModule('bit_hl');

        if($accounts  = \BIT\ORM\BITMlmAccount::getList(array('filter'=>array('UF_USER'=>$userId)))->fetch())
        {
            $accountId = '';
            $deleteId  = false;

            if($type == 'C')     {$deleteId  = $accounts['UF_AC_COPY'];                      $accountId = 'UF_AC_COPY';}
            elseif($type == 'P') {$deleteId  = $accounts['UF_AC_PERSONAL'];                  $accountId = 'UF_AC_PERSONAL';}
            elseif($type == 'F') {$deleteId  = $accounts['UF_AC_FOND'];                      $accountId = 'UF_AC_FOND';}
            elseif($type == 'B') {$deleteId  = $accounts['UF_AC_BON'];                       $accountId = 'UF_AC_BON';}

            if($deleteId !== false && strlen($accountId)>0)
            {
                CSaleUserAccount::Delete($deleteId);
                \BIT\ORM\BITMlmAccount::update($accounts['ID'],array("$accountId"=>NULL));
            }

            # удаление фонда
            if($type == 'F')
            {
                if($fond = \BIT\ORM\BITLogFond::getList(array('filter'=>array('UF_ACCOUNT'=>$deleteId)))->fetch())
                {
                    \BIT\ORM\BITLogFond::delete($fond['ID']);
                }
            }
        }
    }
}
# Класс хранит все операции с элементами инфоблока
class BITEventFond extends BITEvent
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL) {
            self::$instance = new BITEventFond();
        }
        # возвращаем объект
        return self::$instance;
    }

    public static function updateFondNewBp($sponsor)
    {
        CModule::IncludeModule('bit_hl');
        if($fond = \BIT\ORM\BITLogFond::getList(array('filter'=>array('UF_USER'=>$sponsor)))->fetch())
        {
            $c = 1;
            if(intval($fond['UF_BP'])>0) $c = $fond['UF_BP'];
            ++$c;

            \BIT\ORM\BITLogFond::update($fond['ID'],array('UF_BP'=>$c));
        }
    }
}


?>