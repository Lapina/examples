<?php
/**
 * Author : Lapina Anastasia
 * Created: 13.11.14 18:53
*/


class BITCsv
{
    public static function createCsv($fileName,$value)
    {
        file_put_contents($fileName,'');
        
        foreach($value as $val)
        {
            if(is_array($val))
            {
                $str = implode($val,";");
                $str = iconv('utf8','cp1251',$str);
                $str .= PHP_EOL;
                file_put_contents($fileName,$str,FILE_APPEND);
            }
        }
    }
}