<?php
/**
 * Author : Lapina Anastasia
 * Created: 11.08.14 19:07
*/

class BITLoad
{
    protected static $instance = NULL; # экземпляр класса

    protected $iblock = array();

    public function __construct()
    {
        $this->iblock = array(
            'news' => array(
                'ru' => '', # русский
                'en' => '', # Английский
                'cz' => '', # Чешский
                'pl' => '', # Польский
                'bg' => '', # Болгарский
                'hu' => '', # Венгерский
                'es' => '', # Испанский
                'tr' => '', # Турецкий
            )
        );
    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL) {
            self::$instance = new BITLoad();
        }
        # возвращаем объект
        return self::$instance;
    }

    /**
     * Конфигуратор всея сайта при загрузке
     */
    public static function config()
    {
        if(stristr($_SERVER['SCRIPT_URL'],'/bitrix')) return;
        if(stristr($_SERVER['SCRIPT_URL'],'/_mlm')) return;
        self::configDomen();
    }

    /**
     * Конфигурация домена: язык и сайт
     */
    public static function configDomen()
    {
//        unset($_SESSION);
//        die();
        BITLoadCountry::config();
        BITLoadLanguage::config();
        BITLoadSite::config();
        BITLoadCurrency::config();
        self::setDefine();

/*        if($_REQUEST['reload'] == 'Y')
        {
            BITLoadCurrency::reloadBasket();
        }*/
    }

    public static function setDefine()
    {
        if(!stristr($_SERVER['SCRIPT_URL'],'/bitrix'))
        {
            if($_SESSION["BIT_LANGUAGE_HL"]) define(BIT_LANGUAGE_HL,$_SESSION["BIT_LANGUAGE_HL"]);
            if($_SESSION["BIT_LANGUAGE_ID"]) define(BIT_LANGUAGE_ID,$_SESSION["BIT_LANGUAGE_ID"]);
            if($_SESSION["BIT_SITE_ID"])     define(BIT_SITE_ID,$_SESSION["BIT_SITE_ID"]);
            if($_SESSION["BIT_COUNTRY_ID"])  define(BIT_COUNTRY_ID,$_SESSION["BIT_COUNTRY_ID"]);
            if($_SESSION["BIT_CURRENCY_ID"]) define(BIT_CURRENCY_ID,$_SESSION["BIT_CURRENCY_ID"]);
        }
    }

}

class BITLoadCountry extends BITLoad
{
    protected static $instance = NULL; # экземпляр класса

    public function __construct()
    {
    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL) {
            self::$instance = new BITLoadCountry();
        }
        # возвращаем объект
        return self::$instance;
    }

    public static function config()
    {
        if(!$_SESSION["BIT_COUNTRY_ID"] || intval($_SESSION["BIT_COUNTRY_ID"])<1)
        {
            $country = self::GetInstance()->getCountryID();

            self::GetInstance()->save($country);
        }
    }

    /**
     * Сохранение страны
     *
     * @param int $country
     */
    public  static function save($country = 154)
    {
        $_SESSION["BIT_COUNTRY_ID"] = $country;
    }
    /**
     * Получение страны
     *
     * @return bool|int
     */
    public static function getCountryID()
    {
        return BITUser::GetInstance()->getUserCountry();
    }

    /**
     * Функция обновления страны на сайте
     *
     * @param $country
     */
    public static  function updateCountry($country)
    {
        CModule::IncludeModule("bit_hl");

        if(BITUser::GetInstance()->userId)
        {
            $u = new CUser();
            $u->Update(BITUser::GetInstance()->userId,array('UF_COUNTRY'=>$country));
        }


        $result = \BIT\ORM\BITSites::getList(array());
        while($res = $result->fetch())
        {
            $countryList = unserialize($res['UF_COUNTRY_ID']);

            foreach($countryList as $c)
            {
                if($c == $country)
                {
                    $_SESSION["BIT_SITE_ID"]    = $res['ID'];
                    $_SESSION["BIT_SITE_URL"]   = $res['UF_DOMEN'];
                    $_SESSION["BIT_COUNTRY_ID"] = $country;
                }
            }
        }

        $_SESSION['BIT_COUNTRY_ID'] = $country;
        BITLoadSite::reConfig($_SESSION["BIT_SITE_ID"], $_SESSION["BIT_SITE_URL"], $_SESSION["BIT_COUNTRY_ID"]);
    }
}


class BITLoadSite extends BITLoad
{
    protected static $instance = NULL; # экземпляр класса

    public function __construct()
    {
    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL) {
            self::$instance = new BITLoadSite();
        }
        # возвращаем объект
        return self::$instance;
    }

    /**
     * Конфигурация сайта
     */
    public static  function config()
    {
        CModule::IncludeModule("bit_hl");
        if($_REQUEST['ID_SITE'])
        {
            global $APPLICATION;

            self::GetInstance()->save($_REQUEST['ID_SITE']);

            BITLoadCountry::GetInstance()->save($_REQUEST['ID_COUNTRY']);
            BITLoadLanguage::GetInstance()->save($_REQUEST['ID_LANG'],$_REQUEST['HL_LANG']);

            LocalRedirect($APPLICATION->GetCurDir()/*.'?reload=Y'*/);
        }

        # зашел первый раз
        if(!$_SESSION["BIT_SITE_ID"] || !$_SESSION["BIT_SITE_URL"])
        {
            self::currentSite();
        }
        # не первый раз
        else
        {
            if($_REQUEST['NO_SITE'] == 'Y') return;

            if($_SESSION["BIT_SITE_URL"] != $_SERVER['SERVER_NAME'])
            {
                self::GetInstance()->reload($_SESSION["BIT_SITE_ID"]);
            }
        }
    }
    /**
     * Конфигурация сайта
     */
    public static  function reConfig($siteId,$siteUrl = false,$country = false)
    {
        CModule::IncludeModule("bit_hl");
        self::GetInstance()->save($siteId,$siteUrl);
        self::GetInstance()->reload($siteId,$siteUrl,$country);
    }

    /**
     * Получение корректного сайта
     */
    public static function currentSite()
    {
        CModule::IncludeModule("bit_hl");

        $setDomen  = false;
        $result    = \BIT\ORM\BITSites::getList(array());
        while($res = $result->fetch())
        {
            $isCount = false;
            $country = unserialize($res['UF_COUNTRY_ID']);
            foreach($country as $c)
            {
                if($c ==  $_SESSION['BIT_COUNTRY_ID'])
                {
                    $isCount = true;
                }
            }

            if($isCount && ($res['UF_DOMEN'] != $_SERVER['SERVER_NAME']))
            {
                $setDomen = true;
                self::GetInstance()->save($res['ID'],$res['UF_DOMEN']);
                self::GetInstance()->reload($res['ID'],$res["UF_DOMEN"]);
                break;
            }

            if($isCount && ($res['UF_DOMEN'] == $_SERVER['SERVER_NAME']))
            {
                $setDomen = true;
                self::GetInstance()->save($res['ID'],$res['UF_DOMEN']);
                break;
            }
        }

        if(!$setDomen)
        {
            global $APPLICATION;
            LocalRedirect($APPLICATION->GetCurDir().'?NO_SITE=Y');
        }
    }

    /**
     * Перезагрузка
     *
     * @param $siteId
     * @param bool $siteUrl
     * @param $country
     */
    public function reload($siteId,$siteUrl = false,$country = BIT_COUNTRY_ID)
    {
        if($siteUrl === false)
        {
            $result = \BIT\ORM\BITSites::getById($siteId)->fetch();
            if($result)
            {
                $siteUrl = $result['UF_DOMEN'];
            }
        }

        $url = str_replace($_SERVER['SERVER_NAME'],$siteUrl,$_SERVER['SCRIPT_URI']);
        $url .= "?";
        if($_GET)
        {
            foreach($_GET as $k=>$v)
            {
                $url .= "&$k=$v";
            }
        }

        $url .= '&ID_SITE='.$siteId.'&HL_LANG='.BIT_LANGUAGE_HL.'&ID_LANG='.BIT_LANGUAGE_ID.'&ID_COUNTRY='.$country;
        LocalRedirect($url);
    }

    /**
     * Сохранение сайта
     *
     * @param int $siteId
     * @param bool $siteUrl
     */
    public function save($siteId = 2, $siteUrl = false)
    {
        $_SESSION["BIT_SITE_ID"]  = $siteId;
        if($siteUrl === false)
        {
            $result = \BIT\ORM\BITSites::getById($siteId)->fetch();
            if($result)
            {
                $siteUrl = $result['UF_DOMEN'];
            }
        }

        $_SESSION["BIT_SITE_URL"] = $siteUrl;
    }

}

class BITLoadLanguage extends BITLoad
{
    protected static $instance = NULL; # экземпляр класса

    public function __construct()
    {

    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL) {
            self::$instance = new BITLoadLanguage();
        }
        # возвращаем объект
        return self::$instance;
    }

    /**
     *  Конфигуратор языка
     */
    public static  function config()
    {
        $id   = self::GetInstance()->getLangID();
        $lang = self::GetInstance()->getLang($id);
        self::GetInstance()->save($lang,$id);
    }

    /**
     * Сохраняем сессии и бновляем данные пользователя
     *
     * @param string $lang
     * @param int $id
     */
    public function save($lang = 'en',$id = 2)
    {
        if(BITUser::GetInstance()->userId)
        {
            if(strlen(!BITUser::GetInstance()->userArr['UF_LANG_ID'])<1 || BITUser::GetInstance()->userArr['UF_LANG_ID']!=$lang)
            {
                $user = new CUser();
                $user->Update(BITUser::GetInstance()->userId,array('UF_LANG_ID'=>$id));
            }
        }

        $_SESSION["BIT_LANGUAGE_HL"] = $id;
        $_SESSION["BIT_LANGUAGE_ID"] = $lang;
    }


    /**
     * Получаем название языка ru|en|..
     * @param $lang
     * @return bool
     */
    public function getLang($lang)
    {
        CModule::IncludeModule("bit_hl");

        if($lang!== false && strlen($lang)>0)
        {
            $result = \BIT\ORM\BITLang::getById($lang)->fetch();
            if($result['UF_LANG_ID'])
            {
                return $result['UF_LANG_ID'];
            }
        }

        return false;
    }

    /**
     * Получаем ID языка из HL
     *
     * @return bool|string
     */
    public function getLangID()
    {
        $lang = false;
        if(BITUser::GetInstance()->userArr['UF_LANG_ID'] && strlen(BITUser::GetInstance()->userArr['UF_LANG_ID'])>0)
        {
            $lang = BITUser::GetInstance()->userArr['UF_LANG_ID'];
        }
        # если у нас не указан язык
        if(!$lang || !isset($_SESSION["BIT_LANGUAGE_HL"]) || intval($_SESSION["BIT_LANGUAGE_HL"])<1)
        {

            if(!BITUser::GetInstance()->userArr['UF_LANG_ID'] || strlen(BITUser::GetInstance()->userArr['UF_LANG_ID'])<1)
            {
                # ищем страну и язык
                $country = BITLoadCountry::getCountryID();
                $result  = \BIT\ORM\BITCountry::getById($country)->fetch();
                $lang    = $result['UF_LANG_ID'];
            }
        }

        if(isset($_SESSION["BIT_LANGUAGE_HL"]) && intval($_SESSION["BIT_LANGUAGE_HL"])>0)
        {
            $lang = $_SESSION["BIT_LANGUAGE_HL"];
        }

        if($lang === false)
        {
            $lang = '2';
        }

        $_SESSION["BIT_LANGUAGE_HL"] = $lang;

        return $lang;
    }

    /**
     * Функция обновления языка на сайте
     *
     * @param $langId
     */
    public static  function updateLang($langId)
    {
        $arLang = BITConfigLang::getLangList();
        if(BITUser::GetInstance()->userId)
        {
            $u = new CUser();
            $u->Update(BITUser::GetInstance()->userId,array('UF_LANG_ID'=>$langId));
            BITUser::clearCache();
        }

        unset($_SESSION['BIT_LANGUAGE_HL']);
        unset($_SESSION['BIT_LANGUAGE_ID']);

        $_SESSION['BIT_LANGUAGE_HL']  = $langId;
        $_SESSION['BIT_LANGUAGE_ID']  = $arLang[$langId]['UF_LANG_ID'];

        global $APPLICATION;
        LocalRedirect($APPLICATION->GetCurPage());
    }
}


class BITLoadCurrency extends BITLoad
{
    protected static $instance = NULL; # экземпляр класса

    public function __construct()
    {

    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL) {
            self::$instance = new BITLoadCurrency();
        }
        # возвращаем объект
        return self::$instance;
    }

    /**
     *  Конфигуратор валюты
     */
    public static  function config()
    {
        $id = self::GetInstance()->getCurrID();

        if($_SESSION["BIT_CURRENCY_ID"] !== $id)
        {
            self::reloadBasket($id);
            self::GetInstance()->save($id);
        }
    }

    /**
     * Находим идентификатор валюты
     *
     * @return string
     */
    public function getCurrID()
    {
        $id = 'RUB';

        CModule::IncludeModule('currency');
        CModule::IncludeModule('bit_hl');

        if($site = \BIT\ORM\BITSites::getById($_SESSION['BIT_SITE_ID'])->fetch())
        {
            $id = $site['UF_CURRENCY'];
        }

        return $id;
    }

    /**
     * Сохраняем сессии
     *
     * @param string $id
     */
    public function save($id = 'RUB')
    {
        $_SESSION["BIT_CURRENCY_ID"] = $id;
    }

    /**
     * Конвертация корзины
     *
     */
    public static  function reloadBasket()
    {
//        convertBasket();
    }
}