<?php
/**
 * Author : Lapina Anastasia
 * Created: 12.08.14 15:43
*/

class BITUser
{
    public $userId  = NULL;
    public $userArr = array();

    protected static $instance = false;# экземпляр

    private $sessid   = NULL; # Id сессии

    public function __construct()
    {
        CModule::IncludeModule('bit_hl');

        $this->userId  = CUser::GetID();
        if($this->userId)
        {
            $this->collect();
        }
        else
        {
            return false;
        }
    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL) {
            self::$instance = new BITUser();
        }
        # возвращаем объект
        return self::$instance;
    }

    /**
     * Собираем коллекцию функций
     */
    private function collect()
    {
        $this->getSession();
        $this->getUser();
    }

    /**
     * Устанавливаем сессию
     */
    protected function getSession()
    {
        $this->sessid = bitrix_sessid();
    }

    /**
     * Получаем инфу о текущем юзере
     */
    protected function getUser()
    {
        $arrUser = BITCacheAPC::GetInstance()->getData("user_info_".$this->sessid.$this->userId);
        if($arrUser)
        {
            $this->userId  = $arrUser["ID"];
            $this->userArr = $arrUser;
        }
        else
        {
            $rsUser = CUser::GetByID(CUser::GetID());
            $arUser = $rsUser->Fetch();

            if($arUser)
            {
                $this->userId  = $arUser["ID"];
                $this->userArr = $arUser;

                BITCacheAPC::GetInstance()->setData("user_info_".$this->sessid.$this->userId,$arUser);
            }
        }
    }

    /**
     *  Очищения кэша пользователя
     *
     * @param bool $id
     * @param bool $sid
     * @return bool
     */
    public static function clearCache($id = false,$sid = false)
    {
        if($sid === false && self::GetInstance()->sessid) $sid = self::GetInstance()->sessid;
        if($id  === false && self::GetInstance()->userId) $id  = self::GetInstance()->userId;
        else return false;

        $rs = CSession::GetList(($by = "s_id"), ($order = "desc"), array('USER_ID'=>$id), $is_filtered);

        if ($ar = $rs->Fetch())
        {
            if($ar['USER_ID'] == $id)
            {
                $sid = $ar['ID'];
            }
        }

        BITCacheAPC::GetInstance()->delData('user_info_'.$sid.$id);
    }

    /**
     * Получаем инфу о юзере по ID
     *
     * @param $id
     * @return bool|mixed|null
     */
    public function getUserByID($id = false)
    {
        if($id === false && $this->userId) $id = $this->userId;
        if($id === false) return false;

        $arrUser = BITCacheAPC::GetInstance()->getData("user_info_".$this->sessid.$id);

        if($arrUser)
        {
            return $arrUser;
        }
        else
        {
            $rsUser = CUser::GetByID($id);
            $arUser = $rsUser->Fetch();
            if(count($arUser)>0)
            {
                BITCacheAPC::GetInstance()->setData("user_info_".$this->sessid.$id,$arUser);
            }

            return $arUser;
        }

        return false;
    }

    /**
     * Получаем инфу о списке юеров из массива
     *
     * @param $arr
     * @return array|bool|mixed|null
     */
    public function getUserByArray($arr)
    {
        if(count($arr)<1) return false;
        $_id = "";
        foreach($arr as $_a)
        {
            $_id .= $_a." | ";
        }
        $arrUsers = BITCacheAPC::GetInstance()->getData("user_list_".$this->sessid.$_id);

        if($arrUsers)
        {
            return $arrUsers;
        }
        else
        {
            $rsUser = CUser::GetList(
                ($by="name"), ($order="asc"),
                array(
                    "ID" => $_id
                )
            );
            $ret = array();
            while($arUser = $rsUser->GetNext())
            {
                $ret[$arUser["ID"]] = $arUser;
            }
            BITCacheAPC::GetInstance()->setData("user_list_".$this->sessid.$_id,$ret);

            return $ret;
        }

        return false;
    }

    /**
     * Страна юзера
     *
     * @param bool $id
     * @return bool|int
     */
    public function getUserCountry($id = false)
    {
        if($id  === false && self::GetInstance()->userId) $id  = self::GetInstance()->userId;

        $countryId = false;

        if(intval(BITUser::GetInstance()->userArr['UF_COUNTRY'])>0)
        {
            $countryId = BITUser::GetInstance()->userArr['UF_COUNTRY'];
        }
        else
        {
            # если не установлена страна, найдем и занесем ее
            $geoLocate = new BITGeo();

            if($geoLocate->USER_CURRENT_COUNTRY['ID'])
            {
                $geoCountry = $geoLocate->USER_CURRENT_COUNTRY['ID'];
                $findResult = \BIT\ORM\BITCountry::getList(array( 'filter' => array('UF_GEO_ID'=>$geoCountry)));

                if($_  = $findResult->fetch())
                {
                    $countryId = $_['ID'];
                    # обновленияе страны
                    if($id)
                    {
                        $user = new CUser();
                        $user->Update($id,array('UF_COUNTRY'=>$countryId));
                    }
                }
            }
        }

        if(!$countryId) $countryId = 154;

        return $countryId;
    }

}