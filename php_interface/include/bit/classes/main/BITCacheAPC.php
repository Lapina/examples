<?php
/**
 * Author : Lapina Anastasia
 * Created: 12.08.14 10:53
*/
class BITCacheAPC
{
    protected static $instance = false; # экземпляр

    protected $timeLive    = 60;     # время жизни
    protected $apcEnabled = false; # а включен ли APC


    public function __construct()
    {
        $this->apcEnabled = extension_loaded('apc');
    }

    public static function GetInstance()
    {
        # если переменная пустая, то инициализируем объект
        if (self::$instance == NULL) {
            self::$instance = new BITCacheAPC();
        }
        # возвращаем объект
        return self::$instance;
    }

    /**
     * Получаем данные с памяти
     *
     * @param $sKey
     * @return mixed|null
     */
    function getData($sKey)
    {
        $bRes  = false;
        $vData = apc_fetch($sKey, $bRes);

        return ($bRes) ? $vData :null;
    }

    /**
     * Сохраняем данные в память
     *
     * @param $sKey
     * @param $vData
     * @return array|bool
     */
    function setData($sKey, $vData)
    {
        return apc_store($sKey, $vData, $this->timeLive);
    }

    /**
     * Удаляем данные с памяти
     *
     * @param $sKey
     * @return bool|string[]
     */
    function delData($sKey)
    {
        $bRes = false;
        apc_fetch($sKey, $bRes);
        return ($bRes) ? apc_delete($sKey) : true;
    }
}
