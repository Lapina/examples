<?php
/**
 * Author : Lapina Anastasia
 * Created: 12.08.14 10:52
*/
use BIT\ORM;

class BITConfigLang
{
    /**
     * Обновление файла с языками
     */
    public static function updateLangFiles()
    {
        $arUpdate = array();
        $langList = self::getLangList();

        $result = ORM\BITMess::getList();
        while($res = $result->fetch())
        {
            $arUpdate[$res['UF_LANG_ID']][$res['UF_MESS']] = $res['UF_VALUE'];
        }

        foreach($langList as $lang)
        {
            self::includeLangFile($lang['UF_LANG_ID']);
            $file  = __DIR__."/".$lang['UF_LANG_ID']."/"."mess.php";
            $fp = fopen($file,"w");
            fwrite($fp, "<?");
            fwrite($fp, "\n");
            fwrite($fp, 'global $MESS;');
            fwrite($fp, "\n");

            foreach($arUpdate[$lang["ID"]] as $mess=>$update)
            {

                fwrite($fp, '$MESS["'.$mess.'"] = "'.$update.'";');
                fwrite($fp, "\n");
            }

            fclose($fp);
        }
    }

    /**
     * Получение списка языков, использованных на сайте
     *
     * @return array
     */
    public static function getLangList()
    {
        $arCountry = array();
        $lang      = array();
        $arLang    = array();
        $arReturn  = array();
        $cacheId   = 'getLangList'.BIT_LANGUAGE_ID;

        if($arr = BITCacheAPC::GetInstance()->getData($cacheId))
        {
            $arReturn = $arr;
        }
        else
        {
            $lang   = array();
            $result = ORM\BITSites::getList();
            while($res = $result->fetch())
            {
                $lang[$res['UF_LANG_ID']] = $res['UF_LANG_ID'];
                $country = unserialize($res['UF_COUNTRY_ID']);
                foreach($country as $c)
                {
                    $arCountry[$c] = $c;
                }
            }

            if(count($arCountry)<1) return;

            $result = ORM\BITCountry::getList(array(
                'filter' => array('ID'=>$arCountry)
            ));
            while($res = $result->fetch())
            {
                $arLang[$res['UF_LANG_ID']] = $res['UF_LANG_ID'];
            }

            if(count($arLang)<1) return;

            $arLang = array_merge($arLang,$lang);
            $arLang = array_unique($arLang);

            $result = ORM\BITLang::getList(array(
                'filter' => array('ID'=>$arLang)
            ));
            while($res = $result->fetch())
            {
                $arReturn[$res['ID']] = $res;
            }
            BITCacheAPC::GetInstance()->setData($cacheId,$arReturn);
        }

        return $arReturn;
    }

    /**
     * Подключение языкового файла
     * 
     * @param bool $lang
     */
    public static function includeLangFile($lang = false)
    {
        if($lang == false) $lang = $_SESSION['BIT_LANGUAGE_ID'];
        $_dir  = false;
        $_file = false;

        $dir   = __DIR__."/".$lang."/";
        $_dir  = is_dir($dir);
        $file  = $dir."mess.php";

        if($_dir === false)
        {
            mkdir($dir,0755);
            $_dir = is_dir($dir);
        }
        if($_dir !== false)
        {
            $_file = is_file($file);
            if($_file === false)
            {
                $fp = fopen($file,"a+");
                fwrite($fp, "<?");
                fwrite($fp, "\n");
                fwrite($fp, 'global $MESS;');
                fwrite($fp, "\n");
                fclose($fp);
            }
            $_file = is_file($file);
            if($_file !== false)
            {
                @include_once($file);
            }
        }
    }
}


