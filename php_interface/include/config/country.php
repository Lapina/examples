<?php
/**
 * Author : Lapina Anastasia
 * Created: 18.08.14 11:34
*/
use BIT\ORM;

class BITConfigCountry
{

    /**
     * Получение списка стран, использованных на сайте
     *
     * @return array
     */
    public static function getCountryList()
    {
        $arCountry = array();
        $arReturn  = array();
        $cacheId   = 'getCountryList';

        if($arr = BITCacheAPC::GetInstance()->getData($cacheId))
        {
            $arReturn = $arr;
        }
        else
        {
            $result = ORM\BITSites::getList();
            while($res = $result->fetch())
            {
                $country = unserialize($res['UF_COUNTRY_ID']);
                foreach($country as $c)
                {
                    $arCountry[$c] = $c;
                }
            }

            if(count($arCountry)<1) return;

            $result = ORM\BITCountry::getList(array(
                'filter' => array('ID'=>$arCountry)
            ));
            while($res = $result->fetch())
            {
                $arReturn[$res['ID']] = $res;
            }

            BITCacheAPC::GetInstance()->setData($cacheId,$arReturn);
        }

        return $arReturn;
    }
}


