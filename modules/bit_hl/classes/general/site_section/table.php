<?
namespace BIT\ORM;

use Bitrix\Main\Application;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use BIT\Fields;
use BIT\Cache;
use BIT\LIB;
Loc::loadMessages(__FILE__);


class BITSiteSectionsTable extends LIB\BTable
{
	const ENTITY_ID = 'HLBLOCK_6';
	const TABLE     = 'bit_site_sections';
}
?>