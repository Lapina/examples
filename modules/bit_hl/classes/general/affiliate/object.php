<?
namespace BIT\ORM;

class BITAffiliate extends BITAffiliateTable
{
    /**
     * Функция перенаправление
     *
     */
    public static function redirectUrl()
    {
        global $APPLICATION;
        $url = $APPLICATION->GetCurUri();

        if(strlen($url)>0)
        {
            if($key = self::getKey($url))
            {
                if($res = self::getUrl($key))
                {
                    if(self::setSession($res['UF_USER']))
                    {
                        self::setDateView($res['ID']);
                    }
                    LocalRedirect($res['UF_URL']);
                }
            }
        }

        return;
    }

    /**
     * Получение url
     * @param $url
     * @return bool|mixed
     */
    public static function getKey($url)
    {
        preg_match("/\/(\w|\d)+/i",$url,$res);

        if(strlen($res[0])>0)
        {
            $_url = str_replace('/','',$res[0]);
            return $_url;
        }

        return false;
    }

    /**
     * Получение url по ключу
     *
     * @param $key
     * @return mixed
     */
    public static function getUrl($key)
    {
        if($res = self::getList(array('filter'=>array('UF_KEY'=>$key)))->fetch())
        {
            if(strlen($res['UF_URL'])>0)
            {
                return $res;
            }
        }
    }

    /**
     * Заносим в лог статистики
     *
     * @param $id
     */
    public static function setDateView($id)
    {
        $date = strtotime(date('d.m.Y',time()));
        if($res = BITAffiliateStatistic::getList(array('filter'=>array('UF_DATE'=>$date,'UF_ID'=>$id)))->fetch())
        {
            $view = intval($res['UF_VIEW'])+1;
            BITAffiliateStatistic::update($res['ID'],array('UF_VIEW'=>$view));
        }
        else
        {
            BITAffiliateStatistic::add(array('UF_ID'=>$id,'UF_VIEW'=>1,'UF_REGISTER'=>0,'UF_DATE'=>$date));
        }
    }

    /**
     * Заносим в лог статистики при регистрации
     *
     */
    public static function setDateRegister()
    {
        if($_SESSION['bit_partner'])
        {
            $date = strtotime(date('d.m.Y',time()));
            if($user = self::getList(array('filter'=>array('UF_USER'=>$_SESSION['bit_partner'])))->fetch())
            {
                if($res = BITAffiliateStatistic::getList(array('filter'=>array('UF_DATE'=>$date,'UF_ID'=>$user['ID'])))->fetch())
                {
                    $view = intval($res['UF_REGISTER'])+1;
                    BITAffiliateStatistic::update($res['ID'],array('UF_REGISTER'=>$view));
                }
                else
                {
                    BITAffiliateStatistic::add(array('UF_ID'=>$user['ID'],'UF_VIEW'=>0,'UF_REGISTER'=>1,'UF_DATE'=>$date));
                }
            }

        }
    }

    /**
     * Создание уникальной ссылки в аффилиаты
     *
     */
    public static function setUniqueHref($userID = false, $url = '', $key = '',$register = false)
    {
        global $USER;

        if($userID == false) $userID = $USER->GetID();

        if($register == true && strlen($key)<1) $key = 'partner_'.$userID;
        if($register == true && strlen($url)<1) $url = '/registration/';

        if(strlen($key)<1) $key = self::getUniqueKey($userID,$url);

        if($key = self::checkKey($key))
        {

            // если есть такая ссылка и НЕ Регистрация
            if($register == false)
            {
                if($res = self::getList(array('filter'=>array('UF_USER' => $userID, 'UF_URL' => $url)))->fetch())
                {
                    return array('ERROR'=>'На данную страницу уже задана ссылка');
                }
                else
                {
                    if($r = self::add(array('UF_USER' => $userID, 'UF_URL' => $url, 'UF_KEY' => $key, 'UF_SITE' => BIT_SITE_ID)))
                    {
                        return array('RESULT'=>$r);
                    }
                }
            }
            //  если нет ссылки и $register
            elseif($register == true)
            {
                if($res = self::getList(array('filter'=>array('UF_USER' => $userID, 'UF_REGISTER' => $register)))->fetch())
                {
                    $newID = 0;

                    if($r = self::add(array('UF_USER' => $userID, 'UF_URL' => $res['UF_URL'], 'UF_KEY' => $key, 'UF_REGISTER' => $register, 'UF_PARENT' => 0, 'UF_SITE' => BIT_SITE_ID)))
                    {
                        $newID = $r->getId();
                    }

                    $_res = self::getList(array('filter'=>array('UF_USER' => $userID, 'UF_REGISTER' => $register,  '!ID' => $newID)));
                    while($_r = $_res->fetch())
                    {
                        if(intval($_r['UF_PARENT']) == 0)
                        {
                            if($update = self::update($_r['ID'],array('UF_PARENT' => $newID)))
                            {
                                return array('RESULT'=>$update);
                            }
                        }
                    }
                }
                else
                {
                    if($r = self::add(array('UF_USER' => $userID, 'UF_URL' => $url, 'UF_KEY' => $key, 'UF_REGISTER' => $register, 'UF_SITE' => BIT_SITE_ID)))
                    {
                        return array('RESULT'=>$r);
                    }
                }
            }
        }
        else
        {
            return array('ERROR' => 'Данный код уже существует');
        }

        return false;
    }

    /**
     * Получение уникальной ссылки в аффилиаты
     *
     */
    public static function getUniqueKey($userID = false, $url = '')
    {
        global $APPLICATION;
        global $USER;
        if(strlen($url)<1)   $url = $APPLICATION->GetCurUri();
        if($userID == false) $userID = $USER->GetID();
        if(stristr($url,'/registration/')) return;

        if($url != '/')
        {
            preg_match("/\/(\w|\d|\/)+/i",$url,$res);

            if(strlen($res[0])>0)
            {
                $key = substr($res[0],1);
                $key = str_replace('/','_',$key);
                $key .= $userID;
            }
        }
        else
        {
            $key = 'ref_'.$userID;
        }

        while($res = self::getList(array('filter'=>array('UF_KEY'=>$key)))->fetch())
        {
            $key .= rand(100,1000);
        }

        if(strlen($key)>0) return $key;

        return false;
    }

    /**
     * Получение уникальной ссылки в аффилиаты
     *
     */
    public static function checkUniqueHref($userID = false, $url = '')
    {
        if(strlen($url)<1)   return;
        if($userID == false) $userID = CUser::GetID();
        if(stristr($url,'/registration/')) return;

        preg_match("/\/(\w|\d|\/)+/i",$url,$res);

        if(strlen($res[0])>0)
        {
            $key = str_replace('/','_',$res[0]);
            $key .= $userID;
        }
        while($res = self::getList(array('filter'=>array('UF_KEY'=>$key)))->fetch())
        {
            $key .= rand(100,1000);
        }

        if(strlen($key)>0) return $key;

        return false;
    }

    /**
     * Получение уникальной ссылки в аффилиаты
     *
     */
    public static function checkKey($key)
    {
        if(strlen($key)<1)   return;

        preg_match("/(\w|\d)+/i",$key,$res);

        if(strlen($res[0])>0)
        {
            $key = str_replace('/','_',$res[0]);
        }
        if($res = self::getList(array('filter'=>array('UF_KEY'=>$key)))->fetch())
        {
            return false;
        }

        if(strlen($key)>0) return $key;

        return false;
    }


    /**
     * Создание уникальной ссылки в аффилиаты
     *
     */
    public static function getCurPage($userID = false, $url = '')
    {
        global $APPLICATION;
        global $USER;

        if($userID == false) $userID = $USER->GetID();
        if(strlen($url)<1)   $url = $APPLICATION->GetCurUri();

        if($res = self::getList(array('filter'=>array('UF_USER' => $userID, 'UF_URL' => $url, 'UF_REGISTER' => false)))->fetch())
        {
            return $res;
        }

        return false;
    }

    /**
     * Сохранение в сессию
     *
     * @param $user
     * @return bool
     */
    public static function setSession($user)
    {
        if(!$_SESSION['bit_partner'] || $_SESSION['bit_partner']!=$user)
            $_SESSION['bit_partner'] = $user;
        else
            return false;

        return true;
    }
}
?>