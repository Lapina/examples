<?
namespace BIT\ORM;

use Bitrix\Main\Application;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use BIT\Fields;
use BIT\LIB;

Loc::loadMessages(__FILE__);

class BITUserFeedbackTable extends LIB\BTable {

	const ENTITY_ID = 'HLBLOCK_9';
	const TABLE     = 'bit_user_feedback';

    #onBeforeAdd
    #onAfterAdd
    #onAfterUpdate
    #OnBeforeDelete
    #public static function *some*($Event)
    #{
    #    $id     = $Event->getParameter("id");
    #    $Params = $Event->getParameters();
    #    $Fields = $Params['fields'];
    #    return;
    #}

}
?>