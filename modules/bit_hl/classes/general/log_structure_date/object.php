<?
namespace BIT\ORM;

class BITLogStructureDate extends BITLogStructureDateTable
{
    /**
     * Функция создает структуру на 1 день
     *
     * @param bool $day
     * @return bool
     */
    public static function createStructureDay($day = false)
    {

        $arUser   = array();
        $level    = array();
        $arLevels = array();
        $maxLevel = 0;

        $dirCache = '/tmp/file_cache/save_structure/';

        $tmp = $day;
        if($tmp === false)
        {
            $tmp  = date('Y-m-d', time());
            $tmp  = strtotime("$tmp");
        }

        if($day === false)
        {
            $res = BITMlmAll::getList(array(
                'select'=>array('UF_USER','UF_SPONSOR','UF_LEVEL'),
                'order'=>array('UF_USER'=>'ASC')
            ));
            while($r = $res->fetch())
            {
                $arLevels[$r['UF_LEVEL']][$r['UF_SPONSOR']][] = array($r['UF_USER']=> $r['UF_SPONSOR']);
                $arUser[$r['UF_USER']] = $r['UF_SPONSOR'];
                if($r['UF_LEVEL']>$maxLevel) $maxLevel = $r['UF_LEVEL'];
            }
        }
        else
        {
            $res = \BITLogSponsorSQLLite::GetInstance()->getByDate($day);

            foreach($res as $r)
            {
                $arLevels[$r['UF_LEVEL']][$r['UF_SPONSOR']][] = array($r['UF_USER']=> $r['UF_SPONSOR']);
                $arUser[$r['UF_USER']] = $r['UF_SPONSOR'];
                if($r['UF_LEVEL']>$maxLevel) $maxLevel = $r['UF_LEVEL'];
            }
        }

        if(count($arLevels)>0)
        {
            for($l = $maxLevel; $l>0; $l--)
            {
                foreach($arLevels[$l]as $sponsor=>$users)
                {
                    for($pl = 1; $pl<=($l-1); $pl++)
                    {
                        $level[$sponsor][$pl] = array_merge((array)$level[$sponsor][$pl],(array)$users);
                        $sponsor = $arUser[$sponsor];
                    }
                }
            }

            if(!is_dir($dirCache))
            {
                mkdir($dirCache);
            }

            foreach($level as $sponsor=>$arr)
            {
                $dir = $dirCache.$sponsor.'/';

//                $mod  = 0755;
//                $root = 'bitrix';
//            system("$dir -R $mod $root");

                if(!is_dir($dir)) { mkdir($dir); }
//            system("$dir -R $mod $root");

                $dir = $dir.$day.'/';

                if(!is_dir($dir)) { mkdir($dir); }
//            system("$dir -R $mod $root");

                file_put_contents($dir.'str', serialize($arr));
                file_put_contents($dir.'level1', serialize($arr[1]));
            }

            self::add(array('UF_DATE'=>$day));
        }
        return true;
    }

    /**
     * Получение структуры по дню
     *
     * @param $userId
     * @param $day
     * @param bool $create
     * @return bool|mixed
     */
    public static function getByDay($userId,$day,$create = false)
    {
        $have = false;
        $file = '/tmp/file_cache/save_structure/'.$userId.'/'.$day.'/str';

        if(is_file($file))
        {
            $have = true;
        }
        else
        {
            if($create === true) $have = self::createStructureDay($day);
            if(!is_file($file))  $have = false;
        }
        if($have === false) return false;

        $ar  = file_get_contents('/tmp/file_cache/save_structure/'.$userId.'/'.$day.'/str');
        $res = unserialize($ar);
        return $res;
    }

    /**
     * Проверка пользователя на день
     * @param $userId
     * @param bool $day
     * @return bool
     */
    public static function checkUser($userId,$day = false)
    {
        $have = false;
        $file = '/tmp/file_cache/save_structure/'.$userId.'/'.$day.'/str';

        if(is_dir($file))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Получени структцры за период для юзера
     * @param $userID
     * @param $dateStart
     * @param $dateFinish
     * @return array
     */
    public static function getStructure($userID,$dateStart,$dateFinish)
    {
        $setDate = array();
        $lastArr = array();
        $MLM     = array();
        $delta   = $dateFinish - $dateStart;

        // работает, не трогать!
        // -------------------->
        if($delta > 86400)
        {
            //  если найдена хоть 1 запись в БД в этом промежутке дат
            //  то найдем их
            if($file = self::getList(array(
                'order'  => array('UF_DATE'=>'DESC'),
                'filter' => array('>UF_DATE'=>$dateStart,'<UF_DATE'=>$dateFinish),
                'limit'  => 1,
            ))->fetch())
            {
                $_start = $dateStart;
                while ($_start<$dateFinish)
                {
                    if($res = self::getByDay($userID,$_start))
                    {
                        $MLM[$_start] = $res;
                        $lastArr = $MLM[$_start];
                        if(count($setDate)>0)
                        {
                            foreach($setDate as $set)
                            {
                                $MLM[$set] = $MLM[$_start];
                            }
                            unset($setDate);
                            $setDate = array();
                        }
                    }
                    elseif(count($lastArr)>0)
                    {
                        $MLM[$_start] = $lastArr;
                        if(count($setDate)>0)
                        {
                            foreach($setDate as $set)
                            {
                                $MLM[$set] = $MLM[$_start];
                            }
                            unset($setDate);
                            $setDate = array();
                        }
                    }
                    else
                    {
                        $setDate[$_start] = $_start;
                    }
                    $_start+=86400;
                }
            }
            # иначе находим любую ближайшую дату
            else
            {
                if($file = self::getList(array(
                    'order'  => array('UF_DATE'=>'ASC'),
                    'filter' => array('>=UF_DATE'=>$dateFinish),
                    'limit'  => 1,
                ))->fetch())
                {
                    if($res = self::getByDay($userID,$file['UF_DATE']))
                    {
                        $_start = $dateStart;
                        while ($_start<$dateFinish)
                        {
                            $MLM[$_start] = $res;
                            $_start+=86400;
                        }
                    }
                }
            }
            if(count($MLM)<1)
            {
                if($file = self::getList(array(
                    'order'  => array('UF_DATE'=>'ASC'),
                    'filter' => array('<=UF_DATE'=>$dateFinish),
                    'limit'  => 1,
                ))->fetch())
                {
                    if($res = self::getByDay($userID,$file['UF_DATE']))
                    {
                        $_start = $dateStart;
                        while ($_start<$dateFinish)
                        {
                            $MLM[$_start] = $res;
                            $_start+=86400;
                        }
                    }
                }
            }
            if(count($MLM)<1)
            {
                ShowError('Невозможно создать структуру на заданный период для данного пользователя');
                die();
            }
        }
        else
        {
            if($res = self::getByDay($userID,$dateStart,true))
            {
                $MLM[$dateStart] = $res;
            }
            else
            {
                if($file = self::getList(array(
                    'order'  => array('UF_DATE'=>'ASC'),
                    'filter' => array('>=UF_DATE'=>$dateFinish),
                    'limit'  => 1,
                ))->fetch())
                {

                    if($res = self::getByDay($userID,$file['UF_DATE']))
                    {
                        $_start = $dateStart;
                        while ($_start<$dateFinish)
                        {
                            $MLM[$_start] = $res;
                            $_start+=86400;
                        }
                    }
                }
            }
        }
        // --------------------<
        return $MLM;
    }

    /**
     * @param $userID
     * @param $dateStart
     * @param $dateFinish
     * @param $level
     * @return array
     */
    public static function getStructureLevel($userID,$dateStart,$dateFinish,$level)
    {
        $setDate = array();
        $lastArr = array();
        $MLM     = array();
        $delta   = $dateFinish - $dateStart;

        // работает, не трогать!
        // -------------------->
        if($delta > 86400)
        {
            //  если найдена хоть 1 запись в БД в этом промежутке дат
            //  то найдем их
            if($file = self::getList(array(
                'order'  => array('UF_DATE'=>'DESC'),
                'filter' => array('>=UF_DATE'=>$dateStart,'<=UF_DATE'=>$dateFinish),
                'limit'  => 1,
            ))->fetch())
            {
                $_start = $dateStart;
                while ($_start<$dateFinish)
                {
                    if($res = self::getByDay($userID,$_start))
                    {
                        $_res         = $res[$level];
                        $MLM[$_start] = $_res;
                        $lastArr      = $MLM[$_start];

                        if(count($setDate)>0)
                        {
                            foreach($setDate as $set)
                            {
                                $MLM[$set] = $MLM[$_start];
                            }
                            unset($setDate);
                            $setDate = array();
                        }
                    }
                    elseif(count($lastArr)>0)
                    {
                        $MLM[$_start] = $lastArr;
                        if(count($setDate)>0)
                        {
                            foreach($setDate as $set)
                            {
                                $MLM[$set] = $MLM[$_start];
                            }
                            unset($setDate);
                            $setDate = array();
                        }
                    }
                    else
                    {
                        $setDate[$_start] = $_start;
                    }
                    $_start+=86400;
                }
            }
            # иначе находим любую ближайшую дату
            else
            {
                if($file = self::getList(array(
                    'order'  => array('UF_DATE'=>'ASC'),
                    'filter' => array('>=UF_DATE'=>$dateFinish),
                    'limit'  => 1,
                ))->fetch())
                {
                    if($res = self::getByDay($userID,$file['UF_DATE']))
                    {
                        $_start = $dateStart;
                        while ($_start<$dateFinish)
                        {
                            $_res         = $res[$level];
                            $MLM[$_start] = $_res;
                            $_start       +=86400;
                        }
                    }
                }
            }
            if(count($MLM)<1)
            {
                ShowError('Невозможно создать структуру на заданный период для данного пользователя');
                die();
            }
        }
        else
        {
            if($res = self::getByDay($userID,$dateStart,true))
            {
                $MLM[$dateStart] = $res[$level];
            }
            else
            {
                if($file = self::getList(array(
                    'order'  => array('UF_DATE'=>'ASC'),
                    'filter' => array('>=UF_DATE'=>$dateFinish),
                    'limit'  => 1,
                ))->fetch())
                {
                    if($res = self::getByDay($userID,$file['UF_DATE']))
                    {
                        $_start = $dateStart;
                        while ($_start<$dateFinish)
                        {
                            $_res         = $res[$level];
                            $MLM[$_start] = $_res;
                            $_start       +=86400;
                        }
                    }
                }
            }
        }
        // --------------------<
        return $MLM;
    }
}
?>