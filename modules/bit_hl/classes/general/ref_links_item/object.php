<?
namespace BIT\ORM;

class BITRefLinksItem extends BITRefLinksItemTable
{

    public static function GetRefLinkByCode($CODE)
    {
        if(empty($CODE))
        {
            return false;
        }
        
        $rsRefLinksItem = \BIT\ORM\BITRefLinksItem::getList(array('filter'=>array('UF_LINK_CODE' => $CODE)));
        return $rsRefLinksItem->Fetch();
    }
}
?>