<?
namespace BIT\ORM;

class BITUserSettings extends BITUserSettingsTable
{
    // настройка вида каталога по умолчанию
    // сортировка каталога
    // способ получения заказа по умолчанию
    // адреса самовывоза
    // адреса самовывоза / профили
//    private $catalogVid  = array('P','S','P');
//    private $catalogSort = array('R');
//    private $order       = array('S','D'); // S - самостоятельно, D - доставка
//    private $option = array(
//        'catalog_vid'  => 'catalog_vid',
//        'catalog_sort' => 'catalog_sort',
//        'order'       => 'order',
//        'store'       => 'store',
//        'profile'     => 'profile',
//    );


    public static function getStore()
    {
        \CModule::IncludeModule('catalog');
        \CModule::IncludeModule('sale');

        $ret = array();

        $res = \CCatalogStore::GetList(array(),array(),false,false,array());
        while($r = $res->Fetch())
            $ret[$r['ID']] = $r;

        return $ret;
    }

    public static function configStore($storeID, $userID = false)
    {
        if($userID === false) $userID = \CUser::GetID();

        if($res = self::getList(array('filter'=>array('UF_USER'=>$userID)))->fetch())
        {
            self::update($res['ID'],array('UF_STORE'=>$storeID));
        }
        else
        {
            self::add(array('UF_USER'=>$userID,'UF_STORE'=>$storeID));
        }
    }
}
?>