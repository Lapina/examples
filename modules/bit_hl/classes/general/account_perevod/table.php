<?
namespace BIT\ORM;

use Bitrix\Main\Application;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use BIT\Fields;
use BIT\Cache;
use BIT\LIB;
Loc::loadMessages(__FILE__);


class BITAccountPerevodTable extends LIB\BTable
{
	const ENTITY_ID = 'HLBLOCK_31';
	const TABLE     = 'bit_account_perevod';
}
?>