<?
namespace BIT\ORM;

use Bitrix\Main\Application;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use BIT\Fields;
use BIT\LIB;

Loc::loadMessages(__FILE__);

class BITMlmAccountTable extends LIB\BTable {

	const ENTITY_ID = 'HLBLOCK_15';
	const TABLE     = 'bit_mlm_account';

    #onBeforeAdd
    #onAfterAdd
    #onAfterUpdate
    #OnBeforeDelete
    #public static function *some*($Event)
    #{
    #    $id     = $Event->getParameter("id");
    #    $Params = $Event->getParameters();
    #    $Fields = $Params['fields'];
    #    return;
    #}

}
?>