<?
namespace BIT\ORM;

class BITCurrencyConvert extends BITCurrencyConvertTable
{
    /**
     * Конвертер из Бонусов в валюту
     * @param $valSum
     * @param $curTo
     * @return bool|float
     */
    public static function ConvertBall($valSum, $curTo = BIT_CURRENCY_ID)
    {
        $amount = false;

        if($res = self::getList(array('filter'=>array('UF_CURRENCY'=>$curTo)))->fetch())
        {
            if($res['UF_AMOUNT'])
            {
                $amount = DoubleVal($res['UF_AMOUNT']*1/1/1);
            }
        }

        if($amount !== false) return doubleval(doubleval($valSum) * $amount);
        else return false;
    }

    /**
     * Конвертер из валюты в бонусы
     *
     * @param $valSum
     * @param $curFrom
     * @return bool|float
     */
    public static function ConvertToBall($valSum, $curFrom = BIT_CURRENCY_ID)
    {
        $amount = false;

        if($res = self::getList(array('filter'=>array('UF_CURRENCY'=>$curFrom)))->fetch())
        {
            if($res['UF_AMOUNT'])
            {
                $amount = DoubleVal($res['UF_AMOUNT']);
            }
        }

        if($amount !== false) return doubleval(doubleval($valSum) / $amount);
        else return false;
    }

    public static function getCurrency($curTo = BIT_CURRENCY_ID)
    {
        if($res = self::getList(array('filter'=>array('UF_CURRENCY'=>$curTo)))->fetch())
        {
            return $res;
        }

        return false;
    }

}


?>