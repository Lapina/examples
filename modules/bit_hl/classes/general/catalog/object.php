<?
namespace BIT\ORM;

class BitProduct extends BitProductTable {
	public static function ConvertPrice($Price = 0, $Currency = 'RUB') {

		return \CCurrencyRates::ConvertCurrency($Price * 1, $Currency, $_SESSION['BIT_CURRENCY_ID'] ? $_SESSION['BIT_CURRENCY_ID'] : "RUB");

	}

	public static function GetPrice($ElementID = false) {
		$Prod = \CCatalogProduct::GetOptimalPrice($ElementID);
		if ($Prod['PRICE']) {
			return self::ConvertPrice($Prod['PRICE']['PRICE'], $Prod['PRICE']['CURRENCY']);
		}
	}

	public static function PrintElementPrice($ElementID = false) {

		echo \SaleFormatCurrency(self::GetPrice($ElementID), $_SESSION['BIT_CURRENCY_ID'] ? $_SESSION['BIT_CURRENCY_ID'] : "RUB");

	}

}
?>