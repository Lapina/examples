<?
namespace BIT\Structs;
use BIT\Types;

class UF_LO extends Types\Float
{
	const CODE   = 'UF_LO';
	const NAME   = 'Личный объем (ежемесячно)';
	const TYPE   = 'float';
	const XML_ID = 'UF_LO';
	const REQ    = false;

	public $VALUE = NULL;
}
?>