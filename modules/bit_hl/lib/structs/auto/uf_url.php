<?
namespace BIT\Structs;
use BIT\Types;

class UF_URL extends Types\String
{
	const CODE   = 'UF_URL';
	const NAME   = 'URL';
	const TYPE   = 'string';
	const XML_ID = 'UF_URL';
	const REQ    = false;

	public $VALUE = NULL;
}
?>