<?
namespace BIT\Structs;
use BIT\Types;

class UF_REGISTER extends Types\Integer
{
	const CODE   = 'UF_REGISTER';
	const NAME   = 'Регистрация';
	const TYPE   = 'integer';
	const XML_ID = 'UF_REGISTER';
	const REQ    = false;

	public $VALUE = NULL;
}
?>