<?
namespace BIT\Structs;
use BIT\Types;

class UF_NAME extends Types\String
{
	const CODE   = 'UF_NAME';
	const NAME   = 'Название';
	const TYPE   = 'string';
	const XML_ID = 'UF_NAME';
	const REQ    = false;

	public $VALUE = NULL;
}
?>