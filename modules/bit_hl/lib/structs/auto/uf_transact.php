<?
namespace BIT\Structs;
use BIT\Types;

class UF_TRANSACT extends Types\Integer
{
	const CODE   = 'UF_TRANSACT';
	const NAME   = 'Транзакция';
	const TYPE   = 'integer';
	const XML_ID = 'UF_TRANSACT';
	const REQ    = false;

	public $VALUE = NULL;
}
?>