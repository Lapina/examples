<?
namespace BIT\Structs;
use BIT\Types;

class UF_LO_3 extends Types\Integer
{
	const CODE   = 'UF_LO_3';
	const NAME   = 'Уровень 3';
	const TYPE   = 'integer';
	const XML_ID = 'UF_LO_3';
	const REQ    = false;

	public $VALUE = NULL;
}
?>