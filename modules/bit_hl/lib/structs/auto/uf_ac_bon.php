<?
namespace BIT\Structs;
use BIT\Types;

class UF_AC_BON extends Types\Integer
{
	const CODE   = 'UF_AC_BON';
	const NAME   = 'Бонусный счет';
	const TYPE   = 'integer';
	const XML_ID = 'UF_AC_BON';
	const REQ    = false;

	public $VALUE = NULL;
}
?>