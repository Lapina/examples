<?
namespace BIT\Structs;
use BIT\Types;

class UF_ORDER extends Types\Integer
{
	const CODE   = 'UF_ORDER';
	const NAME   = 'ID заказа';
	const TYPE   = 'integer';
	const XML_ID = 'UF_ORDER';
	const REQ    = false;

	public $VALUE = NULL;
}
?>