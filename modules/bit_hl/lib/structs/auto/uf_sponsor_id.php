<?
namespace BIT\Structs;
use BIT\Types;

class UF_SPONSOR_ID extends Types\String
{
	const CODE   = 'UF_SPONSOR_ID';
	const NAME   = '';
	const TYPE   = 'string';
	const XML_ID = NULL;
	const REQ    = true;

	public $VALUE = NULL;
}
?>