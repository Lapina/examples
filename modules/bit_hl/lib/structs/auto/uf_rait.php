<?
namespace BIT\Structs;
use BIT\Types;

class UF_RAIT extends Types\Integer
{
	const CODE   = 'UF_RAIT';
	const NAME   = '';
	const TYPE   = 'integer';
	const XML_ID = NULL;
	const REQ    = true;

	public $VALUE = NULL;
}
?>