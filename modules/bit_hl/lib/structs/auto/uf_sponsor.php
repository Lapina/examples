<?
namespace BIT\Structs;
use BIT\Types;

class UF_SPONSOR extends Types\Integer
{
	const CODE   = 'UF_SPONSOR';
	const NAME   = 'Спонсор';
	const TYPE   = 'integer';
	const XML_ID = 'UF_SPONSOR';
	const REQ    = false;

	public $VALUE = NULL;
}
?>