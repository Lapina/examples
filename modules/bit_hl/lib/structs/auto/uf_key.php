<?
namespace BIT\Structs;
use BIT\Types;

class UF_KEY extends Types\String
{
	const CODE   = 'UF_KEY';
	const NAME   = 'UF_KEY';
	const TYPE   = 'string';
	const XML_ID = 'UF_KEY';
	const REQ    = false;

	public $VALUE = NULL;
}
?>