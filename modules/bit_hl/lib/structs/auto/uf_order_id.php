<?
namespace BIT\Structs;
use BIT\Types;

class UF_ORDER_ID extends Types\Integer
{
	const CODE   = 'UF_ORDER_ID';
	const NAME   = 'ID за';
	const TYPE   = 'integer';
	const XML_ID = 'UF_ORDER_ID';
	const REQ    = false;

	public $VALUE = NULL;
}
?>