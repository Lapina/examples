<?
namespace BIT\Structs;
use BIT\Types;

class UF_VALUE extends Types\String
{
	const CODE   = 'UF_VALUE';
	const NAME   = 'Перевод';
	const TYPE   = 'string';
	const XML_ID = 'UF_VALUE';
	const REQ    = false;

	public $VALUE = NULL;
}
?>