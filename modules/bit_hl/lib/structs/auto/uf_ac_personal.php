<?
namespace BIT\Structs;
use BIT\Types;

class UF_AC_PERSONAL extends Types\Integer
{
	const CODE   = 'UF_AC_PERSONAL';
	const NAME   = 'Персональный';
	const TYPE   = 'integer';
	const XML_ID = 'UF_AC_PERSONAL';
	const REQ    = false;

	public $VALUE = NULL;
}
?>