<?
namespace BIT\Structs;
use BIT\Types;

class UF_2_USER_ACCOUNT extends Types\Integer
{
	const CODE   = 'UF_2_USER_ACCOUNT';
	const NAME   = 'UF_2_USER_ACCOUNT';
	const TYPE   = 'integer';
	const XML_ID = 'UF_2_USER_ACCOUNT';
	const REQ    = false;

	public $VALUE = NULL;
}
?>