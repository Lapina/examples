<?
namespace BIT\Structs;
use BIT\Types;

class UF_AC_FOND extends Types\Integer
{
	const CODE   = 'UF_AC_FOND';
	const NAME   = 'Фонд';
	const TYPE   = 'integer';
	const XML_ID = 'UF_AC_FOND';
	const REQ    = false;

	public $VALUE = NULL;
}
?>