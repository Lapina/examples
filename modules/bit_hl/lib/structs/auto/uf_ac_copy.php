<?
namespace BIT\Structs;
use BIT\Types;

class UF_AC_COPY extends Types\Integer
{
	const CODE   = 'UF_AC_COPY';
	const NAME   = 'Накопительный';
	const TYPE   = 'integer';
	const XML_ID = 'UF_AC_COPY';
	const REQ    = false;

	public $VALUE = NULL;
}
?>