<?
namespace BIT\Structs;
use BIT\Types;

class UF_FOND extends Types\String
{
	const CODE   = 'UF_FOND';
	const NAME   = 'Участие в фонде';
	const TYPE   = 'string';
	const XML_ID = 'UF_FOND';
	const REQ    = false;

	public $VALUE = NULL;
}
?>