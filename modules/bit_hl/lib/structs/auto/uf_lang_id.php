<?
namespace BIT\Structs;
use BIT\Types;

class UF_LANG_ID extends Types\Integer
{
	const CODE   = 'UF_LANG_ID';
	const NAME   = 'ID языка';
	const TYPE   = 'integer';
	const XML_ID = 'UF_LANG_ID';
	const REQ    = false;

	public $VALUE = NULL;
}
?>