<?
namespace BIT\Structs;
use BIT\Types;

class ID extends Types\Integer
{
	const CODE   = 'ID';
	const NAME   = '';
	const TYPE   = 'integer';
	const XML_ID = NULL;
	const REQ    = false;

	public $VALUE = NULL;
}
?>