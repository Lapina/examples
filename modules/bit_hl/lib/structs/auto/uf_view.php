<?
namespace BIT\Structs;
use BIT\Types;

class UF_VIEW extends Types\Integer
{
	const CODE   = 'UF_VIEW';
	const NAME   = 'Просмотры';
	const TYPE   = 'integer';
	const XML_ID = 'UF_VIEW';
	const REQ    = false;

	public $VALUE = NULL;
}
?>