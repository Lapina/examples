<?
namespace BIT\Structs;
use BIT\Types;

class UF_SUMM extends Types\Float
{
	const CODE   = 'UF_SUMM';
	const NAME   = 'UF_SUMM';
	const TYPE   = 'float';
	const XML_ID = 'UF_SUMM';
	const REQ    = false;

	public $VALUE = NULL;
}
?>