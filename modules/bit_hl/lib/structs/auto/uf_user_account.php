<?
namespace BIT\Structs;
use BIT\Types;

class UF_USER_ACCOUNT extends Types\Integer
{
	const CODE   = 'UF_USER_ACCOUNT';
	const NAME   = 'ID ';
	const TYPE   = 'integer';
	const XML_ID = NULL;
	const REQ    = false;

	public $VALUE = NULL;
}
?>