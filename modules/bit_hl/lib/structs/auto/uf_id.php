<?
namespace BIT\Structs;
use BIT\Types;

class UF_ID extends Types\Integer
{
	const CODE   = 'UF_ID';
	const NAME   = 'ID';
	const TYPE   = 'integer';
	const XML_ID = 'UF_ID';
	const REQ    = false;

	public $VALUE = NULL;
}
?>