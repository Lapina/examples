<?
namespace BIT\Structs;
use BIT\Types;

class UF_USER_MESSAGE extends Types\String
{
	const CODE   = 'UF_USER_MESSAGE';
	const NAME   = '';
	const TYPE   = 'string';
	const XML_ID = NULL;
	const REQ    = true;

	public $VALUE = NULL;
}
?>