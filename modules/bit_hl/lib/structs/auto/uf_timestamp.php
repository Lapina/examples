<?
namespace BIT\Structs;
use BIT\Types;

class UF_TIMESTAMP extends Types\Integer
{
	const CODE   = 'UF_TIMESTAMP';
	const NAME   = '';
	const TYPE   = 'integer';
	const XML_ID = NULL;
	const REQ    = true;

	public $VALUE = NULL;
}
?>