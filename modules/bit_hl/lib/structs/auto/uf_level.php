<?
namespace BIT\Structs;
use BIT\Types;

class UF_LEVEL extends Types\Integer
{
	const CODE   = 'UF_LEVEL';
	const NAME   = 'Уровень';
	const TYPE   = 'integer';
	const XML_ID = 'UF_LEVEL';
	const REQ    = false;

	public $VALUE = NULL;
}
?>