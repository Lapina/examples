<?
namespace BIT\Structs;
use BIT\Types;

class UF_STATUS extends Types\String
{
	const CODE   = 'UF_STATUS';
	const NAME   = 'Статус (U | B | S)';
	const TYPE   = 'string';
	const XML_ID = 'UF_STATUS';
	const REQ    = false;

	public $VALUE = NULL;
}
?>