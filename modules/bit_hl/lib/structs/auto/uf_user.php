<?
namespace BIT\Structs;
use BIT\Types;

class UF_USER extends Types\Integer
{
	const CODE   = 'UF_USER';
	const NAME   = 'Юзер';
	const TYPE   = 'integer';
	const XML_ID = 'UF_USER';
	const REQ    = false;

	public $VALUE = NULL;
}
?>