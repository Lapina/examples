<?
namespace BIT\Structs;
use BIT\Types;

class UF_TMP_SUMM extends Types\Float
{
	const CODE   = 'UF_TMP_SUMM';
	const NAME   = '';
	const TYPE   = 'float';
	const XML_ID = NULL;
	const REQ    = false;

	public $VALUE = NULL;
}
?>