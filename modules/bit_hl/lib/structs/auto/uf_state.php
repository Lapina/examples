<?
namespace BIT\Structs;
use BIT\Types;

class UF_STATE extends Types\String
{
	const CODE   = 'UF_STATE';
	const NAME   = 'Статус (U | B | S)';
	const TYPE   = 'string';
	const XML_ID = 'UF_STATE';
	const REQ    = false;

	public $VALUE = NULL;
}
?>