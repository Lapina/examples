<?
namespace BIT\Structs;
use BIT\Types;

class UF_MLM extends Types\String
{
	const CODE   = 'UF_MLM';
	const NAME   = 'Участие в программах MLM';
	const TYPE   = 'string';
	const XML_ID = 'UF_MLM';
	const REQ    = false;

	public $VALUE = NULL;
}
?>