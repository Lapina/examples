<?
namespace BIT\Structs;
use BIT\Types;

class UF_LO_2 extends Types\Integer
{
	const CODE   = 'UF_LO_2';
	const NAME   = 'Уровень 2';
	const TYPE   = 'integer';
	const XML_ID = 'UF_LO_2';
	const REQ    = false;

	public $VALUE = NULL;
}
?>