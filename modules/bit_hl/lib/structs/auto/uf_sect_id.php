<?
namespace BIT\Structs;
use BIT\Types;

class UF_SECT_ID extends Types\Integer
{
	const CODE   = 'UF_SECT_ID';
	const NAME   = 'ID раздела';
	const TYPE   = 'integer';
	const XML_ID = 'UF_SECT_ID';
	const REQ    = false;

	public $VALUE = NULL;
}
?>