<?
namespace BIT\Structs;
use BIT\Types;

class UF_SO extends Types\Float
{
	const CODE   = 'UF_SO';
	const NAME   = 'Структурный объем (ежемесячно)';
	const TYPE   = 'float';
	const XML_ID = 'UF_SO';
	const REQ    = false;

	public $VALUE = NULL;
}
?>