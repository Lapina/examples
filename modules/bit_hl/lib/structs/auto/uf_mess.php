<?
namespace BIT\Structs;
use BIT\Types;

class UF_MESS extends Types\String
{
	const CODE   = 'UF_MESS';
	const NAME   = 'Сообщение';
	const TYPE   = 'string';
	const XML_ID = 'UF_MESS';
	const REQ    = false;

	public $VALUE = NULL;
}
?>