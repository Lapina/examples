<?
namespace BIT\Structs;
use BIT\Types;

class UF_BALL extends Types\Integer
{
	const CODE   = 'UF_BALL';
	const NAME   = 'Баллы';
	const TYPE   = 'integer';
	const XML_ID = 'UF_BALL';
	const REQ    = false;

	public $VALUE = NULL;
}
?>