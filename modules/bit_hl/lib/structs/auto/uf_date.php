<?
namespace BIT\Structs;
use BIT\Types;

class UF_DATE extends Types\Integer
{
	const CODE   = 'UF_DATE';
	const NAME   = 'UF_DATE';
	const TYPE   = 'integer';
	const XML_ID = 'UF_DATE';
	const REQ    = false;

	public $VALUE = NULL;
}
?>