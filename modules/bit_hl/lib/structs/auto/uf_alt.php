<?
namespace BIT\Structs;
use BIT\Types;

class UF_ALT extends Types\String
{
	const CODE   = 'UF_ALT';
	const NAME   = 'ALT';
	const TYPE   = 'string';
	const XML_ID = 'UF_ALT';
	const REQ    = false;

	public $VALUE = NULL;
}
?>