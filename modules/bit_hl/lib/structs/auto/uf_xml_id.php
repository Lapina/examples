<?
namespace BIT\Structs;
use BIT\Types;

class UF_XML_ID extends Types\String
{
	const CODE   = 'UF_XML_ID';
	const NAME   = 'XML_ID';
	const TYPE   = 'string';
	const XML_ID = 'UF_XML_ID';
	const REQ    = false;

	public $VALUE = NULL;
}
?>