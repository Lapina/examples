<?
namespace BIT\Structs;
use BIT\Types;

class UF_PARENT extends Types\Integer
{
	const CODE   = 'UF_PARENT';
	const NAME   = 'Родительская ссылка';
	const TYPE   = 'integer';
	const XML_ID = 'UF_PARENT';
	const REQ    = false;

	public $VALUE = NULL;
}
?>