<?
namespace BIT\Structs;
use BIT\Types;

class UF_USER_ID extends Types\Integer
{
	const CODE   = 'UF_USER_ID';
	const NAME   = '';
	const TYPE   = 'integer';
	const XML_ID = NULL;
	const REQ    = true;

	public $VALUE = NULL;
}
?>