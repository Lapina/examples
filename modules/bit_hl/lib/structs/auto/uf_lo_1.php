<?
namespace BIT\Structs;
use BIT\Types;

class UF_LO_1 extends Types\Integer
{
	const CODE   = 'UF_LO_1';
	const NAME   = 'Уровень 1';
	const TYPE   = 'integer';
	const XML_ID = 'UF_LO_1';
	const REQ    = false;

	public $VALUE = NULL;
}
?>