<?
namespace BIT\Structs;
use BIT\Types;

class UF_PUBLISHED extends Types\Integer
{
	const CODE   = 'UF_PUBLISHED';
	const NAME   = '';
	const TYPE   = 'integer';
	const XML_ID = NULL;
	const REQ    = true;

	public $VALUE = NULL;
}
?>