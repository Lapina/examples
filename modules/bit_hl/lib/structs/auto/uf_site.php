<?
namespace BIT\Structs;
use BIT\Types;

class UF_SITE extends Types\Integer
{
	const CODE   = 'UF_SITE';
	const NAME   = 'Сайт';
	const TYPE   = 'integer';
	const XML_ID = 'UF_SITE';
	const REQ    = false;

	public $VALUE = NULL;
}
?>