<?
namespace BIT\Structs;
use BIT\Types;

class UF_2_USER extends Types\Integer
{
	const CODE   = 'UF_2_USER';
	const NAME   = 'Комупереводит';
	const TYPE   = 'integer';
	const XML_ID = 'UF_2_USER';
	const REQ    = false;

	public $VALUE = NULL;
}
?>