<?
namespace BIT\Structs;
class Base
{
	const CODE   = 'BASE_FIELD';
	const NAME   = 'Базовое поле';
	const TYPE   = 'string';
	const REQ    = false;
    const XML_ID = NULL;

	public $VALUE = NULL;
}
?>