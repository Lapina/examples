<?
namespace BIT\LIB;

use Bitrix\Main\Application;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use BIT\Fields;
use BIT\LIB\Cache;
use BIT\ORM;
Loc::loadMessages(__FILE__);

class BTable extends Entity\DataManager
{

    const ENTITY_ID     = '';
    public $MAP         = array();
    public $XML_MAP     = array();
    const TABLE         = '';
    const LOG           = false;
    public $ERRORS      = array();


    const UF_USER       = false;
    const UF_HL_ID      = false;
    const UF_VALUE      = false;
    const UF_DATETIME   = false;

	const LOG_UPDATES 	= false;
	const LOG_ADDS 		= true;

	const TEXT_ADD 		= 'Элемент';
	const NAME_FIELD	= 'ID';

	public function getTableNameExt()
    {
		$C = get_called_class();
		return $C::TABLE;
	}

	public function getEntityExt()
    {
		$C = get_called_class();
		return $C::ENTITY_ID;
	}

	public function getEntityLogExt()
    {
		$C = get_called_class();
		return $C::LOG;
	}

	public function __construct($dropCache = false)
    {

		if ($dropCache || (false == ($MAP = Cache\Obj::Get(self::getTableNameExt() . 'MAP'))))
        {
			$MAP = $this->getMap();
			Cache\Obj::Put($this->TABLE . 'MAP', $MAP);
		}

		$FieldCodes = array_keys($MAP);

		if (count($this->MAP) == 0 || $dropCache)
        {

			foreach ($MAP as $FieldCode => $FieldParams)
            {
				$Field = Fields\Base::GetInstance($FieldCode, $FieldParams, 1);
				$this->MAP[$Field::CODE] = $Field;

				if ($Field::XML_ID)
                {
					$this->XML_MAP[$Field::XML_ID] = $Field;
				}

			}
		}

	}

	public static function getFilePath()
    {
		return __FILE__;
	}

	public static function getTableName()
    {
		return self::getTableNameExt();
	}

    /**
     * Получить map hl
     * @return array|\ArrayObject|bool|mixed
     */
    public static function getMap()
    {
		$Map = array();
		if ($MAP = Cache\Obj::Get($CacheMapName = self::getTableNameExt() . 'MAP'))
        {
			return $MAP;
		}
		global $DB;
		$obMysqlField = $DB->Query('SHOW COLUMNS FROM ' . self::getTableNameExt());

		while ($arMysqlField = $obMysqlField->Fetch())
        {
			switch($arMysqlField['Type']) {

				case 'int(11)' :
				case 'int(11) unsigned' :
				case 'int(18)' :
					$type = 'integer';
					break;

				case 'text' :
					$type = 'string';
					break;

				case 'double' :
					$type = 'float';
					break;

				case 'datetime' :
					$type = 'datetime';
					break;

				default :
					$type = 'string';
			}

			$NF = array('data_type' => $type, );

			if ($arMysqlField['Key'] == 'PRI')
            {
				$NF['primary'] = true;
			}

			if ($arMysqlField['Extra'] == 'auto_increment')
            {
				$NF['autocomplete'] = true;
			}

			$Map[$arMysqlField['Field']] = $NF;
		}

		$obUfFields = \CUserTypeEntity::GetList(array(), $F = array('ENTITY_ID' => self::getEntityExt()));

		while ($arUfField = $obUfFields->Fetch())
        {

			$UfField = \CUserTypeEntity::GetByID($arUfField['ID']);

			$Map[$UfField['FIELD_NAME']] = $Map[$UfField['FIELD_NAME']] + array(
				'XML_ID' => $UfField['XML_ID'],
				'title' => $UfField['EDIT_FORM_LABEL']['ru'],
				'NAME' => $UfField['EDIT_FORM_LABEL']['ru'],
				'REQ' => $UfField['MANDATORY'] == 'Y',
				'TEXTAREA' => $UfField['SETTINGS']['ROWS'] > 1,
				'USER_TYPE' => $UfField['USER_TYPE_ID']
			);

		}
		Cache\Obj::Put($CacheMapName, $MAP);
		return $Map;
	}

	public function PrepareFieldsIn($Event, $C)
    {
		$errors      = array();
		$eventResult = new Entity\EventResult();
		$Params      = $Event->getParameters();
		$UnsetFields = array();

		if (is_array($Fields = $Params['fields']) && count($Fields) > 0)
        {
			$MAP = $C::GetMap();
			unset($MAP['ID']);

			foreach ($Fields as $FieldCode => $FieldVal)
            {
				if (!$MAP[$FieldCode])
                {
					$UnsetFields[$FieldCode];
				}
				$Field = Fields\Base::GetInstance($FieldCode, $MAP[$FieldCode], $FieldVal);
				if ($Field !== false)
                {
					$Fields[$FieldCode] = $Field->ToBase();
				}
			}
			foreach ($MAP as $FieldCode => $FieldParams)
            {
				if ($MAP[$FieldCode]['REQ'] && array_key_exists($FieldCode, $Fields) && ($Fields[$FieldCode] == '' || $Fields[$FieldCode] == '0'))
                {
					$errors[] = new Entity\EntityError('Полe ' . $MAP[$FieldCode]['NAME'] . ' обязательно для заполнения');
				}
			}
			$eventResult->modifyFields($Fields);
			$eventResult->unsetFields($UnsetFields);
		}
        else
        {
			$errors[] = new Entity\EntityError('Поля не найдены');
		}
		if (count($errors) > 0)
        {
			$eventResult->SetErrors($errors);
		}
		$Event->AddResult($eventResult);
		return $Event;
	}

	public static function onBeforeUpdate($Event)
    {
        self::storeLog($Event,$type='update');
		return self::PrepareFieldsIn($Event, $C = get_called_class());
	}

	public static function onBeforeAdd($Event)
    {
		return self::PrepareFieldsIn($Event, $C = get_called_class());
	}

	public static function onAfterAdd($Event)
    {
        self::storeLog($Event);
		return true;
	}

    public static function storeLog(Entity\Event $event,$type='add')
    {
        $id   = $event->getParameter("id");
        $idHL = self::getTableNameExt();
        $log  = (bool) constant($event->getEntity()->getFullName() . '::LOG' );
        $get  = array();
        $set  = array();

        if(!$id || !$idHL || $log === false) return;

        $Fields = $event->getParameter("fields");
        $Entity = $event->getEntity()->getFullName();

        $get["UF_USER"]     = constant($Entity . '::UF_USER' );
        $get["UF_HL_ID"]    = constant($Entity . '::UF_HL_ID' );
        $get["UF_VALUE"]    = constant($Entity . '::UF_VALUE' );
        $get["UF_DATETIME"] = constant($Entity . '::UF_DATETIME' );

        // если не добавляем в лог
        if($type=='add' && !constant($Entity . '::LOG_ADDS' ))
        {
            return;
        }

        foreach($get as $k=>$v)
        {
            if($Fields[$v])
            {
                $set[$k] = $Fields[$v];
            }
        }

        // обновление логов
        if(constant($Entity . '::LOG_UPDATES' ) && $type=='update')
        {
            $currentFields = $Entity::GetByID($id)->Fetch();
            $newFields     = $Fields + $currentFields;
            $diffFields    = array_keys(array_diff_assoc($currentFields, $newFields));
            $changes       = array();

            foreach($diffFields as $diffFieldName)
            {
                $changes[$diffFieldName] = $currentFields[$diffFieldName].'===>>>'.$newFields[$diffFieldName];
            }

            $set['UF_SERVICE_DATA'] = 'CHANGES='.json_encode($changes);
        }

        if(count($set)>0)
        {
            $upId = $id;

            if($id['ID'])
            {
                $upId = $id['ID'];
            }

            $set["UF_HL_ID"]    = $upId;
            $set["UF_HL_TABLE"] = $idHL;

            if(strlen($set["UF_USER"])<1)
            {
                $set["UF_USER"] = \CUser::GetID();
            }

            if(!$set['UF_DATETIME'])
            {
                $set['UF_DATETIME']=time();
            }

            $set['UF_ENTITY_OBJECT'] = get_called_class();
            $res = ORM\BITEventLog::add($set);
        }
    }

	public static function storeLogDirect($set)
    {

        if(count($set)>0)
        {

            if(strlen($set["UF_USER_MOD"])<1)
            {
                $set["UF_USER_MOD"] = \CUser::GetID();
            }

			if(!$set['UF_DATETIME']) {
				$set['UF_DATETIME'] = time();
			}

			$set['UF_ENTITY_OBJECT'] = get_called_class();

            $res = ORM\BITEventLog::add($set);
        }
    }
}
?>