<?

namespace BIT\LIB\Cache;

const USE_APC = false;
const SALT    = 4;

class Obj
{
	public function __construct()
    {
		global $BIT_CACHE;
		$BIT_CACHE = array();
	}

	public static function Drop($key)
    {
		unset($GLOBALS['BIT_CACHE'][$key]);
		return true;
	}

	static function Put($Obj = false)
    {
		list($Obj, $Val, $NoApc) = func_get_args();

		if (is_object($Obj))
        {

			if ($NoApc == false && USE_APC && function_exists('apc_store'))
            {
				return apc_store(SALT . 'BitCache' . get_class($Obj), new ArrayObject($Obj), 3600);
			}
            else
            {
				$GLOBALS['BIT_CACHE'][SALT . 'BitCache' . get_class($Obj)] = $Obj;
			}

			return true;
		}
        else if ($Obj && $Val)
        {
			if (is_object($Val))
            {
				if ($NoApc == false && USE_APC && function_exists('apc_store'))
                {
					return apc_store(SALT . 'BitCache' . $Obj, new \ArrayObject($Val), 3600);
				}
                else
                {
					$GLOBALS['BIT_CACHE'][SALT . 'BitCache' . $Obj] = $Val;
				}

				return true;
			}
            else
            {
				if ($NoApc == false && USE_APC && function_exists('apc_store'))
                {
					return apc_store(SALT . 'BitCache' . $Obj, $Val, 3600);
				}
                else
                {
					$GLOBALS['BIT_CACHE'][SALT . 'BitCache' . $Obj] = $Val;
				}

			}

			return true;
		}

		return false;
	}

	static function Get($key = '', $NoApc = false)
    {
		global $USER;

		if ($USER->IsAdmin())
        {
			return false;
		}

		if ($NoApc == false && USE_APC && function_exists('apc_fetch'))
        {
			$RES = apc_fetch(SALT . 'BitCache' . $key);

			if ($RES instanceof \ArrayObject && ($arrayCopy = $RES->getArrayCopy()))
            {
				if (class_exists($arrayCopy['__PHP_Incomplete_Class_Name']))
                {
					$RET = new $arrayCopy['__PHP_Incomplete_Class_Name'];
					unset($arrayCopy['__PHP_Incomplete_Class_Name']);

					foreach ($arrayCopy as $Field => $Value)
                    {
						$RET->$Field = $Value;
					}
					return $RET;
				}
                else
                {
					return false;
				}
			}
            else
            {
				return $RES;
			}

		}
        else
        {
			return $GLOBALS['BIT_CACHE'][SALT . 'BitCache' . $key] ? $GLOBALS['BIT_CACHE'][SALT . 'BitCache' . $key] : false;
		}
	}

}
?>