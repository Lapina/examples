<?

namespace BIT\Fields;
use BIT\Structs;
use BIT\Types;
use BIT\LIB\Cache;

class Base extends Structs\Base
{
	const RELOAD = false;

    /**
     * @param bool $FieldName
     * @param array $FieldParams
     * @param null $Value
     * @return \ArrayObject|bool|mixed
     */
    static function GetInstance($FieldName = false, $FieldParams = array(), $Value = NULL)
    {
		if ($FieldName == false || !is_array($FieldParams))
        {
			return false;
		}

		$CacheFieldName = 'BCache' . $FieldName;

		if (Cache\Obj::Get($CacheFieldName,$NO_APC=true))
        {
			$new_field = clone Cache\Obj::Get($CacheFieldName,$NO_APC=true);

			if($new_field instanceof __PHP_Incomplete_Class)
            {
				$new_field = new $new_field->__PHP_Incomplete_Class_Name;
			}

			$new_field->SetVal($Value);

			return $new_field;
		}

		/////////////////////////////////////////////////////
		//           CHECK TYPE   //////////////////////////
		///////////////////////////////////////////////////
		$strType = ucfirst($FieldParams['data_type'] ? $FieldParams['data_type'] : BIT\Structs\Base::TYPE);

		if (!file_exists($TypeFile = __DIR__ . '/fields/types/' . strtolower($strType) . '.php') && !file_exists($TypeFile = __DIR__ . '/fields/types/auto/' . strtolower($strType) . '.php') || self::RELOAD)
        {

			$Type = <<<TYPE
<?
namespace BIT\Types;
use BIT\Fields;
class {$strType} extends Fields\Base
{

}
?>
TYPE;

			RewriteFile($TypeFile, $Type);
		}

		include_once ($TypeFile);

		/////////////////////////////////////////////////////
		//           CHECK STRUCT //////////////////////////
		///////////////////////////////////////////////////

		$FileLost     = !file_exists($StructFile = __DIR__ . '/structs/' . strtolower($FieldName) . '.php') && !file_exists($StructFile = __DIR__ . '/structs/auto/' . strtolower($FieldName) . '.php') || self::RELOAD;
		$StructChange = false;
		$XML_ID       = $FieldParams["XML_ID"] ? "'" . $FieldParams["XML_ID"] . "'" : 'NULL';

		if (!$FileLost)
        {
			include_once ($StructFile);

			$sName  = 'BIT\Structs\\' . $FieldName;
			$Struct = new $sName;

			$StructChange = $Struct::NAME != $FieldParams["NAME"] || ($XML_ID && $Struct::XML_ID != $XML_ID) || $Struct::REQ != (bool)$FieldParams["REQ"];
		}

		if ($FileLost || $StructChange)
        {

			$REQ    = ((bool)$FieldParams["REQ"]) ? "true" : 'false';
			$struct = <<<STRUCT
<?
namespace BIT\Structs;
use BIT\Types;

class {$FieldName} extends Types\\{$strType}
{
	const CODE   = '{$FieldName}';
	const NAME   = '{$FieldParams["NAME"]}';
	const TYPE   = '{$FieldParams["data_type"]}';
	const XML_ID = {$XML_ID};
	const REQ    = {$REQ};

	public \$VALUE = NULL;
}
?>
STRUCT;

			RewriteFile($StructFile, $struct);
		}

		include_once ($StructFile);


		if (
            !file_exists($FieldFile = __DIR__ . '/fields/' . strtolower($FieldName) . '.php')      &&
            !file_exists($FieldFile = __DIR__ . '/fields/auto/' . strtolower($FieldName) . '.php') ||
            self::RELOAD)
        {

			$XML_ID = $FieldParams["XML_ID"] ? "'" . $FieldParams["XML_ID"] . "'" : 'NULL';
			$REQ    = ((bool)$FieldParams["REQ"]) ? "true" : 'false';
			$Field  = <<<FIELD
<?
namespace BIT\Fields;
use BIT\Structs;

class {$FieldName} extends Structs\\{$FieldName}
{

}
?>
FIELD;

			RewriteFile($FieldFile, $Field);
		}

		include_once ($FieldFile);

		$FieldName = 'BIT\Fields\\' . $FieldName;
		$obField   = new $FieldName;

		$obField->SetVal($Value);

		Cache\Obj::Put($CacheFieldName, $obField,$NO_APC=true);

		return $obField;
	}

    /**
     * @return \ArrayObject|bool|mixed
     */
    public static function GetEnumID()
    {
		$Return = array();
		$_CLASS = array_pop(explode('\\', get_called_class()));

        $CacheEnumName = 'BCacheEnum' . $_CLASS;

		if ($Return = Cache\Obj::Get($CacheEnumName))
        {
			return $Return;
		}

		if ($arUF = \CUserTypeEntity::GetList(array(), array(
			'ENTITY_ID' => 'HLBLOCK_%',
			'FIELD_NAME' => $_CLASS
		))->Fetch())
        {
			return $arUF['ID'];
		}
	}

    /**
     * @param bool $ClearCache
     * @return array|\ArrayObject|bool|mixed
     */
    public static function GetEnums($ClearCache = false)
    {
		$Return = array();
		$_CLASS = array_pop(explode('\\', get_called_class()));

		$CacheEnumName = 'BCacheEnum' . $_CLASS;

		if ($ClearCache == false && ($Return = Cache\Obj::Get($CacheEnumName)))
        {
			return $Return;
		}

		if ($FID = self::GetEnumID())
        {
			$obEnums = \CUserFieldEnum::GetList(array(), array("USER_FIELD_ID" => $FID));

			while ($arEnum = $obEnums->Fetch())
            {
				$Return[$arEnum['ID']] = $arEnum['VALUE'];
			}

			Cache\Obj::Put($CacheEnumName, $Return);
		}
		return $Return;
	}

    /**
     * @param bool $Val
     * @return bool
     */
    public function Validate($Val = false)
    {
		return true;
	}

    /**
     * @return null
     */
    public function FromBase()
    {
		return $this->VALUE;
	}

    /**
     * @return string
     */
    public function ToBase()
    {
		return htmlspecialchars($this->VALUE);
	}

    /**
     * @param null $val
     */
    public function SetVal($val = NULL)
    {
		$this->VALUE = $val;
	}

    /**
     * @param bool $print
     * @return null
     */
    public function GetVal($print = false)
    {
		if ($print)
        {
			return $this->FromBase();
		}

		return $this->VALUE;
	}

    /**
     * @return string
     */
    public function __toString()
    {
		return (string)$this->GetVal();
	}

}
?>