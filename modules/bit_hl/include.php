<?

if (!CModule::IncludeModule('main'))
	return false;

IncludeModuleLangFile(__FILE__);

$INC = array(
	'BIT\E' => 'lib/e.php',

	'BIT\LIB\Cache\Obj' => 'lib/cache/class.php',
	'BIT\Structs\Base'  => 'lib/structs/base_field.php',
	'BIT\Fields\Base'   => 'lib/base_field.php',
	'BIT\LIB\BTable'    => 'lib/base_table.php',

    # логи
    'BIT\ORM\BITEventLogTable' => 'classes/general/log/table.php',
    'BIT\ORM\BITEventLog'      => 'classes/general/log/object.php',

    # языки
    'BIT\ORM\BITLangTable' => 'classes/general/lang/table.php',
    'BIT\ORM\BITLang'      => 'classes/general/lang/object.php',

    # страны
    'BIT\ORM\BITCountryTable' => 'classes/general/country/table.php',
    'BIT\ORM\BITCountry'      => 'classes/general/country/object.php',

    # разделы сайта
    'BIT\ORM\BITSiteSectionsTable' => 'classes/general/site_section/table.php',
    'BIT\ORM\BITSiteSections'      => 'classes/general/site_section/object.php',

    # сообщения
    'BIT\ORM\BITMessTable' => 'classes/general/mess/table.php',
    'BIT\ORM\BITMess'      => 'classes/general/mess/object.php',

    # сайты
    'BIT\ORM\BITSitesTable' => 'classes/general/sites/table.php',
    'BIT\ORM\BITSites'      => 'classes/general/sites/object.php',

    # формы
    'BIT\ORM\BITFormsTable' => 'classes/general/forms/table.php',
    'BIT\ORM\BITForms'      => 'classes/general/forms/object.php',

    # отзывы
    'BIT\ORM\BITUserFeedbackTable' => 'classes/general/feedback/table.php',
    'BIT\ORM\BITUserFeedback'      => 'classes/general/feedback/object.php',


    # каталог
    'BIT\ORM\BitProductTable' => 'classes/general/catalog/table.php',
    'BIT\ORM\BitProduct'      => 'classes/general/catalog/object.php',

    # структуа юзеров
    'BIT\ORM\BITMlmAllTable' => 'classes/general/mlm_all/table.php',
    'BIT\ORM\BITMlmAll'      => 'classes/general/mlm_all/object.php',

    # mlm сводная таблица счетов
    'BIT\ORM\BITMlmAccountTable' => 'classes/general/mlm_account/table.php',
    'BIT\ORM\BITMlmAccount'      => 'classes/general/mlm_account/object.php',

    # mlm заказы
    'BIT\ORM\BITMlmOrderTable' => 'classes/general/mlm_order/table.php',
    'BIT\ORM\BITMlmOrder'      => 'classes/general/mlm_order/object.php',

    # mlm логи статусов
    'BIT\ORM\BITLogStatusTable' => 'classes/general/log_status/table.php',
    'BIT\ORM\BITLogStatus'      => 'classes/general/log_status/object.php',

    # mlm логи фондов
    'BIT\ORM\BITLogFondTable' => 'classes/general/log_fond/table.php',
    'BIT\ORM\BITLogFond'      => 'classes/general/log_fond/object.php',

    # mlm логи таблицы изеров/спорсоров
    'BIT\ORM\BITMlmLogMlmTable' => 'classes/general/log_mlm/table.php',
    'BIT\ORM\BITMlmLogMlm'      => 'classes/general/log_mlm/object.php',

    # mlm логи LO
    'BIT\ORM\BITLogLoTable' => 'classes/general/log_lo/table.php',
    'BIT\ORM\BITLogLo'      => 'classes/general/log_lo/object.php',

    # mlm логи баллов
    'BIT\ORM\BITLogBallTable' => 'classes/general/log_ball/table.php',
    'BIT\ORM\BITLogBall'      => 'classes/general/log_ball/object.php',

    # mlm логи крона
    'BIT\ORM\BITLogCronTable' => 'classes/general/log_cron/table.php',
    'BIT\ORM\BITLogCron'      => 'classes/general/log_cron/object.php',

    # mlm логи крона 1 раз в месяц
    'BIT\ORM\BITLogCronMonthTable' => 'classes/general/log_cron_month/table.php',
    'BIT\ORM\BITLogCronMonth'      => 'classes/general/log_cron_month/object.php',

    # mlm логи спонсоров
    'BIT\ORM\BITLogSponsorTable' => 'classes/general/log_sponsor/table.php',
    'BIT\ORM\BITLogSponsor'      => 'classes/general/log_sponsor/object.php',
    
    # mlm логи личного объема
    'BIT\ORM\BILogLoTable' => 'classes/general/log_lo/table.php',
    'BIT\ORM\BILogLo'      => 'classes/general/log_lo/object.php',
    
    # ref список ссылок
    'BIT\ORM\BITRefLinksTable' => 'classes/general/ref_links/table.php',
    'BIT\ORM\BITRefLinks'      => 'classes/general/ref_links/object.php',

    # лог сохранения структуры юзеров
    'BIT\ORM\BITLogStructureDateTable' => 'classes/general/log_structure_date/table.php',
    'BIT\ORM\BITLogStructureDate'      => 'classes/general/log_structure_date/object.php',

    # таблица сатусов
    'BIT\ORM\BITStatusTable' => 'classes/general/status/table.php',
    'BIT\ORM\BITStatus'      => 'classes/general/status/object.php',

    # пользовательские предупреждения
    'BIT\ORM\BITUserNotificationTable' => 'classes/general/user_notification/table.php',
    'BIT\ORM\BITUserNotification'      => 'classes/general/user_notification/object.php',

    # типы предупреждений
    'BIT\ORM\BITTypeNoticeTable' => 'classes/general/type_notification/table.php',
    'BIT\ORM\BITTypeNotice'      => 'classes/general/type_notification/object.php',

    # перевод между счетами
    'BIT\ORM\BITAccountPerevodTable' => 'classes/general/account_perevod/table.php',
    'BIT\ORM\BITAccountPerevod'      => 'classes/general/account_perevod/object.php',

    # конвертер валют
    'BIT\ORM\BITCurrencyConvertTable' => 'classes/general/currency_convert/table.php',
    'BIT\ORM\BITCurrencyConvert'      => 'classes/general/currency_convert/object.php',

    # аффилиаты
    'BIT\ORM\BITAffiliateTable' => 'classes/general/affiliate/table.php',
    'BIT\ORM\BITAffiliate'      => 'classes/general/affiliate/object.php',

    # статистика посещение по аффилиатам
    'BIT\ORM\BITAffiliateStatisticTable' => 'classes/general/affiliate_statistic/table.php',
    'BIT\ORM\BITAffiliateStatistic'      => 'classes/general/affiliate_statistic/object.php',

    # настройка юзеров
    'BIT\ORM\BITUserSettingsTable' => 'classes/general/user_settings/table.php',
    'BIT\ORM\BITUserSettings'      => 'classes/general/user_settings/object.php',

    # местоположения складов
    'BIT\ORM\BITStorageLocationTable' => 'classes/general/storage_location/table.php',
    'BIT\ORM\BITStorageLocation'      => 'classes/general/storage_location/object.php',

);
Bitrix\Main\Loader::registerAutoLoadClasses('bit_hl', $INC);

?>