<?php

class BITSendPay
{

    public $error       = array(); # массив ошибок
    public $orderId     = false;   # ID заказа
    public $owner       = false;   # ID пользователя владельца
    public $ownerMail   = false;   # E-mail пользователя владельца
    public $elementId   = false;   # ID элемента
    public $element     = array(); # Инфа элемента
    public $price       = array(); # Массив ен
    public $currency    = false;   # валюта

    private $domain                = '';
    private $payMail               = ''; # e-mail на который платить комиссию
    private $sandbox               =  false;
    private $developerAccountEmail = '';
    private $ReverseAllParallelPaymentsOnError = '';
    private $applicationId         = '';
    private $apiUsername           = '';
    private $apiPassword           = '';
    private $apiSignature          = '';
    private $apiSubject            = '';

    private $actionType            = 'PAY';
    private $cancelURL             = '';
    private $returnURL             = '';
    private $feesPayer             = 'PRIMARYRECEIVER'; # SENDER, PRIMARYRECEIVER, EACHRECEIVER, SECONDARYONLY

    public function  __construct($id)
    {
        # устанавливаем заказ
        if(!$id)
        {
            $this->setError('Не указан ID заказа','No order ID');
            return $this->setReturn();
        }
        else
        {
            $this->orderId = $id;
        }
        # подключаем модули
        if(!$this->includeModule())
        {
            return $this->setReturn();
        }
        # устанавливаем инфу о заказе
        if(!$this->setOrder())
        {
            return $this->setReturn();
        }
        # устанавливаем инфу о элементе и владельце
        if(!$this->getInfo())
        {
            return $this->setReturn();
        }
        # устанавливаем инфу о элементе и владельце
        if(!$this->setElement())
        {
            return $this->setReturn();
        }
        # устанавливаем инфу о элементе и владельце
        $this->sendPay();
    }

    /**
     * Подключение модулей
     *
     * @return bool
     */
    public function includeModule()
    {
        if(!CModule::IncludeModule('bit_rent_calendar') || !CModule::IncludeModule('iblock') || !CModule::IncludeModule('sale'))
        {
            $this->setError('Невозможно подключить необходиме модули','Unable to connect the necessary modules');
            return false;
        }
        return true;
    }

    /**
     * Устанавливаем инфу о заказе
     *
     * @return bool
     */
    public function setOrder()
    {
        $db_vals = CSaleOrderPropsValue::GetList(
            array("SORT" => "ASC"),
            array(
                "ORDER_ID" => $this->orderId,
                "ORDER_PROPS_ID" => 18
            )
        );
        if ($arVals = $db_vals->Fetch())
            $PRICE = $arVals["VALUE"];

        if($PRICE)
        {

            $arOrder = CSaleOrder::GetByID($this->orderId);
            if(!$arOrder)
            {
                $this->setError('Невозможно найти данный заказ','Unable to find this order');
                return false;
            }
            if($arOrder["PAYED"] == "Y")
            {
                $this->setError('Заказ уже оплачен','Order has been paid');
                return false;
            }
            if(intval($arOrder['PRICE'])>0 && strlen($arOrder['CURRENCY'])>0)
            {
                $this->price['ALL']    = intval($PRICE);
                $this->price['OWNER']  = intval($PRICE)*0.90; # деньги арендодателю
                $this->price['ARENDA'] = intval($PRICE)*0.10; # деньги аренде
                $this->currency        = 'RUB';
            }
            else
            {
                $this->setError('Не указана цена или валюта','Not specified price or currency');
                return false;
            }
            return true;
        }
        else
        {
            $this->setError('Проведение оплаты невозможнон','Error sent pay');
            return false;
        }

    }

    /**
     * Устанавливаем информацию о товаре и владельце
     * @return bool
     */
    public function getInfo()
    {
        # найдем аренду
        $find   = false;
        $arRent = BITRent::GetInstance()->getItemByParams(array('ID_ORDER'=>$this->orderId));
        if(count($arRent)>0)
        {
            $find = true;
            foreach($arRent as $rent)
            {
                if(!$rent["OWNER"])
                {
                    $this->setError('Не указан владелец дома','Unknown homeowner');
                    return false;
                }
                else
                {
                    $this->owner = $rent["OWNER"];
                }

                if($rent["ID_ELEMENT"])
                {
                    $this->elementId = $rent["ID_ELEMENT"];
                    break;
                }
            }
        }
        # найдем покупку, если нет аренды
        if(!$this->owner  || !$this->elementId )
        {
            $arRent = BITSaleOrder::GetInstance()->getItemByParams(array('ID_ORDER'=>$this->orderId));
            if(count($arRent)>0)
            {
                $find = true;
                foreach($arRent as $rent)
                {
                    if(!$rent["OWNER"])
                    {
                        $this->setError('Не указан владелец дома','Unknown homeowner');
                        return false;
                    }
                    else
                    {
                        $this->owner = $rent["OWNER"];
                    }

                    if($rent["ID_ELEMENT"])
                    {
                        $this->elementId = $rent["ID_ELEMENT"];
                        break;
                    }
                }
            }
        }
        if(!$this->setMail())
        {
            //$this->setError('Не указан Mail','Unknown Mail');
            return false;
        }
        if(!$find)
        {
            $this->setError('Не найдено заказа аренды','Order rent found');
            return false;
        }

        return true;
    }

    /**
     * Устанавливаем адрес почты
     *
     * @return bool
     */
    public function setMail()
    {
        if(!$this->owner)
        {
            $this->setError('Не указан владелец','Unknown homeowner');
            return false;
        }

        $rsUser = CUser::GetByID($this->owner);
        $arUser = $rsUser->Fetch();

        if(!$arUser['UF_PAYPALL_MAIL'])
        {
            $this->setError('Не указан e-mail владельца дома','No e-mail home owner');
            return false;
        }
        else
        {
            $this->ownerMail = $arUser['UF_PAYPALL_MAIL'];
        }
        return true;
    }

    /**
     * Установка инфы о элементе
     * @return bool
     */
    public function setElement()
    {
        if(!$this->elementId)
        {
            $this->setError('Не указан дом','No home');
            return false;
        }

        $elem = CIBlockElement::GetByID($this->elementId)->GetNext();
        if($elem['NAME'])
        {
            $this->element = $elem;
        }
        else
        {
            $this->setError('Не указано название дома','No name home');
            return false;
        }
        return true;
    }

    /**
     * Отправка оплаты
     */
    public function sendPay()
    {

        require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/include/sale_payment/bit_paypal/lib/includes/config.php');
        require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/include/sale_payment/bit_paypal/lib/autoload.php');

        # Create PayPal object.
        $PayPalConfig = array(
            'Sandbox'               => $this->sandbox,
            'DeveloperAccountEmail' => $this->developerAccountEmail,
            'ReverseAllParallelPaymentsOnError' => $this->ReverseAllParallelPaymentsOnError,
            'ApplicationID'         => $this->applicationId,
            'IPAddress'             => $_SERVER['REMOTE_ADDR'],
            'APIUsername'           => $this->apiUsername,
            'APIPassword'           => $this->apiPassword,
            'APISignature'          => $this->apiSignature,
            'APISubject'            => $this->apiSubject
        );

        $PayPal = new BIT\PayPal\Adaptive($PayPalConfig);

        # Prepare request arrays
        $PayRequestFields = array(
            'ActionType'    => $this->actionType,
            'CancelURL'     =>  $this->cancelURL,
            'CurrencyCode'  => $this->currency,
            'FeesPayer'     => $this->feesPayer,
            'IPNNotificationURL' => $this->domain,
            'ReturnURL'    =>  $this->returnURL,
        );

        $ClientDetailsFields = array(
            'CustomerID'   => '',
            'CustomerType' => '',
            'GeoLocation'  => '',
            'Model'        => '',
            'PartnerName'  => ''
        );

        $Receivers = array();

        $Receiver = array(
            'Amount'         => $this->price['ALL'],
            'Email'          => '',
            'InvoiceID'      => '',
            'PaymentType'    => '',
            'PaymentSubType' => '',
            'Phone'          => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''),
            'Primary'        => 'true'
        );
        array_push($Receivers,$Receiver);

        $Receiver = array(
            'Amount'         => $this->price['ARENDA'],
            'Email'          =>  $this->payMail ,
            'InvoiceID'      => '',
            'PaymentType'    => '',
            'PaymentSubType' => '',
            'Phone'          => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''),
            'Primary'        => 'false'
        );
        array_push($Receivers,$Receiver);

        $SenderIdentifierFields = array(
            'UseCredentials' => ''
        );

        $AccountIdentifierFields = array(
            'Email' => '',
            'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => '') # Sender's phone number.  Numbers only.
        );

        $PayPalRequestData = array(
            'PayRequestFields'        => $PayRequestFields,
            'ClientDetailsFields'     => $ClientDetailsFields,
            'Receivers'               => $Receivers,
            'SenderIdentifierFields'  => $SenderIdentifierFields,
            'AccountIdentifierFields' => $AccountIdentifierFields
        );


        $PayPalResult = $PayPal->Pay($PayPalRequestData);
        global $APPLICATION;

        if($PayPalResult)
        {
            $APPLICATION->RestartBuffer();
            echo json_encode($PayPalResult);
            die();
        }

        else
        {
            $this->setError('Произошла ошибка при запросе на сервер PayPall','An error occurred while requesting the server PayPall');

            $APPLICATION->RestartBuffer();

            $errors['ERROR_BIT'] = $this->error;
            echo json_encode($errors);

            die();

        }
    }

    /**
     * Запись ошибок
     *
     * @param bool $ru
     * @param bool $en
     */
    public function setError($ru = false,$en = false)
    {
        $name = $en;
        if(LANGUAGE_ID=='ru')
            $name = $ru;
        $this->error[] = $name;
    }

    /**
     * Установка возвращаемого массива
     *
     * @param array $return
     * @return array
     */
    public function setReturn($return = array())
    {
        global $APPLICATION;

        //$this->setError('Произошла ошибка при запросе на сервер PayPall','An error occurred while requesting the server PayPall');

        $APPLICATION->RestartBuffer();

        echo json_encode($this->error);

        die();

        return $arr;
    }
}
?>