<?php namespace BIT\PayPal;

use DOMDocument;

class Adaptive extends PayPal
{
    var $XMLNamespace  = '';
    var $ApplicationID = ' ';
    var $DeviceID      = '';
    var $IPAddress     = '';
    var $DetailLevel   = '';
    var $ErrorLanguage = '';

    var $DeveloperAccountEmail = '';

    function __construct($DataArray)
    {
        parent::__construct($DataArray);

        $this->XMLNamespace = 'http://svcs.paypal.com/types/ap';
        $this->DeviceID = isset($DataArray['DeviceID']) ? $DataArray['DeviceID'] : '';
        $this->IPAddress = isset($DataArray['IPAddress']) ? $DataArray['IPAddress'] : $_SERVER['REMOTE_ADDR'];
        $this->DetailLevel = isset($DataArray['DetailLevel']) ? $DataArray['DetailLevel'] : 'ReturnAll';
        $this->ErrorLanguage = isset($DataArray['ErrorLanguage']) ? $DataArray['ErrorLanguage'] : 'ru_RU';
        $this->APISubject = isset($DataArray['APISubject']) ? $DataArray['APISubject'] : '';
        $this->DeveloperAccountEmail = isset($DataArray['DeveloperAccountEmail']) ? $DataArray['DeveloperAccountEmail'] : '';

        if($this -> Sandbox)
        {
            $this -> ApplicationID = isset($DataArray['ApplicationID']) ? $DataArray['ApplicationID'] : '';
            $this -> APIUsername = isset($DataArray['APIUsername']) && $DataArray['APIUsername'] != '' ? $DataArray['APIUsername'] : '';
            $this -> APIPassword = isset($DataArray['APIPassword']) && $DataArray['APIPassword'] != '' ? $DataArray['APIPassword'] : '';
            $this -> APISignature = isset($DataArray['APISignature']) && $DataArray['APISignature'] != '' ? $DataArray['APISignature'] : '';

            $this -> EndPointURL = isset($DataArray['EndPointURL']) && $DataArray['EndPointURL'] != '' ? $DataArray['EndPointURL'] : 'https://svcs.sandbox.paypal.com/';
        }
        else
        {
            $this -> ApplicationID = isset($DataArray['ApplicationID']) ? $DataArray['ApplicationID'] : '';
            $this -> APIUsername = isset($DataArray['APIUsername']) && $DataArray['APIUsername'] != '' ? $DataArray['APIUsername'] : '';
            $this -> APIPassword = isset($DataArray['APIPassword']) && $DataArray['APIPassword'] != ''  ? $DataArray['APIPassword'] : '';
            $this -> APISignature = isset($DataArray['APISignature']) && $DataArray['APISignature'] != ''  ? $DataArray['APISignature'] : '';
            $this -> EndPointURL = isset($DataArray['EndPointURL']) && $DataArray['EndPointURL'] != ''  ? $DataArray['EndPointURL'] : 'https://svcs.paypal.com/';
        }
    }


    /**
     * HTTP заголовки для запроса к API
     *
     * @access	public
     * @param	boolean	$PrintHeaders
     * @return	string	$headers
     */
    function BuildHeaders($PrintHeaders)
    {
        $headers = array(
            'X-PAYPAL-SECURITY-USERID: ' . $this -> APIUsername,
            'X-PAYPAL-SECURITY-PASSWORD: ' . $this -> APIPassword,
            'X-PAYPAL-SECURITY-SIGNATURE: ' . $this -> APISignature,
            'X-PAYPAL-SECURITY-SUBJECT: ' . $this -> APISubject,
            'X-PAYPAL-SECURITY-VERSION: ' . $this -> APIVersion,
            'X-PAYPAL-REQUEST-DATA-FORMAT: XML',
            'X-PAYPAL-RESPONSE-DATA-FORMAT: XML',
            'X-PAYPAL-APPLICATION-ID: ' . $this -> ApplicationID,
            'X-PAYPAL-DEVICE-ID: ' . $this -> DeviceID,
            'X-PAYPAL-DEVICE-IPADDRESS: ' . $this -> IPAddress
        );

        if($this -> Sandbox)
        {
            array_push($headers, 'X-PAYPAL-SANDBOX-EMAIL-ADDRESS: '.$this->DeveloperAccountEmail);
        }
        if($PrintHeaders)
        {
/*            echo '<pre />';
            print_r($headers);*/
        }

        return $headers;
    }

    /**
     * запрос API PayPal, используя CURL
     *
     * @param string $Request
     * @param string $APIName
     * @param string $APIOperation
     * @return mixed
     */
    function CURLRequest($Request = "", $APIName = "", $APIOperation = "")
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_VERBOSE, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_URL, $this -> EndPointURL . $APIName . '/' . $APIOperation);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $Request);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this -> BuildHeaders(false));
        $a = curl_error($curl);


        if($this -> APIMode == 'Certificate')
        {
            curl_setopt($curl, CURLOPT_SSLCERT, $this -> PathToCertKeyPEM);
//            die(CURLOPT_SSLCERT);
        }

        $Response = curl_exec($curl);

        curl_close($curl);
        return $Response;
    }

    /**
     * Получить все ошибки от PayPal
     *
     * @access	public
     * @param	string	$XML
     * @return	mixed[]	$ErrorsArray
     */
    function GetErrors($XML)
    {
        $DOM = new DOMDocument();
        $DOM -> loadXML($XML);

        $Errors = $DOM -> getElementsByTagName('error') -> length > 0 ? $DOM -> getElementsByTagName('error') : array();
        $ErrorsArray = array();
        foreach($Errors as $Error)
        {
            $Receiver    = $Error -> getElementsByTagName('receiver') -> length > 0 ? $Error -> getElementsByTagName('receiver') -> item(0) -> nodeValue : '';
            $Category    = $Error -> getElementsByTagName('category') -> length > 0 ? $Error -> getElementsByTagName('category') -> item(0) -> nodeValue : '';
            $Domain      = $Error -> getElementsByTagName('domain') -> length > 0 ? $Error -> getElementsByTagName('domain') -> item(0) -> nodeValue : '';
            $ErrorID     = $Error -> getElementsByTagName('errorId') -> length > 0 ? $Error -> getElementsByTagName('errorId') -> item(0) -> nodeValue : '';
            $ExceptionID = $Error -> getElementsByTagName('exceptionId') -> length > 0 ? $Error -> getElementsByTagName('exceptionId') -> item(0) -> nodeValue : '';
            $Message     = $Error -> getElementsByTagName('message') -> length > 0 ? $Error -> getElementsByTagName('message') -> item(0) -> nodeValue : '';
            $Parameter   = $Error -> getElementsByTagName('parameter') -> length > 0 ? $Error -> getElementsByTagName('parameter') -> item(0) -> nodeValue : '';
            $Severity    = $Error -> getElementsByTagName('severity') -> length  > 0 ? $Error -> getElementsByTagName('severity') -> item(0) -> nodeValue : '';
            $Subdomain   = $Error -> getElementsByTagName('subdomain') -> length > 0 ? $Error -> getElementsByTagName('subdomain') -> item(0) -> nodeValue : '';

            $CurrentError = array(
                'Receiver'  => $Receiver,
                'Category'  => $Category,
                'Domain'    => $Domain,
                'ErrorID'   => $ErrorID,
                'ExceptionID' => $ExceptionID,
                'Message'   => $Message,
                'Parameter' => $Parameter,
                'Severity'  => $Severity,
                'Subdomain' => $Subdomain
            );
            array_push($ErrorsArray, $CurrentError);
        }
        return $ErrorsArray;
    }

    /**
     * Получить запрос из XML
     *
     * @access	public
     * @return	string	$XML	Returns raw XML request envelope
     */
    function GetXMLRequestEnvelope()
    {
        $XML = '<requestEnvelope xmlns="">';
        $XML .= '<detailLevel>' . $this -> DetailLevel . '</detailLevel>';
        $XML .= '<errorLanguage>' . $this -> ErrorLanguage . '</errorLanguage>';
        $XML .= '</requestEnvelope>';

        return $XML;
    }

    /**
     * Отправка запроса оплаты  PayPal
     *
     * @access	public
     * @param	mixed[]	$DataArray
     * @return	mixed[] $ResponseDataArray
     */
    function Pay($DataArray)
    {
        // запрос
        $PayRequestFields = isset($DataArray['PayRequestFields']) ? $DataArray['PayRequestFields'] : array();
        $ActionType       = isset($PayRequestFields['ActionType']) ? $PayRequestFields['ActionType'] : '';
        $CancelURL        = isset($PayRequestFields['CancelURL']) ? $PayRequestFields['CancelURL'] : '';
        $CurrencyCode     = isset($PayRequestFields['CurrencyCode']) ? $PayRequestFields['CurrencyCode'] : '';
        $FeesPayer        = isset($PayRequestFields['FeesPayer']) ? $PayRequestFields['FeesPayer'] : '';
        $Memo             = isset($PayRequestFields['Memo']) ? $PayRequestFields['Memo'] : '';
        $Pin              = isset($PayRequestFields['Pin']) ? $PayRequestFields['Pin'] : '';
        $PreapprovalKey   = isset($PayRequestFields['PreapprovalKey']) ? $PayRequestFields['PreapprovalKey'] : '';
        $ReturnURL        = isset($PayRequestFields['ReturnURL']) ? $PayRequestFields['ReturnURL'] : '';
        $SenderEmail      = isset($PayRequestFields['SenderEmail']) ? $PayRequestFields['SenderEmail'] : '';
        $TrackingID       = isset($PayRequestFields['TrackingID']) ? $PayRequestFields['TrackingID'] : '';
        $IPNNotificationURL = isset($PayRequestFields['IPNNotificationURL']) ? $PayRequestFields['IPNNotificationURL'] : '';
        $ReverseAllParallelPaymentsOnError = '';

        // клиент
        $ClientDetailsFields = isset($DataArray['ClientDetailsFields']) ? $DataArray['ClientDetailsFields'] : array();
        $CustomerID          = isset($ClientDetailsFields['CustomerID']) ? $ClientDetailsFields['CustomerID'] : '';
        $CustomerType        = isset($ClientDetailsFields['CustomerType']) ? $ClientDetailsFields['CustomerType'] : '';
        $GeoLocation         = isset($ClientDetailsFields['GeoLocation']) ? $ClientDetailsFields['GeoLocation'] : '';
        $Model               = isset($ClientDetailsFields['Model']) ? $ClientDetailsFields['Model'] : '';
        $PartnerName         = isset($ClientDetailsFields['PartnerName']) ? $ClientDetailsFields['PartnerName'] : '';

        $FundingTypes = isset($DataArray['FundingTypes']) ? $DataArray['FundingTypes'] : array();

        // Получатели
        $Receivers = isset($DataArray['Receivers']) ? $DataArray['Receivers'] : array();
        $Amount    = isset($Receivers['Amount']) ? $Receivers['Amount'] : '';
        $Email     = isset($Receivers['Email']) ? $Receivers['Email'] : '';
        $InvoiceID = isset($Receivers['InvoiceID']) ? $Receivers['InvoiceID'] : '';
        $PaymentType    = isset($Receivers['PaymentType']) ? $Receivers['PaymentType'] : '';
        $PaymentSubType = isset($Receivers['PaymentSubType']) ? $Receivers['PaymentSubType'] : '';
        $Phone   = isset($Receivers['Phone']) ? $Receivers['Phone'] : '';
        $Primary = isset($Receivers['Primary']) ? strtolower($Receivers['Primary']) : '';

        $SenderIdentifierFields  = isset($DataArray['SenderIdentifierFields']) ? $DataArray['SenderIdentifierFields'] : array();
        $UseCredentials          = isset($SenderIdentifierFields['UseCredentials']) ? $SenderIdentifierFields['UseCredentials'] : '';
        $AccountIdentifierFields = isset($DataArray['AccountIdentifierFields']) ? $DataArray['AccountIdentifierFields'] : array();
        $AccountEmail = isset($AccountIdentifierFields['Email']) ? $AccountIdentifierFields['Email'] : '';
        $AccountPhone = isset($AccountIdentifierFields['Phone']) ? $AccountIdentifierFields['Phone'] : '';

        // генерация запроса XML
        $XMLRequest = '<?xml version="1.0" encoding="utf-8"?>';
        $XMLRequest .= '<PayRequest xmlns="' . $this -> XMLNamespace . '">';
        $XMLRequest .= $this -> GetXMLRequestEnvelope();
        $XMLRequest .= $ActionType != '' ? '<actionType xmlns="">' . $ActionType . '</actionType>' : '';
        $XMLRequest .= $CancelURL != '' ? '<cancelUrl xmlns="">' . $CancelURL . '</cancelUrl>' : '';

        if(count($ClientDetailsFields) > 0)
        {
            $XMLRequest .= '<clientDetails xmlns="">';
            $XMLRequest .= $this -> ApplicationID != '' ? '<applicationId xmlns="">' . $this -> ApplicationID . '</applicationId>' : '';
            $XMLRequest .= $CustomerID != '' ? '<customerId xmlns="">' . $CustomerID . '</customerId>' : '';
            $XMLRequest .= $CustomerType != '' ? '<customerType xmlns="">' . $CustomerType . '</customerType>' : '';
            $XMLRequest .= $this -> DeviceID != '' ? '<deviceId xmlns="">' . $this -> DeviceID . '</deviceId>' : '';
            $XMLRequest .= $GeoLocation != '' ? '<geoLocation xmlns="">' . $GeoLocation . '</geoLocation>' : '';
            $XMLRequest .= $this -> IPAddress != '' ? '<ipAddress xmlns="">' . $this -> IPAddress . '</ipAddress>' : '';
            $XMLRequest .= $Model != '' ? '<model xmlns="">' . $Model . '</model>' : '';
            $XMLRequest .= $PartnerName != '' ? '<partnerName xmlns="">' . $PartnerName . '</partnerName>' : '';
            $XMLRequest .= '</clientDetails>';
        }

        $XMLRequest .= $CurrencyCode != '' ? '<currencyCode xmlns="">' . $CurrencyCode . '</currencyCode>' : '';
        $XMLRequest .= $FeesPayer != '' ? '<feesPayer xmlns="">' . $FeesPayer . '</feesPayer>' : '';

        if(count($FundingTypes) > 0)
        {
            $XMLRequest .= '<fundingConstraint xmlns="">';
            $XMLRequest .= '<allowedFundingType xmlns="">';

            foreach($FundingTypes as $FundingType)
            {
                $XMLRequest .= '<fundingTypeInfo xmlns="">';
                $XMLRequest .= '<fundingType xmlns="">' . $FundingType . '</fundingType>';
                $XMLRequest .= '</fundingTypeInfo>';
            }

            $XMLRequest .= '</allowedFundingType>';
            $XMLRequest .= '</fundingConstraint>';
        }

        $XMLRequest .= $IPNNotificationURL != '' ? '<ipnNotificationUrl xmlns="">' . $IPNNotificationURL . '</ipnNotificationUrl>' : '';
        $XMLRequest .= $Memo != '' ? '<memo xmlns="">' . $Memo . '</memo>' : '';
        $XMLRequest .= $Pin != '' ? '<pin xmlns="">' . $Pin . '</pin>' : '';
        $XMLRequest .= $PreapprovalKey != '' ? '<preapprovalKey xmlns="">' . $PreapprovalKey . '</preapprovalKey>' : '';

        $XMLRequest .= '<receiverList xmlns="">';

        $IsDigitalGoods = FALSE;

        foreach($Receivers as $Receiver)
        {
            $IsDigitalGoods = strtoupper($Receiver['PaymentType']) == 'DIGITALGOODS' ? TRUE : FALSE;

            $XMLRequest .= '<receiver xmlns="">';
            $XMLRequest .= $Receiver['Amount'] != '' ? '<amount xmlns="">' . $Receiver['Amount'] . '</amount>' : '';
            $XMLRequest .= $Receiver['Email'] != '' ? '<email xmlns="">' . $Receiver['Email'] . '</email>' : '';
            $XMLRequest .= $Receiver['InvoiceID'] != '' ? '<invoiceId xmlns="">' . $Receiver['InvoiceID'] . '</invoiceId>' : '';
            $XMLRequest .= $Receiver['PaymentType'] != '' ? '<paymentType xmlns="">' . $Receiver['PaymentType'] . '</paymentType>' : '';
            $XMLRequest .= $Receiver['PaymentSubType'] != '' ? '<paymentSubType xmlns="">' . $Receiver['PaymentSubType'] . '</paymentSubType>' : '';

            if($Receiver['Phone']['CountryCode'] != '')
            {
                $XMLRequest .= '<phone xmlns="">';
                $XMLRequest .= $Receiver['Phone']['CountryCode'] != '' ? '<countryCode xmlns="">' . $Receiver['Phone']['CountryCode'] . '</countryCode>' : '';
                $XMLRequest .= $Receiver['Phone']['PhoneNumber'] != '' ? '<phoneNumber xmlns="">' . $Receiver['Phone']['PhoneNumber'] . '</phoneNumber>' : '';
                $XMLRequest .= $Receiver['Phone']['Extension'] != '' ? '<extension xmlns="">' . $Receiver['Phone']['Extension'] . '</extension>' : '';
                $XMLRequest .= '</phone>';
            }

            $XMLRequest .= $Receiver['Primary'] != '' ? '<primary xmlns="">' . strtolower($Receiver['Primary']) . '</primary>' : '';
            $XMLRequest .= '</receiver>';
        }
        $XMLRequest .= '</receiverList>';


        if(count($SenderIdentifierFields) > 0)
        {
            $XMLRequest .= '<sender>';
            $XMLRequest .= '<useCredentials xmlns="">' . $SenderIdentifierFields['UseCredentials'] . '</useCredentials>';
            $XMLRequest .= '</sender>';
        }

        if(count($AccountIdentifierFields) > 0)
        {
            $XMLRequest .= '<account xmlns="">';
            $XMLRequest .= $AccountEmail != '' ? '<email xmlns="">' . $AccountEmail . '</email>' : '';

            if($AccountPhone != '')
            {
                $XMLRequest .= '<phone xmlns="">';
                $XMLRequest .= $AccountPhone['CountryCode'] != '' ? '<countryCode xmlns="">' . $AccountPhone['CountryCode'] . '</countryCode>' : '';
                $XMLRequest .= $AccountPhone['PhoneNumber'] != '' ? '<phoneNumber xmlns="">' . $AccountPhone['PhoneNumber'] . '</phoneNumber>' : '';
                $XMLRequest .= $AccountPhone['Extension'] != '' ? '<extension xmlns="">' . $AccountPhone['Extension'] . '</extension>' : '';
                $XMLRequest .= '</phone>';
            }

            $XMLRequest .= '</account>';
        }

        $XMLRequest .= $ReturnURL != '' ? '<returnUrl xmlns="">' . $ReturnURL . '</returnUrl>' : '';
        $XMLRequest .= $ReverseAllParallelPaymentsOnError != '' ? '<reverseAllParallelPaymentsOnError xmlns="">' . $ReverseAllParallelPaymentsOnError . '</reverseAllParallelPaymentsOnError>' : '';
        $XMLRequest .= $SenderEmail != '' ? '<senderEmail xmlns="">' . $SenderEmail . '</senderEmail>' : '';
        $XMLRequest .= $TrackingID != '' ? '<trackingId xmlns="">' . $TrackingID . '</trackingId>' : '';
        $XMLRequest .= '</PayRequest>';

        //делаем запрос
        $XMLResponse = $this -> CURLRequest($XMLRequest, 'AdaptivePayments', 'Pay');

        $DOM = new DOMDocument();
        $DOM -> loadXML($XMLResponse);

        // парсим XML
        $Fault  = $DOM -> getElementsByTagName('FaultMessage') -> length > 0 ? true : false;
        $Errors = $this -> GetErrors($XMLResponse);
        $Ack    = $DOM -> getElementsByTagName('ack') -> length > 0 ? $DOM -> getElementsByTagName('ack') -> item(0) -> nodeValue : '';
        $Build  = $DOM -> getElementsByTagName('build') -> length > 0 ? $DOM -> getElementsByTagName('build') -> item(0) -> nodeValue : '';
        $CorrelationID = $DOM -> getElementsByTagName('correlationId') -> length > 0 ? $DOM -> getElementsByTagName('correlationId') -> item(0) -> nodeValue : '';
        $Timestamp     = $DOM -> getElementsByTagName('timestamp') -> length > 0 ? $DOM -> getElementsByTagName('timestamp') -> item(0) -> nodeValue : '';

        $PayKey = $DOM -> getElementsByTagName('payKey') -> length > 0 ? $DOM -> getElementsByTagName('payKey') -> item(0) -> nodeValue : '';
        $PaymentExecStatus = $DOM -> getElementsByTagName('paymentExecStatus') -> length > 0 ? $DOM -> getElementsByTagName('paymentExecStatus') -> item(0) -> nodeValue : '';

        if($this -> Sandbox)
        {
            if($IsDigitalGoods)
            {
                $RedirectURL = 'https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/pay?paykey='.$PayKey;
            }
            else
            {
                $RedirectURL = 'https://www.sandbox.paypal.com/webscr?cmd=_ap-payment&paykey=' . $PayKey;
            }
        }
        else
        {
            if($IsDigitalGoods)
            {
                $RedirectURL = 'https://www.paypal.com/webapps/adaptivepayment/flow/pay?paykey='.$PayKey;
            }
            else
            {
                $RedirectURL = 'https://www.paypal.com/webscr?cmd=_ap-payment&paykey=' . $PayKey;
            }
        }

        $ResponseDataArray = array(
            'Errors' => $Errors,
            'Ack'    => $Ack,
            'Build'  => $Build,
            'CorrelationID' => $CorrelationID,
            'Timestamp' => $Timestamp,
            'PayKey' => $PayKey,
            'PaymentExecStatus' => $PaymentExecStatus,
            'RedirectURL' => $PayKey != '' ? $RedirectURL : '',
            'XMLRequest'  => $XMLRequest,
            'XMLResponse' => $XMLResponse
        );

        return $ResponseDataArray;
    }



}